﻿Module modPublic
    Public IAMLOADING As Boolean = True
    Public OKFORPOSTBACK As Boolean = False
    Public PriorDropDownNDX As Long = -99999
    Public WasteMax As Long = 500
    Public ORGLabelSize As Font
    Public RESOURCEID As String
    Public RESOURCENAME As String
    'Public TASKID As String
    Public SUBJOBNO As String
    Public ORDERNO As String
    Public MINORSTOPDURATION As String
    Public TASKUPDATEINTERVAL As String
    Public ACTUALSUPDATEINTERVAL As String
    Public CURRENTOPERATORID As String = ""
    Public CURRENTOPERATORNAME As String = ""
    Public EVENTSTARTDATE As String
    Public GLOBALSTARTENDOVERRIDE As String = ""
    Public UNKNOWNSTOP As Boolean = False
    Public UnknownStopStart As Date
    Public DROPMINUTE As Boolean = False
    Public SHOWERROR As Boolean = False

    Public currentnoAssistants As String
    Public currenttaskID As String
    Public currentClient As String
    Public currentJobName As String
    Public currentOpCodeNDX As String
    Public currentconfigNDX As String
    Public LastQtyToGo As Long
    Public InitialQtyToGo As Long
    Public CurrentQtyToGo As Long
    'Public TotalImpressions As Long
    Public TotalPieces As Long
    'Public WasteImpressions As Long
    Public WastePieces As Long
    Public noUp As Long
    Public noUpFromJob As Long
    Public JOBSPEED As Long
    Public RESOURCEPRINTER As String
    Public BARTENDERPATH As String
    Public LOADFLAGQTY As String
    Public TEMPLATEPATH As String


    Public opCodeRejectGoodNdx As Long
    Public opCodeAddGoodNdx As Long
    Public opCodeUnknownStopNdx As Long
    Public opCodeMinorStopNdx As Long
    Public opCodeJobStartSetupNdx As Long
    Public opCodeJobStartRunningNdx As Long
    Public OpCodeNotInUseNdx As Long

    Public EMERGENCYJOB As Boolean = False
    Public DoNotWriteApplicationLog As Boolean = True
    Public HideDC360 As Boolean = False

    Public WIP_NEWPALLET As String = ""
    Public WIP_NUp As String = ""
    Public WIP_PartRef As String = ""

    Public PriorJobLst As New List(Of Json_Structures_Cls.cls_task_resource)

    Private UPDATE_ACTUALS_PRIOR_TASKID As Long = -1

    Public Structure OpCodeList
        Dim opCodeID As String
        Dim opCodeUnitID As String
        Dim opCodeAreaID As String
        Dim opCodeAreaName As String
        Dim opCodeName As String
        Dim opCodeColor As String
        Dim opCodeTypeName As String
    End Structure
    Public myOpCodeList() As OpCodeList

    Public Structure ConfigList
        Dim ConfigID As String
        Dim ConfigName As String
        Dim ConfigDDNdx As Long
    End Structure
    Public myConfigList() As ConfigList

    Public Structure RePost
        Dim wholeLine As String
        Dim strhttpRoot As String
        Dim strhttpPath As String
        Dim strCorID As String
        Dim strVersion As String
        Dim strNotes As String
        Dim strJSonData As String
    End Structure

    Public Function GetOpCodeNdx(opCodeName As String) As Long
        WriteApplicationLog("MODPUBLIC GetOpCodeNdx - Entered - Parameter: " & opCodeName)
        For i As Long = 0 To myOpCodeList.GetUpperBound(0)
            If myOpCodeList(i).opCodeName = opCodeName Then
                WriteApplicationLog("MODPUBLIC GetOpCodeNdx - Exited")
                Return i
            End If
        Next
        WriteApplicationLog("MODPUBLIC GetOpCodeNdx - Exited")
        Return -1
    End Function
    Public Function GetOpCodeNdxByID(opCodeID As String) As Long
        WriteApplicationLog("MODPUBLIC GetOpCodeNdxByID - Entered")
        For i As Long = 0 To myOpCodeList.GetUpperBound(0)
            If myOpCodeList(i).opCodeID = opCodeID Then
                WriteApplicationLog("MODPUBLIC GetOpCodeNdxByID - Exited")
                Return i
            End If
        Next
        WriteApplicationLog("MODPUBLIC GetOpCodeNdxByID - Exited")
        Return -1
    End Function
    Public Function GetConfigNdxByID(configID As String) As Long
        WriteApplicationLog("MODPUBLIC GetConfigNdxByID - Entered")
        For i As Long = 0 To myConfigList.GetUpperBound(0)
            If myConfigList(i).ConfigID = configID Then
                WriteApplicationLog("MODPUBLIC GetConfigNdxByID - Exited")
                Return myConfigList(i).ConfigDDNdx
            End If
        Next
        WriteApplicationLog("MODPUBLIC GetConfigNdxByID - Exited")
        Return 0
    End Function
    Public Function CreateEventDateTime(dateTime As Date) As String
        WriteApplicationLog("MODPUBLIC CreateEventDateTime - Entered")
        Dim returnString As String = Format(dateTime, "yyyy-MM-ddTHH:mm:ss")
        WriteApplicationLog("MODPUBLIC CreateEventDateTime - Exited")
        Return returnString
    End Function

    Public Function GetCorrelationID() As String
        WriteApplicationLog("MODPUBLIC GetCorrelationID - Entered")
        WriteApplicationLog("MODPUBLIC GetCorrelationID - Exited")
        Return RESOURCEID.ToString.PadLeft(4, "0") & (New Random).Next(1, 99999).ToString.PadLeft(5, "0")
    End Function

    Public Sub DoPostBackTimesheet(eventType As String, operatorID As String, eventTime As String)
        If Not OKFORPOSTBACK Then Exit Sub
        WriteApplicationLog("MODPUBLIC DoPostBackTimesheet - Entered")
        Dim lstNewEvent As Json_Structures_Cls.struct_POST_timesheetEvents '<- Make Sure You Set The Correct Structure, In This Case We Want struct_POST_timesheetEvents

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() ' Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        With lstNewEvent
            .id = 1
            .resourceId = RESOURCEID
            .eventType = eventType '"Login" or "Logout"
            .operatorId = operatorID
            .eventDateTime = eventTime '"2018-04-18T18:41:01"
        End With ''"{"id":0,"resourceId":120,"eventType":"Login","operatorId":456,"eventDateTime":"2018-04-18T14:53:01.517"}

        Json_Structures_Cls.post_timesheetEvents(lstNewEvent) 'Will Return True If Successfull POST, You Can Use Your Own Variable If You See A Need For It
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            'MsgBox("Successfull POST")
        Else
            'MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If
        DoPostBackActuals()
        WriteApplicationLog("MODPUBLIC DoPostBackTimesheet - Exited")
    End Sub
    Public Sub DoPostBackCostEvent(ByRef passedEventEndDate As String, eventCount As String, eventType As String, Optional overrideCurrentOpCodeNdx As Long = -1, Optional overrideNewOpCodeNdx As Long = -1, Optional startEventDate As String = "", Optional newAssist As String = "XXX", Optional newConfig As String = "XXX", Optional newConfigNdx As String = "XXX")
        If Not OKFORPOSTBACK Then Exit Sub
        WriteApplicationLog("MODPUBLIC DoPostBackCostEvent - Entered")
        Dim lstNewEvent As Json_Structures_Cls.struct_POST_sfdcEvents '<- Make Sure You Set The Correct Structure, In This Case We Want struct_POST_sfdcEvents      

        If startEventDate = "" Then startEventDate = EVENTSTARTDATE
        Dim eventEndDate As String = passedEventEndDate

        Dim ignoreGross As Boolean = False
        If eventType = "26" Or eventType = "27" Then ignoreGross = True 'Add Good, Add Bad

        Dim localCurrentOpCodeNdx As Long = currentOpCodeNDX
        If overrideCurrentOpCodeNdx <> -1 Then localCurrentOpCodeNdx = overrideCurrentOpCodeNdx

        Dim localNewOpCodeNdx As Long = GetOpCodeNdx(frmMain.f.cmbOpCode.Text)
        If overrideNewOpCodeNdx <> -1 Then localNewOpCodeNdx = overrideNewOpCodeNdx
        If localNewOpCodeNdx = -1 Then localNewOpCodeNdx = localCurrentOpCodeNdx

        If localCurrentOpCodeNdx = -1 Then localCurrentOpCodeNdx = localNewOpCodeNdx

        If newAssist = "XXX" Then newAssist = frmMain.f.cmbAssistants.Text
        If newConfig = "XXX" Then newConfig = frmMain.f.cmbConfig.Text
        If newConfigNdx = "XXX" Then newConfigNdx = frmMain.f.cmbConfig.SelectedIndex



        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1


        Dim grossImpressions As String = eventCount / noUp
        Dim grossCopies As String = eventCount
        Dim netImpressions As String = eventCount / noUp
        Dim netCopies As String = eventCount

        Dim currentEventCount As Long
        If Not ignoreGross Then
            currentEventCount = LastQtyToGo - eventCount
            'Check to see if we are in a waste opCode
            If myOpCodeList(localCurrentOpCodeNdx).opCodeTypeName = "Waste" Or myOpCodeList(localCurrentOpCodeNdx).opCodeTypeName = "Setup" Then
                grossImpressions = currentEventCount / noUp
                grossCopies = currentEventCount
                netImpressions = 0
                netCopies = 0
                InitialQtyToGo += currentEventCount 'Since we have "RUN" these pieces, increase the initialQtyToGo by this amount, thus eliminating the 'bad' pieces from the math
                WastePieces += currentEventCount
            Else 'Count as normal
                LastQtyToGo = eventCount
                grossImpressions = currentEventCount / noUp
                grossCopies = currentEventCount
                netImpressions = currentEventCount / noUp
                netCopies = currentEventCount
            End If

        Else
            currentEventCount = eventCount

            If eventType = "26" Then 'add good
                LastQtyToGo -= eventCount
                InitialQtyToGo -= eventCount
                WastePieces -= eventCount
                CurrentQtyToGo -= eventCount
            ElseIf eventType = "27" Then 'add bad
                LastQtyToGo += eventCount
                InitialQtyToGo += eventCount
                WastePieces += eventCount
                CurrentQtyToGo += eventCount
            End If

            grossImpressions = 0
            grossCopies = 0
            netImpressions = currentEventCount / noUp
            netCopies = currentEventCount
        End If


        If DROPMINUTE Then eventEndDate = CreateEventDateTime(UnknownStopStart)
        DROPMINUTE = False

        If GLOBALSTARTENDOVERRIDE <> "" Then
            startEventDate = GLOBALSTARTENDOVERRIDE
            eventEndDate = GLOBALSTARTENDOVERRIDE
        ElseIf startEventDate > eventEndDate Then
            eventEndDate = passedEventEndDate 'if we are ending before we start, set them equal.
            UnknownStopStart = DateTime.ParseExact(eventEndDate, "yyyy-MM-ddTHH:mm:ss", Nothing) '2019-03-18T14:44:22
        End If

        EMERGENCYJOB = False
        If Not currenttaskID Is Nothing Then
            If currenttaskID < 0 Then EMERGENCYJOB = True
        End If


        SHOWERROR = False
        'If eventType = "21" And grossImpressions - netImpressions > WasteMax Then
        '    frmMain.f.lblError.Text = "Warning, you have posted a" & vbCrLf & "large amount of waste: " & grossImpressions - netImpressions
        '    SHOWERROR = True

        '    'Dim th As System.Threading.Thread = New Threading.Thread(AddressOf DialogTask)
        '    'th.SetApartmentState(Threading.ApartmentState.STA)
        '    'th.Start("Warning, you have posted a large amount of waste: " & grossImpressions - netImpressions & vbCrLf & "If this was not intended, please transfer the waste to good.")

        '    'Dim tmpBox As New frmDialog("Warning, you have posted a large amount of waste: " & grossImpressions - netImpressions & vbCrLf & "If this was not intended, please transfer the waste to good.")
        '    'tmpBox.StartPosition = FormStartPosition.CenterScreen
        '    'tmpBox.Show()
        'ElseIf eventType = "21" And grossImpressions > 0 And currentnoAssistants = 0 Then

        '    frmMain.f.lblError.Text = "Warning, you are running" & vbCrLf & "without assistants"
        '    SHOWERROR = True

        '    'Dim th As System.Threading.Thread = New Threading.Thread(AddressOf DialogTask)
        '    'th.SetApartmentState(Threading.ApartmentState.STA)
        '    'th.Start("Warning, you are running without assistants" & vbCrLf & "If this was not intended," & vbCrLf & "please correct the number of assistants.")

        '    'Dim tmpBox As New frmDialog("Warning, you are running without assistants" & vbCrLf & "If this was not intended," & vbCrLf & "please correct the number of assistants.")
        '    'tmpBox.StartPosition = FormStartPosition.CenterScreen
        '    'tmpBox.Show()
        'End If

        With lstNewEvent
            .resourceID = RESOURCEID                        'As int32           '* 120
            .noAssistants = currentnoAssistants             'As Int32           '* 1
            .grossImpressions = grossImpressions          'As Int32           '* 20
            .netImpressions = netImpressions             'As Int32           '* 10
            .grossCopies = grossCopies                'As Int32           '* 10
            .netCopies = netCopies                  'As Int32           '* 5
            .eventType = eventType                          'As Int32           '* 0
            .eventStartDateTime = startEventDate            'As String          '* "2018-03-22T09:14:58"
            .eventEndDateTime = eventEndDate                'As String          '* "2018-03-22T11:25:38"
            .operatorId = CURRENTOPERATORID                 'As String          '* "1590"
            .operatorName = CURRENTOPERATORNAME                              'As String          '* "OSTROWSKI, VINCENT R"
            .noStreams = 1                                  'As Int32           '* 1
            '.timeSheetNo = 0                                'As Int32           '  394971
            .taskID = currenttaskID                     'As Int32           '* 0
            .subJobNo = IIf(SUBJOBNO Is Nothing, "", SUBJOBNO)                                  'As String          '* "string"
            .OpCodeID = myOpCodeList(localCurrentOpCodeNdx).opCodeID                    'As Int32           '*11
            .emergencyJob = EMERGENCYJOB ' False                           'As Boolean         '*
            .OpUnitID = myOpCodeList(localCurrentOpCodeNdx).opCodeUnitID                                   'As Int32           '*
            .OpAreaID = myOpCodeList(localCurrentOpCodeNdx).opCodeAreaID                                   'As Int32           '* 449
            .configId = myConfigList(currentconfigNDX).ConfigID                     'As Int32           '* 0
            .configName = myConfigList(currentconfigNDX).ConfigName          'As String          '* "string"
            .runNo = 0                                      'As Int32           '* 1
            .shiftNote = ""                                 'As String          '  "string"
            'New to V2
            .noAssistantsNew = newAssist
            .configIdNew = myConfigList(newConfigNdx).ConfigID
            .configNameNew = newConfig
            .OpCodeIDNew = myOpCodeList(localNewOpCodeNdx).opCodeID
            .OpUnitIDNew = myOpCodeList(localNewOpCodeNdx).opCodeUnitID
            .OpAreaIDNew = myOpCodeList(localNewOpCodeNdx).opCodeAreaID

        End With

        'If we were not in a wasteful code, but now we are, update the freeze count
        If Not (myOpCodeList(localCurrentOpCodeNdx).opCodeTypeName = "Waste" Or myOpCodeList(localCurrentOpCodeNdx).opCodeTypeName = "Setup") And
            (myOpCodeList(localNewOpCodeNdx).opCodeTypeName = "Waste" Or myOpCodeList(localNewOpCodeNdx).opCodeTypeName = "Setup") Then
            frmMain.f.lblFreezeCount.Text = LastQtyToGo
        End If

        Json_Structures_Cls.post_sfdcEvents(lstNewEvent) 'Will Return True If Successfull POST, You Can Use Your Own Variable If You See A Need For It
        If (Json_Structures_Cls.bWebResponseSuccess = True) Then
            ' MsgBox("Successfull POST")
        Else
            ' MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If

        If eventType = 21 Then 'Only update actuals on an event of 21
            DoPostBackActuals(newConfigNdx, localNewOpCodeNdx)
        End If
        WriteApplicationLog("MODPUBLIC DoPostBackCostEvent - Exited")
    End Sub

    Public Sub DoPostBackActuals(Optional configIDPost As Long = -99, Optional opCodeIDPost As Long = -99, Optional QtyToGo As Long = -99)
        If Not OKFORPOSTBACK Then Exit Sub

        'If we do not have a task loaded and we've already posted once that we do not have one loaded, then don't bother posting again.
        If currenttaskID = 0 And UPDATE_ACTUALS_PRIOR_TASKID = 0 Then Exit Sub
        UPDATE_ACTUALS_PRIOR_TASKID = currenttaskID

        WriteApplicationLog("MODPUBLIC DoPostBackActuals - Entered")

        Dim localOpCode As Long = IIf(opCodeIDPost = -99, currentOpCodeNDX, opCodeIDPost)
        Dim localConfig As Long = IIf(configIDPost = -99, currentconfigNDX, configIDPost)
        Dim localQtyToGo As Long = IIf(QtyToGo = -99, CurrentQtyToGo, QtyToGo)

        Dim lstNewEvent As Json_Structures_Cls.struct_POST_updateActuals '<- Make Sure You Set The Correct Structure, In This Case We Want struct_POST_timesheetEvents

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() ' Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        Dim currSpeed As Integer = Val(GetSetting("iVu", "Config", "CurrentAvg")) 'Computer\HKEY_CURRENT_USER\Software\VB and VBA Program Settings\iVu\Config\CurrentAvg
        Dim avgCopySpeed As Integer = Val(GetSetting("DCL360_Machine_Counts", "Jobs\" & GetSetting("iVu", "Config", "JobNumber") & "\Totals\All_Shifts", "RunAverage"))

        With lstNewEvent
            .id = 1
            .taskId = currenttaskID
            .avgCopySpeed = avgCopySpeed 'currSpeed
            .lastGCSpeed = currSpeed
            .copiesPerCycle = noUp
            .inProgress = True
            .taskComplete = False
            .grossNoUp = 1
            .grossCollect = 1
            .netNoUp = 1
            .netCollect = 1
            .configName = myConfigList(localConfig).ConfigName
            .quantityToGo = localQtyToGo
            .OpCodeID = myOpCodeList(localOpCode).opCodeID
            .MRWaste = 0
            .RunWaste = WastePieces
        End With ''"{"id":0,"resourceId":120,"eventType":"Login","operatorId":456,"eventDateTime":"2018-04-18T14:53:01.517"}

        Json_Structures_Cls.post_updateActuals(lstNewEvent) 'Will Return True If Successfull POST, You Can Use Your Own Variable If You See A Need For It
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            ' MsgBox("Successfull POST")
        Else
            ' MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If
        WriteApplicationLog("MODPUBLIC DoPostBackActuals - Exited")
    End Sub


    Public Function DoPostWIPConsume(ByVal barcodeID As String) As String
        If Not OKFORPOSTBACK Then
            Return ""
            Exit Function
        End If
        WriteApplicationLog("MODPUBLIC DoPostWIPConsume - Entered")
        Dim lstNewEvent As Json_Structures_Cls.struct_POST_WIPConsume '<- Make Sure You Set The Correct Structure

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() ' Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        With lstNewEvent
            lstNewEvent.system = "VisualMail"
            lstNewEvent.barcode = barcodeID
            lstNewEvent.dateTime = Format(Now(), "yyyy-MM-ddTHH:mm:ss")
            lstNewEvent.operatorId = CURRENTOPERATORID
            lstNewEvent.jobNo = currenttaskID
            'lstNewEvent.pieces = ""
            'lstNewEvent.noUp = ""
            lstNewEvent.resourceId = RESOURCEID
            lstNewEvent.method = "FULL"

        End With

        Json_Structures_Cls.post_WIPConsume(lstNewEvent) 'Will Return True If Successfull POST, You Can Use Your Own Variable If You See A Need For It
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            Return "Pallet Consumed"
        Else
            If Json_Structures_Cls.strWebResponseErrorMessage.Contains("400") Then
                Return "Pallet already consumed"
            Else
                Return "Pallet not found"
            End If
            'MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If
        WriteApplicationLog("MODPUBLIC DoPostWIPConsume - Exited")
    End Function

    Public Function DoPostWIPCreate(ByVal partRefID As String, ByVal quantity As String, ByVal noUp As String, Optional ByVal startNo As String = "0", Optional ByVal endNo As String = "0") As String
        If Not OKFORPOSTBACK Then
            Return ""
            Exit Function
        End If
        WriteApplicationLog("MODPUBLIC DoPostWIPCreate - Entered")
        Dim lstNewEvent As Json_Structures_Cls.struct_POST_WIPCreate '<- Make Sure You Set The Correct Structure

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() ' Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        With lstNewEvent
            lstNewEvent.system = "VisualMail"
            lstNewEvent.partRef = partRefID
            lstNewEvent.dateTime = Format(Now(), "yyyy-MM-ddTHH:mm:ss")
            lstNewEvent.operatorId = CURRENTOPERATORID
            lstNewEvent.pieces = IIf(quantity = "", 0, quantity)
            lstNewEvent.noUp = noUp
            lstNewEvent.startRecordNo = IIf(startNo = "", 0, startNo)
            lstNewEvent.endRecordNo = IIf(endNo = "", 0, endNo)
            'lstNewEvent.docsPerRecord = 
            lstNewEvent.resourceId = RESOURCEID
            lstNewEvent.loadFlagTemplate = TEMPLATEPATH
            lstNewEvent.printerUNC = RESOURCEPRINTER '"\\sg360-print-01\Gilman (Left) DP Canon iR-ADV C7200"
            lstNewEvent.loadFlagQtyToPrint = LOADFLAGQTY
            lstNewEvent.bartenderCommanderPath = BARTENDERPATH

        End With

        Json_Structures_Cls.post_WIPCreate(lstNewEvent) 'Will Return True If Successfull POST, You Can Use Your Own Variable If You See A Need For It
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            ' MsgBox("Successfull POST")
            Return "New Pallet Created " & WIP_NEWPALLET
        Else
            Return "Part Ref Not Found"
            'MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If
        WriteApplicationLog("MODPUBLIC DoPostWIPCreate - Exited")
    End Function

    Public Sub CheckFont(ByRef myLabel As Label)
        If IAMLOADING Then Exit Sub
        WriteApplicationLog("MODPUBLIC CheckFont - Entered")
        myLabel.Font = ORGLabelSize 'Reset the font to its original size

        Dim tmpLbl As New Label
        tmpLbl.Width = (myLabel.Width * 0.93)
        tmpLbl.Text = myLabel.Text

        Dim g As Graphics = tmpLbl.CreateGraphics()

        Dim sngFontSize As Single = myLabel.Font.Size
        Dim fntText As Font

        Do
            'Dim newStringFormat As New StringFormat
            'newStringFormat.FormatFlags = StringFormatFlags.DirectionVertical
            'Dim numOfCharInString As Integer

            fntText = New Font(myLabel.Font.FontFamily, sngFontSize)
            'Dim newHeight As Long = CInt(g.MeasureString(tmpLbl.Text, fntText, tmpLbl.Size, newStringFormat, numOfCharInString, New Integer).Height)
            tmpLbl.Height = CInt(g.MeasureString(tmpLbl.Text, fntText, tmpLbl.Width).Height)

            'If (newHeight > (myLabel.Height) Or numOfCharInString < myLabel.Text.Length) Then
            If (tmpLbl.Height > (myLabel.Height)) Then
                sngFontSize = 0.99F * sngFontSize
            Else
                ' frmMain.f.Controls.Add(tmpLbl)
                Exit Do
            End If
        Loop
        g.Dispose()
        myLabel.Font = New Font(myLabel.Font.FontFamily, Math.Floor(sngFontSize))



        '    Dim sngFontSize As Single = myLabel.Font.Size
        'Dim fntText As Font
        'Dim areaAvailable As New Size(myLabel.ClientSize.Width * 0.93, Integer.MaxValue)
        'Dim areaRequired As Size
        'Do
        '    fntText = New Font(myLabel.Font.FontFamily, sngFontSize)
        '    areaRequired = TextRenderer.MeasureText(myLabel.CreateGraphics, myLabel.Text, fntText, areaAvailable, TextFormatFlags.WordBreak Or TextFormatFlags.NoPadding)
        '    If (areaRequired.Height > myLabel.ClientSize.Height) Then
        '        sngFontSize = 0.99F * sngFontSize
        '    Else
        '        Exit Do
        '    End If
        'Loop
        'myLabel.Font = fntText

        WriteApplicationLog("MODPUBLIC CheckFont - Exited")
    End Sub

    Public Sub WriteApplicationLog(logEntry As String)
        If DoNotWriteApplicationLog Then Exit Sub
        Try
            Dim outTs As New IO.StreamWriter(strDC360RootPath & strDC360ApplicationLogFileName, True, System.Text.Encoding.Default)
            outTs.WriteLine(Now().ToString("yyyy-MM-dd-HH-mm-ss") & vbTab & logEntry)
            outTs.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub AdjustWidthComboBox_DropDown(ByRef senderComboBox As ComboBox)
        Dim width As Integer = senderComboBox.DropDownWidth
        Dim g As Graphics = senderComboBox.CreateGraphics()
        Dim aFont As Font = senderComboBox.Font
        Dim vertScrollBarWidth As Integer = 0
        If senderComboBox.Items.Count > senderComboBox.MaxDropDownItems Then
            vertScrollBarWidth = SystemInformation.VerticalScrollBarWidth
        End If
        Dim newWidth As Integer
        For Each s As String In senderComboBox.Items
            newWidth = g.MeasureString(s, aFont).Width + vertScrollBarWidth
            If (width < newWidth) Then
                width = newWidth
            End If
        Next
        senderComboBox.DropDownWidth = width
    End Sub

    Private Sub DialogTask(message As String)

        WriteApplicationLog("MODPUBLIC DialogTask - Entered")
        Dim tmpBox As New frmDialog(message)
        tmpBox.StartPosition = FormStartPosition.CenterScreen
        'tmpBox.Show()
        Application.Run(tmpBox)
        WriteApplicationLog("MODPUBLIC DialogTask - Exited")
    End Sub

    Public Function GetAdminLogin(changeReason As String) As String
        Dim AdminFrm As New frmKeyPad("Admin", changeReason)
        AdminFrm.StartPosition = FormStartPosition.CenterScreen
        Dim bkgrndOp As New BackgroundOpacity
        bkgrndOp.Show()
        AdminFrm.ShowDialog()
        bkgrndOp.Dispose()
        Select Case AdminFrm.myResult
            Case -1 'Admin ID Good
                Return AdminFrm.txtCode.Text
            Case -2 'Cancel Event
                Return ""
        End Select
        Return ""
    End Function

    Public Function GetVersionFromSubJob(subjob As String) As String
        WriteApplicationLog("FORM1 GetVersionFromSubJob - Entered")
        Dim dashNdx As Long = InStr(subjob, "-")
        If dashNdx <= 0 Then Return ""
        Return subjob.Substring(dashNdx)
        WriteApplicationLog("FORM1 GetVersionFromSubJob - Exited")
    End Function
End Module
