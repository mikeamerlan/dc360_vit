﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnMinimize = New System.Windows.Forms.Button()
        Me.tmrCount = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblLargeClient = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblLargeVersion = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblLargeJobName = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblLargeJobNumber = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblCount = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.grpSmall = New System.Windows.Forms.GroupBox()
        Me.cmbSmallConfig = New System.Windows.Forms.ComboBox()
        Me.cmbSmallAssistants = New System.Windows.Forms.ComboBox()
        Me.btnShowDetails = New System.Windows.Forms.Button()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.lblSmallJobNumber = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.lblSmallJobName = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.lblSmallVersion = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.lblSmallClient = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.grpJobInfo = New System.Windows.Forms.GroupBox()
        Me.btnChangeJob = New System.Windows.Forms.Button()
        Me.grpOperations = New System.Windows.Forms.GroupBox()
        Me.cmbConfig = New System.Windows.Forms.ComboBox()
        Me.cmbOpCode = New System.Windows.Forms.ComboBox()
        Me.cmbAssistants = New System.Windows.Forms.ComboBox()
        Me.cmbNumberUp = New System.Windows.Forms.ComboBox()
        Me.lblNoUp = New System.Windows.Forms.Label()
        Me.grpMetrics = New System.Windows.Forms.GroupBox()
        Me.lblFreezeCount = New System.Windows.Forms.Label()
        Me.lblComplete = New System.Windows.Forms.Label()
        Me.pgrStatus = New System.Windows.Forms.ProgressBar()
        Me.lblError = New System.Windows.Forms.Label()
        Me.btnGoodToWaste = New System.Windows.Forms.Button()
        Me.tmrShiftChange = New System.Windows.Forms.Timer(Me.components)
        Me.tmrUpdateInterval = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.tmrCountTimer = New System.Timers.Timer()
        Me.tmrShiftChangeTimer = New System.Timers.Timer()
        Me.tmrUpdateIntervalTimer = New System.Timers.Timer()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.tmrUpdateActualsTimer = New System.Timers.Timer()
        Me.btnWIP = New System.Windows.Forms.Button()
        Me.tmrRePostFailed = New System.Timers.Timer()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.grpSmall.SuspendLayout()
        Me.grpJobInfo.SuspendLayout()
        Me.grpOperations.SuspendLayout()
        Me.grpMetrics.SuspendLayout()
        CType(Me.tmrCountTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmrShiftChangeTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmrUpdateIntervalTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmrUpdateActualsTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmrRePostFailed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnMinimize
        '
        Me.btnMinimize.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMinimize.Location = New System.Drawing.Point(783, 329)
        Me.btnMinimize.Name = "btnMinimize"
        Me.btnMinimize.Size = New System.Drawing.Size(107, 35)
        Me.btnMinimize.TabIndex = 3
        Me.btnMinimize.Text = "Minimize"
        Me.btnMinimize.UseVisualStyleBackColor = True
        '
        'tmrCount
        '
        Me.tmrCount.Interval = 500
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 25)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Job Number:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(116, 25)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Qty To Go:"
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblUser.Location = New System.Drawing.Point(146, 27)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(103, 25)
        Me.lblUser.TabIndex = 8
        Me.lblUser.Text = "Luis"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(38, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 25)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Operator:"
        '
        'lblLargeClient
        '
        Me.lblLargeClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLargeClient.Location = New System.Drawing.Point(366, 67)
        Me.lblLargeClient.Name = "lblLargeClient"
        Me.lblLargeClient.Size = New System.Drawing.Size(339, 25)
        Me.lblLargeClient.TabIndex = 10
        Me.lblLargeClient.Text = "Leo Burnett"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(287, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 25)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Client:"
        '
        'lblLargeVersion
        '
        Me.lblLargeVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLargeVersion.Location = New System.Drawing.Point(146, 67)
        Me.lblLargeVersion.Name = "lblLargeVersion"
        Me.lblLargeVersion.Size = New System.Drawing.Size(135, 25)
        Me.lblLargeVersion.TabIndex = 12
        Me.lblLargeVersion.Text = "F1R3"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(49, 67)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(91, 25)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Version:"
        '
        'lblLargeJobName
        '
        Me.lblLargeJobName.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLargeJobName.Location = New System.Drawing.Point(366, 27)
        Me.lblLargeJobName.Name = "lblLargeJobName"
        Me.lblLargeJobName.Size = New System.Drawing.Size(339, 40)
        Me.lblLargeJobName.TabIndex = 14
        Me.lblLargeJobName.Text = "Test Script:MR-0:Resource-AT-13:Config-AT-4:JobPlan-1796:QTY-300000"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(286, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(74, 25)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Name:"
        '
        'lblLargeJobNumber
        '
        Me.lblLargeJobNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLargeJobNumber.Location = New System.Drawing.Point(146, 27)
        Me.lblLargeJobNumber.Name = "lblLargeJobNumber"
        Me.lblLargeJobNumber.Size = New System.Drawing.Size(134, 25)
        Me.lblLargeJobNumber.TabIndex = 16
        Me.lblLargeJobNumber.Text = "12345"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(280, 65)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(80, 25)
        Me.Label23.TabIndex = 28
        Me.Label23.Text = "Config:"
        '
        'lblCount
        '
        Me.lblCount.AutoSize = True
        Me.lblCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCount.Location = New System.Drawing.Point(128, 27)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(24, 25)
        Me.lblCount.TabIndex = 27
        Me.lblCount.Text = "0"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(23, 65)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(117, 25)
        Me.Label26.TabIndex = 31
        Me.Label26.Text = "Assistants:"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(255, 27)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(105, 25)
        Me.Label28.TabIndex = 29
        Me.Label28.Text = "OP Code:"
        '
        'grpSmall
        '
        Me.grpSmall.Controls.Add(Me.cmbSmallConfig)
        Me.grpSmall.Controls.Add(Me.cmbSmallAssistants)
        Me.grpSmall.Controls.Add(Me.btnShowDetails)
        Me.grpSmall.Controls.Add(Me.Label29)
        Me.grpSmall.Controls.Add(Me.Label32)
        Me.grpSmall.Controls.Add(Me.lblSmallJobNumber)
        Me.grpSmall.Controls.Add(Me.Label34)
        Me.grpSmall.Controls.Add(Me.lblSmallJobName)
        Me.grpSmall.Controls.Add(Me.Label36)
        Me.grpSmall.Controls.Add(Me.lblSmallVersion)
        Me.grpSmall.Controls.Add(Me.Label38)
        Me.grpSmall.Controls.Add(Me.lblSmallClient)
        Me.grpSmall.Controls.Add(Me.Label44)
        Me.grpSmall.Location = New System.Drawing.Point(896, 12)
        Me.grpSmall.Name = "grpSmall"
        Me.grpSmall.Size = New System.Drawing.Size(243, 417)
        Me.grpSmall.TabIndex = 32
        Me.grpSmall.TabStop = False
        '
        'cmbSmallConfig
        '
        Me.cmbSmallConfig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSmallConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSmallConfig.FormattingEnabled = True
        Me.cmbSmallConfig.Items.AddRange(New Object() {"Wafersealing", "Attaching"})
        Me.cmbSmallConfig.Location = New System.Drawing.Point(13, 339)
        Me.cmbSmallConfig.Name = "cmbSmallConfig"
        Me.cmbSmallConfig.Size = New System.Drawing.Size(224, 33)
        Me.cmbSmallConfig.TabIndex = 36
        '
        'cmbSmallAssistants
        '
        Me.cmbSmallAssistants.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSmallAssistants.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSmallAssistants.FormattingEnabled = True
        Me.cmbSmallAssistants.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6"})
        Me.cmbSmallAssistants.Location = New System.Drawing.Point(168, 283)
        Me.cmbSmallAssistants.Name = "cmbSmallAssistants"
        Me.cmbSmallAssistants.Size = New System.Drawing.Size(50, 33)
        Me.cmbSmallAssistants.TabIndex = 35
        '
        'btnShowDetails
        '
        Me.btnShowDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowDetails.Location = New System.Drawing.Point(48, 374)
        Me.btnShowDetails.Name = "btnShowDetails"
        Me.btnShowDetails.Size = New System.Drawing.Size(155, 37)
        Me.btnShowDetails.TabIndex = 48
        Me.btnShowDetails.Text = "Show Details"
        Me.btnShowDetails.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(45, 286)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(117, 25)
        Me.Label29.TabIndex = 47
        Me.Label29.Text = "Assistants:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(6, 312)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(80, 25)
        Me.Label32.TabIndex = 44
        Me.Label32.Text = "Config:"
        '
        'lblSmallJobNumber
        '
        Me.lblSmallJobNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSmallJobNumber.Location = New System.Drawing.Point(138, 103)
        Me.lblSmallJobNumber.Name = "lblSmallJobNumber"
        Me.lblSmallJobNumber.Size = New System.Drawing.Size(99, 25)
        Me.lblSmallJobNumber.TabIndex = 43
        Me.lblSmallJobNumber.Text = "12345"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(6, 153)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(74, 25)
        Me.Label34.TabIndex = 42
        Me.Label34.Text = "Name:"
        '
        'lblSmallJobName
        '
        Me.lblSmallJobName.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSmallJobName.Location = New System.Drawing.Point(82, 153)
        Me.lblSmallJobName.Name = "lblSmallJobName"
        Me.lblSmallJobName.Size = New System.Drawing.Size(155, 102)
        Me.lblSmallJobName.TabIndex = 41
        Me.lblSmallJobName.Text = "Marlboro Click to Mail February"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(6, 128)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(91, 25)
        Me.Label36.TabIndex = 40
        Me.Label36.Text = "Version:"
        '
        'lblSmallVersion
        '
        Me.lblSmallVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSmallVersion.Location = New System.Drawing.Point(143, 128)
        Me.lblSmallVersion.Name = "lblSmallVersion"
        Me.lblSmallVersion.Size = New System.Drawing.Size(94, 25)
        Me.lblSmallVersion.TabIndex = 39
        Me.lblSmallVersion.Text = "F1R3"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(6, 16)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(73, 25)
        Me.Label38.TabIndex = 38
        Me.Label38.Text = "Client:"
        '
        'lblSmallClient
        '
        Me.lblSmallClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSmallClient.Location = New System.Drawing.Point(76, 16)
        Me.lblSmallClient.Name = "lblSmallClient"
        Me.lblSmallClient.Size = New System.Drawing.Size(161, 87)
        Me.lblSmallClient.TabIndex = 37
        Me.lblSmallClient.Text = "Leo Burnett"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(6, 103)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(134, 25)
        Me.Label44.TabIndex = 32
        Me.Label44.Text = "Job Number:"
        '
        'grpJobInfo
        '
        Me.grpJobInfo.Controls.Add(Me.btnChangeJob)
        Me.grpJobInfo.Controls.Add(Me.Label1)
        Me.grpJobInfo.Controls.Add(Me.lblLargeClient)
        Me.grpJobInfo.Controls.Add(Me.Label8)
        Me.grpJobInfo.Controls.Add(Me.lblLargeVersion)
        Me.grpJobInfo.Controls.Add(Me.Label10)
        Me.grpJobInfo.Controls.Add(Me.lblLargeJobName)
        Me.grpJobInfo.Controls.Add(Me.Label12)
        Me.grpJobInfo.Controls.Add(Me.lblLargeJobNumber)
        Me.grpJobInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpJobInfo.Location = New System.Drawing.Point(12, 12)
        Me.grpJobInfo.Name = "grpJobInfo"
        Me.grpJobInfo.Size = New System.Drawing.Size(878, 113)
        Me.grpJobInfo.TabIndex = 33
        Me.grpJobInfo.TabStop = False
        Me.grpJobInfo.Text = "Job Information:"
        '
        'btnChangeJob
        '
        Me.btnChangeJob.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeJob.Location = New System.Drawing.Point(711, 27)
        Me.btnChangeJob.Name = "btnChangeJob"
        Me.btnChangeJob.Size = New System.Drawing.Size(147, 65)
        Me.btnChangeJob.TabIndex = 17
        Me.btnChangeJob.Text = "Change/End Job"
        Me.btnChangeJob.UseVisualStyleBackColor = True
        '
        'grpOperations
        '
        Me.grpOperations.Controls.Add(Me.cmbConfig)
        Me.grpOperations.Controls.Add(Me.cmbOpCode)
        Me.grpOperations.Controls.Add(Me.cmbAssistants)
        Me.grpOperations.Controls.Add(Me.Label6)
        Me.grpOperations.Controls.Add(Me.lblUser)
        Me.grpOperations.Controls.Add(Me.Label23)
        Me.grpOperations.Controls.Add(Me.Label26)
        Me.grpOperations.Controls.Add(Me.Label28)
        Me.grpOperations.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpOperations.Location = New System.Drawing.Point(12, 131)
        Me.grpOperations.Name = "grpOperations"
        Me.grpOperations.Size = New System.Drawing.Size(878, 111)
        Me.grpOperations.TabIndex = 34
        Me.grpOperations.TabStop = False
        Me.grpOperations.Text = "Operations:"
        '
        'cmbConfig
        '
        Me.cmbConfig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbConfig.FormattingEnabled = True
        Me.cmbConfig.Items.AddRange(New Object() {"Wafersealing", "Attaching"})
        Me.cmbConfig.Location = New System.Drawing.Point(366, 62)
        Me.cmbConfig.Name = "cmbConfig"
        Me.cmbConfig.Size = New System.Drawing.Size(268, 33)
        Me.cmbConfig.TabIndex = 34
        '
        'cmbOpCode
        '
        Me.cmbOpCode.BackColor = System.Drawing.SystemColors.Control
        Me.cmbOpCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOpCode.FormattingEnabled = True
        Me.cmbOpCode.Items.AddRange(New Object() {"Running", "MakeReady", "Samples", "Maintenance", "NoWork"})
        Me.cmbOpCode.Location = New System.Drawing.Point(366, 24)
        Me.cmbOpCode.Name = "cmbOpCode"
        Me.cmbOpCode.Size = New System.Drawing.Size(268, 33)
        Me.cmbOpCode.TabIndex = 33
        '
        'cmbAssistants
        '
        Me.cmbAssistants.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAssistants.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAssistants.FormattingEnabled = True
        Me.cmbAssistants.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6"})
        Me.cmbAssistants.Location = New System.Drawing.Point(146, 62)
        Me.cmbAssistants.Name = "cmbAssistants"
        Me.cmbAssistants.Size = New System.Drawing.Size(50, 33)
        Me.cmbAssistants.TabIndex = 32
        '
        'cmbNumberUp
        '
        Me.cmbNumberUp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbNumberUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumberUp.FormattingEnabled = True
        Me.cmbNumberUp.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"})
        Me.cmbNumberUp.Location = New System.Drawing.Point(840, 390)
        Me.cmbNumberUp.Name = "cmbNumberUp"
        Me.cmbNumberUp.Size = New System.Drawing.Size(50, 33)
        Me.cmbNumberUp.TabIndex = 36
        '
        'lblNoUp
        '
        Me.lblNoUp.AutoSize = True
        Me.lblNoUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoUp.Location = New System.Drawing.Point(708, 393)
        Me.lblNoUp.Name = "lblNoUp"
        Me.lblNoUp.Size = New System.Drawing.Size(126, 25)
        Me.lblNoUp.TabIndex = 35
        Me.lblNoUp.Text = "Number Up:"
        '
        'grpMetrics
        '
        Me.grpMetrics.Controls.Add(Me.lblFreezeCount)
        Me.grpMetrics.Controls.Add(Me.lblComplete)
        Me.grpMetrics.Controls.Add(Me.pgrStatus)
        Me.grpMetrics.Controls.Add(Me.Label2)
        Me.grpMetrics.Controls.Add(Me.lblCount)
        Me.grpMetrics.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMetrics.Location = New System.Drawing.Point(12, 249)
        Me.grpMetrics.Name = "grpMetrics"
        Me.grpMetrics.Size = New System.Drawing.Size(878, 74)
        Me.grpMetrics.TabIndex = 35
        Me.grpMetrics.TabStop = False
        Me.grpMetrics.Text = "Metrics:"
        '
        'lblFreezeCount
        '
        Me.lblFreezeCount.AutoSize = True
        Me.lblFreezeCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFreezeCount.Location = New System.Drawing.Point(128, 27)
        Me.lblFreezeCount.Name = "lblFreezeCount"
        Me.lblFreezeCount.Size = New System.Drawing.Size(24, 25)
        Me.lblFreezeCount.TabIndex = 30
        Me.lblFreezeCount.Text = "0"
        Me.lblFreezeCount.Visible = False
        '
        'lblComplete
        '
        Me.lblComplete.AutoSize = True
        Me.lblComplete.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComplete.Location = New System.Drawing.Point(311, 15)
        Me.lblComplete.Name = "lblComplete"
        Me.lblComplete.Size = New System.Drawing.Size(140, 25)
        Me.lblComplete.TabIndex = 29
        Me.lblComplete.Text = "0% Complete"
        '
        'pgrStatus
        '
        Me.pgrStatus.Location = New System.Drawing.Point(260, 43)
        Me.pgrStatus.Name = "pgrStatus"
        Me.pgrStatus.Size = New System.Drawing.Size(260, 23)
        Me.pgrStatus.TabIndex = 28
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError.Location = New System.Drawing.Point(7, 379)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(123, 50)
        Me.lblError.TabIndex = 31
        Me.lblError.Text = "There are 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Assistants"
        Me.lblError.Visible = False
        '
        'btnGoodToWaste
        '
        Me.btnGoodToWaste.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoodToWaste.Location = New System.Drawing.Point(12, 329)
        Me.btnGoodToWaste.Name = "btnGoodToWaste"
        Me.btnGoodToWaste.Size = New System.Drawing.Size(210, 35)
        Me.btnGoodToWaste.TabIndex = 28
        Me.btnGoodToWaste.Text = "GOOD <-> WASTE"
        Me.btnGoodToWaste.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(406, 326)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(56, 47)
        Me.Button1.TabIndex = 36
        Me.Button1.Text = "Reduce Count by 1000"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(461, 326)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(90, 23)
        Me.Button2.TabIndex = 37
        Me.Button2.Text = "Vit Job Change"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(461, 349)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(91, 23)
        Me.Button3.TabIndex = 38
        Me.Button3.Text = "Vit Good Waste"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(550, 326)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(82, 23)
        Me.Button4.TabIndex = 39
        Me.Button4.Text = "Vit OpCode+1"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(550, 349)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(82, 23)
        Me.Button5.TabIndex = 40
        Me.Button5.Text = "Vit User"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(632, 328)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(56, 21)
        Me.Button6.TabIndex = 41
        Me.Button6.Text = "Vit Close"
        Me.Button6.UseVisualStyleBackColor = True
        Me.Button6.Visible = False
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(632, 351)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(56, 21)
        Me.Button7.TabIndex = 42
        Me.Button7.Text = "Vit On"
        Me.Button7.UseVisualStyleBackColor = True
        Me.Button7.Visible = False
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(317, 326)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(90, 23)
        Me.Button8.TabIndex = 43
        Me.Button8.Text = "Vit Minor Stop"
        Me.Button8.UseVisualStyleBackColor = True
        Me.Button8.Visible = False
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(317, 351)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(90, 23)
        Me.Button9.TabIndex = 44
        Me.Button9.Text = "Vit Config+1"
        Me.Button9.UseVisualStyleBackColor = True
        Me.Button9.Visible = False
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(228, 352)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(90, 23)
        Me.Button10.TabIndex = 45
        Me.Button10.Text = "Emerg Job On"
        Me.Button10.UseVisualStyleBackColor = True
        Me.Button10.Visible = False
        '
        'tmrCountTimer
        '
        Me.tmrCountTimer.Enabled = True
        Me.tmrCountTimer.Interval = 500.0R
        Me.tmrCountTimer.SynchronizingObject = Me
        '
        'tmrShiftChangeTimer
        '
        Me.tmrShiftChangeTimer.SynchronizingObject = Me
        '
        'tmrUpdateIntervalTimer
        '
        Me.tmrUpdateIntervalTimer.SynchronizingObject = Me
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(228, 326)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(83, 23)
        Me.Button11.TabIndex = 46
        Me.Button11.Text = "Vit Running"
        Me.Button11.UseVisualStyleBackColor = True
        Me.Button11.Visible = False
        '
        'tmrUpdateActualsTimer
        '
        Me.tmrUpdateActualsTimer.Enabled = True
        Me.tmrUpdateActualsTimer.Interval = 300000.0R
        Me.tmrUpdateActualsTimer.SynchronizingObject = Me
        '
        'btnWIP
        '
        Me.btnWIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWIP.Location = New System.Drawing.Point(694, 329)
        Me.btnWIP.Name = "btnWIP"
        Me.btnWIP.Size = New System.Drawing.Size(81, 35)
        Me.btnWIP.TabIndex = 47
        Me.btnWIP.Text = "WIP"
        Me.btnWIP.UseVisualStyleBackColor = True
        '
        'tmrRePostFailed
        '
        Me.tmrRePostFailed.Enabled = True
        Me.tmrRePostFailed.Interval = 1800000.0R
        Me.tmrRePostFailed.SynchronizingObject = Me
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(687, 355)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(90, 23)
        Me.Button12.TabIndex = 48
        Me.Button12.Text = "Vit NoUp+1"
        Me.Button12.UseVisualStyleBackColor = True
        Me.Button12.Visible = False
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(778, 359)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(56, 21)
        Me.Button13.TabIndex = 49
        Me.Button13.Text = "Bad OP"
        Me.Button13.UseVisualStyleBackColor = True
        Me.Button13.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1147, 435)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.cmbNumberUp)
        Me.Controls.Add(Me.lblNoUp)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.btnWIP)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnGoodToWaste)
        Me.Controls.Add(Me.grpMetrics)
        Me.Controls.Add(Me.grpOperations)
        Me.Controls.Add(Me.grpJobInfo)
        Me.Controls.Add(Me.grpSmall)
        Me.Controls.Add(Me.btnMinimize)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "DC 360"
        Me.TopMost = True
        Me.grpSmall.ResumeLayout(False)
        Me.grpSmall.PerformLayout()
        Me.grpJobInfo.ResumeLayout(False)
        Me.grpJobInfo.PerformLayout()
        Me.grpOperations.ResumeLayout(False)
        Me.grpOperations.PerformLayout()
        Me.grpMetrics.ResumeLayout(False)
        Me.grpMetrics.PerformLayout()
        CType(Me.tmrCountTimer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmrShiftChangeTimer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmrUpdateIntervalTimer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmrUpdateActualsTimer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmrRePostFailed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnMinimize As Button
    Friend WithEvents tmrCount As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents lblLargeClient As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblLargeVersion As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lblLargeJobName As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lblLargeJobNumber As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents lblCount As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents grpSmall As GroupBox
    Friend WithEvents btnShowDetails As Button
    Friend WithEvents Label29 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents lblSmallJobNumber As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents lblSmallJobName As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents lblSmallVersion As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents lblSmallClient As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents grpJobInfo As GroupBox
    Friend WithEvents btnChangeJob As Button
    Friend WithEvents grpOperations As GroupBox
    Friend WithEvents grpMetrics As GroupBox
    Friend WithEvents cmbConfig As ComboBox
    Friend WithEvents cmbOpCode As ComboBox
    Friend WithEvents cmbAssistants As ComboBox
    Friend WithEvents btnGoodToWaste As Button
    Friend WithEvents lblComplete As Label
    Friend WithEvents pgrStatus As ProgressBar
    Friend WithEvents cmbSmallConfig As ComboBox
    Friend WithEvents cmbSmallAssistants As ComboBox
    Friend WithEvents tmrShiftChange As Timer
    Friend WithEvents tmrUpdateInterval As Timer
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents lblUser As Label
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents lblFreezeCount As Label
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents lblError As Label
    Friend WithEvents tmrCountTimer As Timers.Timer
    Friend WithEvents tmrShiftChangeTimer As Timers.Timer
    Friend WithEvents tmrUpdateIntervalTimer As Timers.Timer
    Friend WithEvents Button11 As Button
    Friend WithEvents tmrUpdateActualsTimer As Timers.Timer
    Friend WithEvents btnWIP As Button
    Friend WithEvents tmrRePostFailed As Timers.Timer
    Friend WithEvents cmbNumberUp As ComboBox
    Friend WithEvents lblNoUp As Label
    Friend WithEvents Button12 As Button
    Friend WithEvents Button13 As Button
End Class
