﻿Public Class ucJobRow
    Private aParentController As frmChangeJob
    Private myNDX As Long
    Public selected As Boolean = False
    Public myOrgColor As Color = Me.BackColor
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Public Sub New(ParentController As frmChangeJob, NDX As Long)
        WriteApplicationLog("UCJOBROW New 1 - Entered")

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        aParentController = ParentController
        myNDX = NDX
        WriteApplicationLog("UCJOBROW New 1 - Exited")
    End Sub
    Public Sub New(ParentController As frmChangeJob, NDX As Long, jobNumber As String, jobName As String, company As String)
        WriteApplicationLog("UCJOBROW New 2 - Entered")

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            aParentController = ParentController
            myNDX = NDX
            lblJobNumber.Text = jobNumber
            lblTitle.Text = jobName
            lblCompany.Text = company
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'Try
        '    aParentController = ParentController
        '    MessageBox.Show("Index: " & NDX) : myNDX = NDX
        '    MessageBox.Show("JobNumber: " & jobNumber) : lblJobNumber.Text = jobNumber
        '    MessageBox.Show("Title: " & jobName) : lblTitle.Text = jobName
        '    MessageBox.Show("Company: " & company) : lblCompany.Text = company
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try

        WriteApplicationLog("UCJOBROW New 1 - Exited")
    End Sub
    Private Sub ucJobRow_Click(sender As Object, e As EventArgs) Handles MyBase.Click, lblJobNumber.Click, lblTitle.Click, lblCompany.Click
        WriteApplicationLog("UCJOBROW ucJobRow_Click - Entered")
        selected = Not selected 'if selected was true, then this is a deselect so set it to false
        aParentController.JobClicked(myNDX)
        WriteApplicationLog("UCJOBROW ucJobRow_Click - Exited")
    End Sub

    Private Sub lbl_TextChanged(sender As Object, e As EventArgs) Handles lblTitle.TextChanged, lblCompany.TextChanged, lblJobNumber.TextChanged
        WriteApplicationLog("UCJOBROW lbl_TextChanged - Entered")
        CheckFont(sender)
        WriteApplicationLog("UCJOBROW lbl_TextChanged - Exited")
    End Sub
End Class
