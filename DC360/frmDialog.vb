﻿Public Class frmDialog
    Dim messageText As String = ""
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Public Sub New(message As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        messageText = message
    End Sub

    Private Sub frmDialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label6.Text = messageText
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Me.BackColor = Color.Red Then
            Me.BackColor = Color.Yellow
        Else
            Me.BackColor = Color.Red
        End If

    End Sub
End Class