﻿Imports System.ComponentModel

Public Class frmChangeJob
    Dim localJobRows() As ucJobRow
    Dim deselectedBackColor As Color
    Public myResult As Long = 0
    Dim ascendFlag As String = ""

    Structure DummyJob
        Dim jobNumber As String
        Dim jobName As String
        Dim company As String
        Dim taskID As String
        Dim plannedCopies As String
        Dim qtytoGo As String
        Dim wasteCopies As String
        Dim configID As String
        Dim makeReadyMins As String
        Dim status As String
        Dim noUp As String
        Dim jobSpeed As String
        Dim orderNo As String
        Dim staff As String
        Dim process As String
    End Structure

    Dim dummyJobs() As DummyJob
    Public finalJob As New DummyJob
    Public emergencyJob As New DummyJob

    Public Sub New()

        WriteApplicationLog("FRMCHANGEJOB NEW - Entered")
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        PopulateDummyJobs()
        PopulateFinalJob()
        PopulateEmergencyJob()
        PopulateResources()
        ' PopulateJobs() - This is now done when from PopulateResources
        WriteApplicationLog("FRMCHANGEJOB NEW - Exited")

    End Sub

    Private Sub frmChangeJob_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WriteApplicationLog("FRMCHANGEJOB frmChangeJob_Load - Entered")
        If SUBJOBNO = "No Selection-No Selection" Or SUBJOBNO Is Nothing Then
            btnStartJob.Visible = True
            btnLift.Visible = False
            btnStop.Visible = False
        Else
            btnStartJob.Visible = False
            btnLift.Visible = True
            btnStop.Visible = True
        End If


        PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB frmChangeJob_Load - Exited")

    End Sub
    Private Sub PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB PopulateGrid - Entered")
        pnlJobGrid.Controls.Clear()
        Dim CurrJobRow As New ucJobRow(Me, -1, SUBJOBNO, currentJobName, currentClient)
        deselectedBackColor = CurrJobRow.BackColor 'Save the back color for selection of jobs
        If Not SUBJOBNO = "No Selection-No Selection" And Not SUBJOBNO Is Nothing Then
            CurrJobRow.Left = 15
            CurrJobRow.Top = 37
            CurrJobRow.BackColor = Color.LightBlue
            CurrJobRow.myOrgColor = Color.LightBlue
            Me.Controls.Add(CurrJobRow)
        End If

        Dim controlTopNdx As Long = 0
        For i As Long = 0 To dummyJobs.GetUpperBound(0)
            If dummyJobs(i).jobNumber = "" Then Continue For

            Dim tmpJobRow As New ucJobRow(Me, i, dummyJobs(i).jobNumber, dummyJobs(i).jobName, dummyJobs(i).company)
            Select Case dummyJobs(i).status
                Case "Lifted" : tmpJobRow.myOrgColor = Color.Yellow
                Case "Complete" : tmpJobRow.myOrgColor = Color.Red
                Case "Running" : tmpJobRow.myOrgColor = Color.Green
                Case Else : tmpJobRow.myOrgColor = deselectedBackColor
            End Select
            ReDim Preserve localJobRows(i)
            localJobRows(i) = tmpJobRow
            tmpJobRow.BackColor = tmpJobRow.myOrgColor
            If txtJobSearch.Text.Trim <> "" Then
                If Not (dummyJobs(i).jobNumber.ToUpper.Contains(txtJobSearch.Text.Trim.ToUpper) Or
                   dummyJobs(i).jobName.ToUpper.Contains(txtJobSearch.Text.Trim.ToUpper) Or
                   dummyJobs(i).company.ToUpper.Contains(txtJobSearch.Text.Trim.ToUpper) Or
                   dummyJobs(i).jobName = "Emergency Job") Then
                    'Does not match the filter
                    Continue For
                End If
            End If
            tmpJobRow.Left = 3
            tmpJobRow.Top = ((3 + tmpJobRow.Height) * controlTopNdx) + 3 : controlTopNdx += 1
            pnlJobGrid.Controls.Add(tmpJobRow)
        Next

        VScrollBar1.Visible = pnlJobGrid.VerticalScroll.Visible
        If pnlJobGrid.VerticalScroll.Visible Then
            VScrollBar1.Maximum = pnlJobGrid.VerticalScroll.Maximum
            VScrollBar1.Minimum = pnlJobGrid.VerticalScroll.Minimum
            VScrollBar1.Value = pnlJobGrid.VerticalScroll.Value
            VScrollBar1.SmallChange = pnlJobGrid.VerticalScroll.SmallChange
            VScrollBar1.LargeChange = pnlJobGrid.VerticalScroll.LargeChange
        End If

        WriteApplicationLog("FRMCHANGEJOB PopulateGrid - Exited")
    End Sub
    Private Sub PopulateJobs(Optional myResourceID As String = "I've got a lovely bunch of coconuts...")
        WriteApplicationLog("FRMCHANGEJOB PopulateJobs - Entered")
        If myResourceID = "I've got a lovely bunch of coconuts..." Then myResourceID = RESOURCEID

        Dim lst As List(Of Json_Structures_Cls.cls_task_resource) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = myResourceID
        Dim strOptionalSearchString As String = "" 'SearchString_Tbox.Text
        'Dim bincludeCompleted As Nullable(Of Boolean)

        'If NotSet_Rdo.Checked = False Then bincludeCompleted = True_Rdo.Checked

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost       'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion 'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1
        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_task_resource
        '    Public taskID As Int32                          '424421
        '    Public resourceID As Int32                      '1
        '    Public resourceName As String                   '"Press25"
        '    Public startDateTime As String                  '"2017-08-07T07:00:00-06:00"
        '    Public endDateTime As String                    '"2017-08-09T15:00:00-06:00"
        '    Public subJobNo As String                       '"92672-001A"
        '    Public taskName As String                       '"Wal-Mart Mastercard Reissue Dual EMV Benefits Brochure"
        '    Public company As String                        '"string"
        '    Public orderNo As Int32                         '92672
        '    Public plannedCopies As Int32                   '3683000
        '    Public noUp As Int32                            '10
        '    Public makeReadyMins As Int32                   '1200
        '    Public runMins As Int32                         '2160
        '    Public noPasses As Int32                        '1
        '    Public speed As Int32                           '15675
        '    Public netImpressions As Nullable(Of Int32)     '*73000
        '    Public grossImpressions As Nullable(Of Int32)   '*184329
        '    Public wasteImpressions As Nullable(Of Int32)   '*111329
        '    Public netCopies As Nullable(Of Int32)          '*730000
        '    Public grossCopies As Nullable(Of Int32)        '*1113290
        '    Public wasteCopies As Nullable(Of Int32)        '*0
        '    Public qtytoGo As Int32                         '2953000
        '    Public taskStatus As String                     '"LIVE"
        '    Public totalSubJobQty As Int32                  '20000000
        '    Public configId As Int32                        '*23
        'End Class

        lst = Json_Structures_Cls.Get_task_resource(strResourceID, chkShowCompleted.Checked, strOptionalSearchString)

        If lst Is Nothing Then
            lst = PriorJobLst
        Else
            PriorJobLst = lst
        End If

        strA = "taskID,resourceID,resourceName,startDateTime,endDateTime,subJobNo,taskName,company,orderNo,plannedCopies,noUp,makeReadyMins,runMins,speed,netImpressions,grossImpressions,wasteImpressions,netCopies,grossCopies,wasteCopies,qtytoGo,taskStatus,totalSubJobQty,configId" & vbCrLf
        ReDim dummyJobs(0)
        For iLoop = 0 To (lst.Count - 1)
            If currenttaskID = lst(iLoop).taskID Then Continue For 'ignore the job we are currently working on

            dummyJobs(dummyJobs.GetUpperBound(0)).noUp = lst(iLoop).noUp
            dummyJobs(dummyJobs.GetUpperBound(0)).jobNumber = lst(iLoop).subJobNo
            dummyJobs(dummyJobs.GetUpperBound(0)).jobName = lst(iLoop).taskName
            dummyJobs(dummyJobs.GetUpperBound(0)).taskID = lst(iLoop).taskID
            dummyJobs(dummyJobs.GetUpperBound(0)).company = lst(iLoop).company
            dummyJobs(dummyJobs.GetUpperBound(0)).plannedCopies = lst(iLoop).plannedCopies
            dummyJobs(dummyJobs.GetUpperBound(0)).qtytoGo = lst(iLoop).qtytoGo
            dummyJobs(dummyJobs.GetUpperBound(0)).wasteCopies = lst(iLoop).wasteCopies
            dummyJobs(dummyJobs.GetUpperBound(0)).configID = lst(iLoop).configId
            dummyJobs(dummyJobs.GetUpperBound(0)).makeReadyMins = lst(iLoop).makeReadyMins
            dummyJobs(dummyJobs.GetUpperBound(0)).status = lst(iLoop).taskStatus
            dummyJobs(dummyJobs.GetUpperBound(0)).jobSpeed = lst(iLoop).speed
            dummyJobs(dummyJobs.GetUpperBound(0)).orderNo = lst(iLoop).orderNo
            dummyJobs(dummyJobs.GetUpperBound(0)).staff = lst(iLoop).staff
            dummyJobs(dummyJobs.GetUpperBound(0)).process = lst(iLoop).process

            ReDim Preserve dummyJobs(dummyJobs.GetUpperBound(0) + 1)

            'strA += lst(iLoop).taskID & "|"
            'strA += lst(iLoop).resourceID & "|"
            'strA += lst(iLoop).resourceName & "|"
            'strA += lst(iLoop).startDateTime & "|"
            'strA += lst(iLoop).endDateTime & "|"
            'strA += lst(iLoop).subJobNo & "|"
            'strA += lst(iLoop).taskName & "|"
            'strA += lst(iLoop).company & "|"
            'strA += lst(iLoop).orderNo & "|"
            'strA += lst(iLoop).plannedCopies & "|"
            'strA += lst(iLoop).noUp & "|"
            'strA += lst(iLoop).makeReadyMins & "|"
            'strA += lst(iLoop).runMins & "|"
            'strA += lst(iLoop).speed & "|"
            'strA += lst(iLoop).netImpressions & "|"
            'strA += lst(iLoop).grossImpressions & "|"
            'strA += lst(iLoop).wasteImpressions & "|"
            'strA += lst(iLoop).netCopies & "|"
            'strA += lst(iLoop).grossCopies & "|"
            'strA += lst(iLoop).wasteCopies & "|"
            'strA += lst(iLoop).qtytoGo & "|"
            'strA += lst(iLoop).taskStatus & "|"
            'strA += lst(iLoop).totalSubJobQty & "|"
            'strA += lst(iLoop).configId & "|"
        Next
        ' MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " cls_task_resource")
        dummyJobs(dummyJobs.GetUpperBound(0)) = emergencyJob
        'If dummyJobs.GetUpperBound(0) > 0 Then 'there is at least one entry
        '    'So remove the blank at the end.
        '    ReDim Preserve dummyJobs(dummyJobs.GetUpperBound(0) - 1)
        'End If

        WriteApplicationLog("FRMCHANGEJOB PopulateJobs - Exited")
    End Sub
    Private Sub PopulateDummyJobs()
        WriteApplicationLog("FRMCHANGEJOB PopulateDummyJobs - Entered")
        ReDim dummyJobs(5)

        dummyJobs(0).jobNumber = "12345-F1R3"
        dummyJobs(0).jobName = "Test Job 1"
        dummyJobs(0).company = "SG360"

        dummyJobs(1).jobNumber = "67890-F2R5"
        dummyJobs(1).jobName = "Test Job 2 Has a Really Really Long Job Name"
        dummyJobs(1).company = "SG360"

        dummyJobs(2).jobNumber = "66291-F44R55"
        dummyJobs(2).jobName = "Black and Mild CTM September 2017"
        dummyJobs(2).company = "Leo Burnett"

        dummyJobs(3).jobNumber = "92513-F22R333"
        dummyJobs(3).jobName = "Mid-Month Postcard"
        dummyJobs(3).company = "LogicSource"

        dummyJobs(4).jobNumber = "95043-F2R4"
        dummyJobs(4).jobName = "MileagePlus_PreQual File 3"
        dummyJobs(4).company = "Wunderman"

        dummyJobs(5).jobNumber = "97778-F99R3"
        dummyJobs(5).jobName = "ECOLAB"
        dummyJobs(5).company = "WALSWORTH PUBLISHING CO_ INC_"

        WriteApplicationLog("FRMCHANGEJOB PopulateDummyJobs - Exited")
    End Sub

    Private Sub PopulateResources()
        WriteApplicationLog("FRMCHANGEJOB PopulateResources - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_similarResources) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost       'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion 'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_similarResources
        '    Public resourceID As Int32                  '41
        '    Public resourceName As String               '"AT-1", "Press28"
        '    Public locID As Int32                       'null, 3201
        '    Public locName As String                    'null,"Folder 1"
        '    Public deptCode As String                   '"121"
        '    Public deptName As String                   '"Attachers","Folding"
        '    Public deptType As Int32                    '1 = Prepress, 2 = Press, 3 = PostPress
        '    Public facilityID As Int32                  '15
        '    Public facilityName As String               '"Wolf Rd"
        '    Public pcFriendlyName As String             '"2 devices - AT-1 and AT-2"
        '    Public pcPrimaryMacAddress As String        '"10.60.8.66"
        '    Public pcSecondaryMacAddress As String      'null, 10.60.3.111
        'End Class
        lst = Json_Structures_Cls.Get_similarResources(strResourceID)
        strA = "resourceID,resourceName,locID,deptCode,deptName,deptType,facilityID,facilityName,pcFriendlyName,pcPrimaryMacAddress,pcSecondaryMacAddress" & vbCrLf

        cmbResource.Items.Clear()

        Dim resourceDictionary As New Dictionary(Of String, String)
        resourceDictionary.Add(RESOURCEID, RESOURCENAME)
        For iLoop = 0 To (lst.Count - 1)
            If resourceDictionary.ContainsKey(lst(iLoop).resourceID) Then Continue For
            resourceDictionary.Add(lst(iLoop).resourceID, lst(iLoop).resourceName)

            ' cmbResource.Items.Add(lst(iLoop).resourceID & "_" & lst(iLoop).resourceName)

            'strA += lst(iLoop).resourceID & "|"
            'strA += lst(iLoop).resourceName & "|"
            'strA += lst(iLoop).locID & "|"
            'strA += lst(iLoop).deptCode & "|"
            'strA += lst(iLoop).deptName & "|"
            'strA += lst(iLoop).deptType & "|"
            'strA += lst(iLoop).facilityID & "|"
            'strA += lst(iLoop).facilityName & "|"
            'strA += lst(iLoop).pcFriendlyName & "|"
            'strA += lst(iLoop).pcPrimaryMacAddress & "|"
            'strA += lst(iLoop).pcSecondaryMacAddress & vbCrLf
        Next

        cmbResource.DataSource = New BindingSource(resourceDictionary, Nothing)
        cmbResource.DisplayMember = "Value"
        cmbResource.ValueMember = "Key"

        For i As Long = 0 To cmbResource.Items.Count - 1
            If DirectCast(cmbResource.Items(i), KeyValuePair(Of String, String)).Key = RESOURCEID Then
                cmbResource.SelectedIndex = i
                Exit For
            End If
        Next
        If cmbResource.SelectedIndex = 0 Then
            PopulateJobs(DirectCast(cmbResource.SelectedItem, KeyValuePair(Of String, String)).Key)
            PopulateGrid()
        End If
        'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " similarResources")
        WriteApplicationLog("FRMCHANGEJOB PopulateResources - Exited")
    End Sub

    Private Sub PopulateFinalJob()
        WriteApplicationLog("FRMCHANGEJOB PopulateFinalJob - Entered")
        finalJob.jobNumber = "No Selection-No Selection"
        finalJob.jobName = "No Selection"
        finalJob.company = "No Selection"
        finalJob.plannedCopies = "0"
        finalJob.qtytoGo = "0"
        finalJob.wasteCopies = "0"
        finalJob.configID = "0"
        finalJob.makeReadyMins = "0"
        finalJob.noUp = "1"
        finalJob.jobSpeed = "0"
        finalJob.orderNo = "0"
        finalJob.staff = "0"
        finalJob.process = ""
        WriteApplicationLog("FRMCHANGEJOB PopulateFinalJob - Exited")

    End Sub
    Private Sub PopulateEmergencyJob()
        WriteApplicationLog("FRMCHANGEJOB PopulateEmergencyJob - Entered")
        emergencyJob.jobNumber = "Emergency Job"
        emergencyJob.jobName = "Emergency Job"
        emergencyJob.company = "Emergency Job"
        emergencyJob.plannedCopies = "0"
        emergencyJob.qtytoGo = "0"
        emergencyJob.wasteCopies = "0"
        emergencyJob.configID = "0"
        emergencyJob.makeReadyMins = "0"
        emergencyJob.noUp = "1"
        emergencyJob.jobSpeed = "0"
        emergencyJob.orderNo = "0"
        emergencyJob.staff = "0"
        emergencyJob.process = ""
        WriteApplicationLog("FRMCHANGEJOB PopulateEmergencyJob - Exited")

    End Sub
    Public Sub JobClicked(ndx As Long)
        If ndx = -1 Then Exit Sub 'Current Job
        WriteApplicationLog("FRMCHANGEJOB JobClicked - Entered")
        For i As Long = 0 To localJobRows.GetUpperBound(0)
            If localJobRows(i) Is Nothing Then Continue For
            If localJobRows(i).selected And i = ndx Then
                localJobRows(i).BackColor = Color.LightGreen
            Else
                localJobRows(i).BackColor = localJobRows(i).myOrgColor
                localJobRows(i).selected = False
            End If
        Next
        WriteApplicationLog("FRMCHANGEJOB JobClicked - Exited")

    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        WriteApplicationLog("FRMCHANGEJOB btnStop_Click - Entered")

        If Not localJobRows Is Nothing Then
            For i As Long = 0 To localJobRows.GetUpperBound(0)
                If localJobRows(i).selected Then
                    If dummyJobs(i).status = "Running" Then
                        MessageBox.Show("Warning:  This job is running on another machine." & vbCrLf & "You cannot load a job that is running.", "Job Already Open", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    Exit For
                End If
            Next
        End If

        If MessageBox.Show(Me, "Do you wish to end the current job?", "Confirm End Job", vbYesNo, MessageBoxIcon.Question) = DialogResult.No Then
            WriteApplicationLog("FRMCHANGEJOB btnStop_Click - Exited")
            Exit Sub
        End If
        If Not localJobRows Is Nothing Then 'If jobs are available
            For i As Long = 0 To localJobRows.GetUpperBound(0)
                If localJobRows(i).selected Then
                    finalJob = dummyJobs(i)
                    Exit For
                End If
            Next
        End If
        myResult = 0 'Job Ended
        WriteApplicationLog("FRMCHANGEJOB btnStop_Click - Exited")
        Me.Close()
    End Sub

    Private Sub btnLift_Click(sender As Object, e As EventArgs) Handles btnLift.Click
        WriteApplicationLog("FRMCHANGEJOB btnLift_Click - Entered")

        If Not localJobRows Is Nothing Then
            For i As Long = 0 To localJobRows.GetUpperBound(0)
                If localJobRows(i).selected Then
                    If dummyJobs(i).status = "Running" Then
                        MessageBox.Show("Warning:  This job is running on another machine." & vbCrLf & "You cannot load a job that is running.", "Job Already Open", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    Exit For
                End If
            Next
        End If

        If MessageBox.Show(Me, "Do you wish to pause the current job?", "Confirm Pause Job", vbYesNo, MessageBoxIcon.Question) = DialogResult.No Then
            WriteApplicationLog("FRMCHANGEJOB btnLift_Click - Exited")
            Exit Sub
        End If
        If Not localJobRows Is Nothing Then
            For i As Long = 0 To localJobRows.GetUpperBound(0)
                If localJobRows(i).selected Then
                    finalJob = dummyJobs(i)
                    Exit For
                End If
            Next
        End If
        myResult = -1 'Job Lift
        WriteApplicationLog("FRMCHANGEJOB btnLift_Click - Exited")
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        WriteApplicationLog("FRMCHANGEJOB btnCancel_Click - Entered")
        myResult = -2
        WriteApplicationLog("FRMCHANGEJOB btnCancel_Click - Exited")
        Me.Close()
    End Sub

    Private Sub chkShowCompleted_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowCompleted.CheckedChanged
        WriteApplicationLog("FRMCHANGEJOB chkShowCompleted_CheckedChanged - Entered")
        PopulateJobs(DirectCast(cmbResource.SelectedItem, KeyValuePair(Of String, String)).Key)
        PopulateFinalJob()
        PopulateEmergencyJob()
        PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB chkShowCompleted_CheckedChanged - Exited")
    End Sub

    Private Sub cmbResource_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbResource.SelectedIndexChanged
        WriteApplicationLog("FRMCHANGEJOB cmbResource_SelectedIndexChanged - Entered")
        PopulateJobs(DirectCast(cmbResource.SelectedItem, KeyValuePair(Of String, String)).Key)
        PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB cmbResource_SelectedIndexChanged - Exited")
    End Sub

    Private Sub lblJobNumber_Click(sender As Object, e As EventArgs) Handles lblJobNumber.Click
        WriteApplicationLog("FRMCHANGEJOB lblJobNumber_Click - Entered")
        If ascendFlag = "JobA" Then 'Sort Descending
            For i As Long = 0 To dummyJobs.GetUpperBound(0)
                For j As Long = i + 1 To dummyJobs.GetUpperBound(0)
                    If dummyJobs(i).jobNumber < dummyJobs(j).jobNumber Then
                        Dim tmpJob As New DummyJob
                        tmpJob = dummyJobs(i)
                        dummyJobs(i) = dummyJobs(j)
                        dummyJobs(j) = tmpJob
                    End If
                Next
            Next
            ascendFlag = ""
        Else
            For i As Long = 0 To dummyJobs.GetUpperBound(0)
                For j As Long = i + 1 To dummyJobs.GetUpperBound(0)
                    If dummyJobs(i).jobNumber > dummyJobs(j).jobNumber Then
                        Dim tmpJob As New DummyJob
                        tmpJob = dummyJobs(i)
                        dummyJobs(i) = dummyJobs(j)
                        dummyJobs(j) = tmpJob
                    End If
                Next
            Next
            ascendFlag = "JobA"
        End If
        PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB lblJobNumber_Click - Exited")
    End Sub

    Private Sub lblCompany_Click(sender As Object, e As EventArgs) Handles lblCompany.Click
        WriteApplicationLog("FRMCHANGEJOB lblCompany_Click - Entered")
        If ascendFlag = "CompanyA" Then 'Sort Descending
            For i As Long = 0 To dummyJobs.GetUpperBound(0)
                For j As Long = i + 1 To dummyJobs.GetUpperBound(0)
                    If dummyJobs(i).company < dummyJobs(j).company Then
                        Dim tmpJob As New DummyJob
                        tmpJob = dummyJobs(i)
                        dummyJobs(i) = dummyJobs(j)
                        dummyJobs(j) = tmpJob
                    End If
                Next
            Next
            ascendFlag = ""
        Else
            For i As Long = 0 To dummyJobs.GetUpperBound(0)
                For j As Long = i + 1 To dummyJobs.GetUpperBound(0)
                    If dummyJobs(i).company > dummyJobs(j).company Then
                        Dim tmpJob As New DummyJob
                        tmpJob = dummyJobs(i)
                        dummyJobs(i) = dummyJobs(j)
                        dummyJobs(j) = tmpJob
                    End If
                Next
            Next
            ascendFlag = "CompanyA"
        End If

        PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB lblCompany_Click - Exited")
    End Sub

    Private Sub lblTitle_Click(sender As Object, e As EventArgs) Handles lblTitle.Click
        WriteApplicationLog("FRMCHANGEJOB lblTitle_Click - Entered")
        If ascendFlag = "TitleA" Then 'Sort Descending
            For i As Long = 0 To dummyJobs.GetUpperBound(0)
                For j As Long = i + 1 To dummyJobs.GetUpperBound(0)
                    If dummyJobs(i).jobName < dummyJobs(j).jobName Then
                        Dim tmpJob As New DummyJob
                        tmpJob = dummyJobs(i)
                        dummyJobs(i) = dummyJobs(j)
                        dummyJobs(j) = tmpJob
                    End If
                Next
            Next
            ascendFlag = ""
        Else
            For i As Long = 0 To dummyJobs.GetUpperBound(0)
                For j As Long = i + 1 To dummyJobs.GetUpperBound(0)
                    If dummyJobs(i).jobName > dummyJobs(j).jobName Then
                        Dim tmpJob As New DummyJob
                        tmpJob = dummyJobs(i)
                        dummyJobs(i) = dummyJobs(j)
                        dummyJobs(j) = tmpJob
                    End If
                Next
            Next
            ascendFlag = "TitleA"
        End If


        PopulateGrid()
        WriteApplicationLog("FRMCHANGEJOB lblTitle_Click - Exited")
    End Sub

    Private Sub btnStartJob_Click(sender As Object, e As EventArgs) Handles btnStartJob.Click
        WriteApplicationLog("FRMCHANGEJOB btnStartJob_Click - Entered")
        myResult = -2 'cancel - If an actual job is selected it will use that.
        If Not localJobRows Is Nothing Then 'Jobs available to select
            For i As Long = 0 To localJobRows.GetUpperBound(0)
                If localJobRows(i) Is Nothing Then Continue For
                If localJobRows(i).selected Then
                    If dummyJobs(i).status = "Running" Then
                        MessageBox.Show("Warning:  This job is running on another machine." & vbCrLf & "You cannot load a job that is running.", "Job Already Open", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    finalJob = dummyJobs(i)
                    myResult = -3 'Job Start
                    Exit For
                End If
            Next
        End If

        WriteApplicationLog("FRMCHANGEJOB btnStartJob_Click - Exited")
        Me.Close()
    End Sub

    Private Sub VScrollBar1_Scroll(sender As Object, e As ScrollEventArgs) Handles VScrollBar1.Scroll
        pnlJobGrid.VerticalScroll.Value = VScrollBar1.Value
    End Sub

    Private Sub frmChangeJob_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing

        WriteApplicationLog("FRMCHANGEJOB frmChangeJob_Closing - Entered")
        If finalJob.jobName = "Emergency Job" Then

            Me.Visible = False

            Dim anEmergencyJobFrm As New frmEmergJob
            anEmergencyJobFrm.StartPosition = FormStartPosition.CenterScreen
            anEmergencyJobFrm.ShowDialog()
            Me.Visible = True
            If anEmergencyJobFrm.emergJob.jobName Is Nothing Then
                PopulateFinalJob()
                e.Cancel = True
                Exit Sub
            Else
                finalJob.company = anEmergencyJobFrm.emergJob.company
                finalJob.jobName = anEmergencyJobFrm.emergJob.jobName
                finalJob.jobNumber = anEmergencyJobFrm.emergJob.jobNumber
                finalJob.makeReadyMins = 0
                finalJob.noUp = anEmergencyJobFrm.emergJob.noUp
                finalJob.plannedCopies = anEmergencyJobFrm.emergJob.qtytoGo
                finalJob.qtytoGo = anEmergencyJobFrm.emergJob.qtytoGo
                finalJob.wasteCopies = 0
                finalJob.jobSpeed = "0"
                finalJob.orderNo = "0"
                finalJob.staff = "0"
                finalJob.process = ""

SearchForAnotherTask:
                finalJob.taskID = (Now().ToString("MMddhhmmss") * -1)

                Dim lst As List(Of Json_Structures_Cls.cls_tasks) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
                Dim iLoop As Integer = 0
                Dim strA As String = ""
                Dim strTaskID As String = finalJob.taskID

                Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
                Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
                Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
                Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

                lst = Json_Structures_Cls.Get_tasks(strTaskID)
                If Not lst Is Nothing Then 'If the task already exists, choose another
                    GoTo SearchForAnotherTask
                End If

            End If
        Else
            If finalJob.jobNumber <> "No Selection-No Selection" Then SaveSetting("DCL360_Machine_Counts", "Jobs\" & finalJob.jobNumber.Split("-")(0) & "-" & GetVersionFromSubJob(finalJob.jobNumber), "EmergencyJob", "False")
        End If
            WriteApplicationLog("FRMCHANGEJOB frmChangeJob_Closing - Exited")
    End Sub

    Private Sub txtJobSearch_KeyUp(sender As Object, e As KeyEventArgs) Handles txtJobSearch.KeyUp
        PopulateGrid()
    End Sub
End Class