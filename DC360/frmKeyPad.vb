﻿Imports System.ComponentModel

Public Class frmKeyPad
    Public myResult As Long = -2 'This is the default value for CANCEL.  If the form is closed, it will use this value
    Public myKeyedEntry As String = ""
    Private localKeyType As String = ""
    Public Sub New()

        WriteApplicationLog("FRMKEYPAD New Basic - Entered")
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        WriteApplicationLog("FRMKEYPAD New Basic - Exited")

    End Sub
    Public Sub New(keyType As String, Optional priorValue As String = "", Optional desiredValue As String = "")
        WriteApplicationLog("FRMKEYPAD New Parameters - Entered")

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        localKeyType = keyType

        Select Case keyType
            Case "Login"
                lblMsg1.Text = "Current User: " & priorValue
                lblMsg2.Text = ""
            Case "Admin"
                txtCode.PasswordChar = "*"
                btnAdmin.Enabled = False
                lblMsg1.Text = "Administrator code needed to"
                lblMsg2.Text = priorValue
            Case "GoodWaste"
                lblMsg1.Text = priorValue
                lblMsg2.Text = desiredValue
                lblGoodCopies.Text &= " " & vbCrLf & (TotalPieces - LastQtyToGo)
                lblWasteCopies.Text &= " " & vbCrLf & WastePieces
                lblGoodCopies.Visible = True
                lblWasteCopies.Visible = True
        End Select

        btnLogin.Visible = IIf(keyType = "Login", True, False) : btnLogin.Left = 67 : btnLogin.Top = 350
        btnLogout.Visible = IIf(keyType = "Login", True, False) : btnLogout.Left = 179 : btnLogout.Top = 350
        btnAdmin.Visible = IIf(keyType = "Admin", True, False) : btnAdmin.Left = 67 : btnAdmin.Top = 350
        btnToGood.Visible = IIf(keyType = "GoodWaste", True, False) : btnToGood.Left = 67 : btnToGood.Top = 350
        btnToWaste.Visible = IIf(keyType = "GoodWaste", True, False) : btnToWaste.Left = 179 : btnToWaste.Top = 350

        Me.Height = 445
        WriteApplicationLog("FRMKEYPAD New Parameters - Exited")
    End Sub


    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        WriteApplicationLog("FRMKEYPAD btnLogin_Click - Entered")
        'Check for valid login
        If txtCode.Text = "" Or txtCode.Text = CURRENTOPERATORID Then 'No Reason to log in the current individual
            myResult = -2 'cancel
            Me.Close()
        Else
            'if Valid
            myResult = txtCode.Text
            Me.Close()
        End If

        WriteApplicationLog("FRMKEYPAD btnLogin_Click - Exited")
    End Sub

    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        WriteApplicationLog("FRMKEYPAD btnLogout_Click - Entered")
        If CURRENTOPERATORID = "" Then 'no one to log out
            myResult = -2 'Cancel
        Else
            myResult = -1
        End If
        WriteApplicationLog("FRMKEYPAD btnLogout_Click - Exited")
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        WriteApplicationLog("FRMKEYPAD btnCancel_Click - Entered")
        myResult = -2
        WriteApplicationLog("FRMKEYPAD btnCancel_Click - Exited")
        Me.Close()
    End Sub

    Private Sub btnAdmin_Click(sender As Object, e As EventArgs) Handles btnAdmin.Click
        WriteApplicationLog("FRMKEYPAD btnAdmin_Click - Entered")
        'Check for valid admin code
        Dim lst As List(Of Json_Structures_Cls.cls_admins) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_admins
        '    Public id As Int32          '36
        '    Public userName As String   '"Mark Anderson"
        '    Public pin As Int32         '12345678"
        'End Class

        Dim goodAdmin As Boolean = False
        lst = Json_Structures_Cls.Get_admins()
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            strA = "id,userName,pin" & vbCrLf
            For iLoop = 0 To (lst.Count - 1)
                'strA += lst(iLoop).id & "|"
                'strA += lst(iLoop).userName & "|"
                'strA += lst(iLoop).pin & vbCrLf

                If txtCode.Text = lst(iLoop).pin Then
                    goodAdmin = True
                    Exit For
                End If
            Next
            'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " admins")
        Else
            'MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
            MessageBox.Show(Me, Json_Structures_Cls.strWebResponseErrorMessage, "Web Response Error", vbOKOnly, MessageBoxIcon.Error)
            Exit Sub
        End If


        If goodAdmin Then 'valid
            myResult = -1
        Else
            'MsgBox("Invalid admin pin.  Please try again.", MsgBoxStyle.SystemModal, "Web Response Error - Invalid Admin Pin")
            MessageBox.Show(Me, "Invalid admin pin.  Please try again.", "Web Response Error - Invalid Admin Pin", vbOKOnly, MessageBoxIcon.Error)
            txtCode.Text = ""
            Exit Sub
        End If

        WriteApplicationLog("FRMKEYPAD btnAdmin_Click - Exited")
        Me.Close()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        WriteApplicationLog("FRMKEYPAD btnClear_Click - Entered")
        txtCode.Text = ""
        WriteApplicationLog("FRMKEYPAD btnClear_Click - Exited")
    End Sub

    Private Sub btnNumbers_Click(sender As Object, e As EventArgs) Handles btn0.Click, btn1.Click, btn2.Click, btn3.Click, btn4.Click, btn5.Click, btn6.Click, btn7.Click, btn8.Click, btn9.Click
        WriteApplicationLog("FRMKEYPAD btnNumbers_Click - Entered")
        txtCode.Text &= sender.text
        WriteApplicationLog("FRMKEYPAD btnNumbers_Click - Exited")
    End Sub

    Private Sub btnToGood_Click(sender As Object, e As EventArgs) Handles btnToGood.Click
        WriteApplicationLog("FRMKEYPAD btnToGood_Click - Entered")
        'if valid amount
        If txtCode.Text <> "" Then
            myResult = -1
            myKeyedEntry = txtCode.Text
        Else
            myResult = -2 'Cancel
        End If
        WriteApplicationLog("FRMKEYPAD btnNumbers_Click - Exited")
        Me.Close()
    End Sub

    Private Sub btnToWaste_Click(sender As Object, e As EventArgs) Handles btnToWaste.Click
        WriteApplicationLog("FRMKEYPAD btnToWaste_Click - Entered")
        'if valid amount
        If txtCode.Text <> "" Then
            myResult = -3
            myKeyedEntry = txtCode.Text
        Else
            myResult = -2 'Cancel
        End If
        WriteApplicationLog("FRMKEYPAD btnToWaste_Click - Exited")
        Me.Close()
    End Sub

    Private Sub txtCode_TextChanged(sender As Object, e As EventArgs) Handles txtCode.TextChanged
        Select Case localKeyType
            Case "Admin"
                If txtCode.Text.Length < 2 Then
                    btnAdmin.Enabled = False
                Else
                    btnAdmin.Enabled = True
                End If
        End Select
    End Sub
End Class