﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmWIP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnCreate = New System.Windows.Forms.Button()
        Me.txtBarcode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtQuantity = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtStartRecNum = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtEndRecNum = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtNoUp = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblStatusConsume = New System.Windows.Forms.Label()
        Me.btnGoConsume = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbPartRef = New System.Windows.Forms.ComboBox()
        Me.cmbPrinter = New System.Windows.Forms.ComboBox()
        Me.lblStatusCreate = New System.Windows.Forms.Label()
        Me.cmbCreateType = New System.Windows.Forms.ComboBox()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCreate
        '
        Me.btnCreate.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreate.Location = New System.Drawing.Point(548, 139)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(113, 34)
        Me.btnCreate.TabIndex = 7
        Me.btnCreate.Text = "CREATE"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'txtBarcode
        '
        Me.txtBarcode.Location = New System.Drawing.Point(255, 28)
        Me.txtBarcode.Name = "txtBarcode"
        Me.txtBarcode.Size = New System.Drawing.Size(251, 31)
        Me.txtBarcode.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(91, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(158, 25)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Pallet Barcode:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(177, 25)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Part Ref Number:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(-1, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(185, 25)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Quantity (Copies):"
        '
        'txtQuantity
        '
        Me.txtQuantity.Location = New System.Drawing.Point(190, 35)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(185, 31)
        Me.txtQuantity.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(398, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(157, 25)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Start Rec Num:"
        '
        'txtStartRecNum
        '
        Me.txtStartRecNum.Location = New System.Drawing.Point(561, 6)
        Me.txtStartRecNum.Name = "txtStartRecNum"
        Me.txtStartRecNum.Size = New System.Drawing.Size(100, 31)
        Me.txtStartRecNum.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(405, 35)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(150, 25)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "End Rec Num:"
        '
        'txtEndRecNum
        '
        Me.txtEndRecNum.Location = New System.Drawing.Point(561, 32)
        Me.txtEndRecNum.Name = "txtEndRecNum"
        Me.txtEndRecNum.Size = New System.Drawing.Size(100, 31)
        Me.txtEndRecNum.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(58, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(126, 25)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Number Up:"
        '
        'txtNoUp
        '
        Me.txtNoUp.Location = New System.Drawing.Point(190, 64)
        Me.txtNoUp.Name = "txtNoUp"
        Me.txtNoUp.Size = New System.Drawing.Size(185, 31)
        Me.txtNoUp.TabIndex = 2
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(678, 222)
        Me.TabControl1.TabIndex = 15
        Me.TabControl1.TabStop = False
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lblStatusConsume)
        Me.TabPage1.Controls.Add(Me.btnGoConsume)
        Me.TabPage1.Controls.Add(Me.txtBarcode)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 34)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(670, 184)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Consume"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'lblStatusConsume
        '
        Me.lblStatusConsume.AutoSize = True
        Me.lblStatusConsume.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusConsume.Location = New System.Drawing.Point(6, 89)
        Me.lblStatusConsume.Name = "lblStatusConsume"
        Me.lblStatusConsume.Size = New System.Drawing.Size(79, 25)
        Me.lblStatusConsume.TabIndex = 17
        Me.lblStatusConsume.Text = "Status:"
        '
        'btnGoConsume
        '
        Me.btnGoConsume.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoConsume.Location = New System.Drawing.Point(546, 80)
        Me.btnGoConsume.Name = "btnGoConsume"
        Me.btnGoConsume.Size = New System.Drawing.Size(118, 34)
        Me.btnGoConsume.TabIndex = 5
        Me.btnGoConsume.TabStop = False
        Me.btnGoConsume.Text = "Consume"
        Me.btnGoConsume.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.cmbPartRef)
        Me.TabPage2.Controls.Add(Me.cmbPrinter)
        Me.TabPage2.Controls.Add(Me.lblStatusCreate)
        Me.TabPage2.Controls.Add(Me.cmbCreateType)
        Me.TabPage2.Controls.Add(Me.btnCreate)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.txtNoUp)
        Me.TabPage2.Controls.Add(Me.txtQuantity)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.txtEndRecNum)
        Me.TabPage2.Controls.Add(Me.txtStartRecNum)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Location = New System.Drawing.Point(4, 34)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(670, 184)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Create"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 106)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 25)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Printer:"
        '
        'cmbPartRef
        '
        Me.cmbPartRef.FormattingEnabled = True
        Me.cmbPartRef.Location = New System.Drawing.Point(190, 1)
        Me.cmbPartRef.Name = "cmbPartRef"
        Me.cmbPartRef.Size = New System.Drawing.Size(185, 33)
        Me.cmbPartRef.TabIndex = 0
        '
        'cmbPrinter
        '
        Me.cmbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinter.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPrinter.FormattingEnabled = True
        Me.cmbPrinter.Location = New System.Drawing.Point(91, 103)
        Me.cmbPrinter.Name = "cmbPrinter"
        Me.cmbPrinter.Size = New System.Drawing.Size(570, 33)
        Me.cmbPrinter.TabIndex = 6
        '
        'lblStatusCreate
        '
        Me.lblStatusCreate.AutoSize = True
        Me.lblStatusCreate.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusCreate.Location = New System.Drawing.Point(4, 148)
        Me.lblStatusCreate.Name = "lblStatusCreate"
        Me.lblStatusCreate.Size = New System.Drawing.Size(79, 25)
        Me.lblStatusCreate.TabIndex = 16
        Me.lblStatusCreate.Text = "Status:"
        '
        'cmbCreateType
        '
        Me.cmbCreateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCreateType.FormattingEnabled = True
        Me.cmbCreateType.Items.AddRange(New Object() {"Live", "Seed", "Sample"})
        Me.cmbCreateType.Location = New System.Drawing.Point(410, 64)
        Me.cmbCreateType.Name = "cmbCreateType"
        Me.cmbCreateType.Size = New System.Drawing.Size(251, 33)
        Me.cmbCreateType.TabIndex = 5
        '
        'PrintDocument1
        '
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'frmWIP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(705, 249)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWIP"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "WIP"
        Me.TopMost = True
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCreate As Button
    Friend WithEvents txtBarcode As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtQuantity As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtStartRecNum As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtEndRecNum As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtNoUp As TextBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents lblStatusCreate As Label
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents cmbPrinter As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbCreateType As ComboBox
    Friend WithEvents cmbPartRef As ComboBox
    Friend WithEvents btnGoConsume As Button
    Friend WithEvents lblStatusConsume As Label
End Class
