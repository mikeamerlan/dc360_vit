﻿Public Class frmEmergJob

    Public emergJob As New DummyJob
    Structure DummyJob
        Dim jobNumber As String
        Dim jobName As String
        Dim company As String
        Dim qtytoGo As String
        Dim noUp As String

    End Structure
    Private Sub btnCreateJob_Click(sender As Object, e As EventArgs) Handles btnCreateJob.Click
        WriteApplicationLog("FRMEMERGJOB btnCreateJob_Click - Entered")
        If iHasErrors() Then Exit Sub

        Dim adminCode As String = GetAdminLogin("create emergency jobs")

        If adminCode = "" Then
            Exit Sub
        End If

        emergJob.jobNumber = txtJobNumber.Text
        emergJob.jobName = txtJobName.Text
        emergJob.company = txtClientName.Text
        emergJob.qtytoGo = txtQtyToGo.Text
        emergJob.noUp = txtNumberUp.Text

        SaveSetting("DCL360_Machine_Counts", "Jobs\" & txtJobNumber.Text.Split("-")(0) & "-" & GetVersionFromSubJob(txtJobNumber.Text), "EmergencyJob", "True-" & adminCode)
        WriteApplicationLog("FRMEMERGJOB btnCreateJob_Click - Exited")
        Me.Close()
    End Sub

    Private Function iHasErrors() As Boolean
        WriteApplicationLog("FRMEMERGJOB iHasErrors - Entered")
        If txtJobNumber.Text = "" Then
            MessageBox.Show("Error: Job number Missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtJobName.Text = "" Then
            MessageBox.Show("Error: Job name Missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtClientName.Text = "" Then
            MessageBox.Show("Error: Client name Missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtNumberUp.Text = "" Then
            MessageBox.Show("Error: Number up Missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtQtyToGo.Text = "" Then
            MessageBox.Show("Error: Planned copies Missing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If Not IsNumeric(txtNumberUp.Text) Then
            MessageBox.Show("Error: Number up is not numeric.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If Not IsNumeric(txtQtyToGo.Text) Then
            MessageBox.Show("Error: Planned impressions is not numeric.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtNumberUp.Text = "0" Or (txtNumberUp.Text * 1) <= 0 Or txtNumberUp.Text.Contains(".") Or txtNumberUp.Text.Contains("%") Then
            MessageBox.Show("Error: Invalid Number Up.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtQtyToGo.Text = "0" Or (txtQtyToGo.Text * 1) < 0 Or txtQtyToGo.Text.Contains(".") Or txtQtyToGo.Text.Contains("%") Then
            MessageBox.Show("Error: Invalid Planned copies.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        If txtJobNumber.Text.Trim.Length < 5 Or txtJobNumber.Text.Trim.Length > 50 Then
            MessageBox.Show("Error: Job number must be between 5 and 50 characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        WriteApplicationLog("FRMEMERGJOB iHasErrors - Exited")
        Return False
    End Function

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        WriteApplicationLog("FRMEMERGJOB iHasErrors - Entered")

        WriteApplicationLog("FRMEMERGJOB iHasErrors - Exited")
        Me.Close()
    End Sub
End Class