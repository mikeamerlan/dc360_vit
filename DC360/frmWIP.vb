﻿Imports System.Drawing.Printing
Imports System.Drawing.Text
Imports System.Runtime.InteropServices
Public Class frmWIP

    'Dim partRefDividor As String = "—"
    Dim partRefDividor As String = "-"
    Dim fullPartRef As String = ""
    Dim cannotCreatePallets As Boolean = False
    Dim DefaultNdx As Long = 0
    Dim GeneralBackColor As Color
    Public Sub New(Optional WIPNDX As Long = 0)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DefaultNdx = WIPNDX
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click

        If cannotCreatePallets Then Exit Sub
        If IHasCreateErrors() Then Exit Sub

        fullPartRef = ORDERNO & partRefDividor & (RESOURCENAME & "  ").Substring(0, 2).Trim & partRefDividor & CleanTheText(cmbPartRef.Text) & partRefDividor & cmbCreateType.Text.ToUpper.Substring(0, 4)

        lblStatusCreate.Text = "Status: " & DoPostWIPCreate(fullPartRef, txtQuantity.Text, txtNoUp.Text, txtStartRecNum.Text, txtEndRecNum.Text)
        If WIP_NEWPALLET <> "" Then
            Me.BackColor = Color.Green
            WIP_NEWPALLET = WIP_NEWPALLET.Replace("""", "")

            'LABEL PRINT METHOD
            PrintDocument1.PrinterSettings.PrinterName = cmbPrinter.Text
            PrintDocument1.Print()
        Else
            Me.BackColor = Color.Red
        End If

        txtEndRecNum.Text = ""
        txtStartRecNum.Text = ""
        txtQuantity.Text = ""
        WIP_NUp = txtNoUp.Text
        WIP_PartRef = cmbPartRef.Text
        cmbPartRef.Focus()

    End Sub


    Private Sub btnGoConsume_Click(sender As Object, e As EventArgs) Handles btnGoConsume.Click
        If IHasConsumeErrors() Then Exit Sub
        lblStatusConsume.Text = "Status: " & DoPostWIPConsume(txtBarcode.Text)
        If lblStatusConsume.Text = "Status: Pallet Consumed" Then
            Me.BackColor = Color.Green
        Else
            Me.BackColor = Color.Red
        End If

        txtBarcode.Text = ""
        txtBarcode.Focus()
    End Sub

    Private Function IHasConsumeErrors() As Boolean
        If txtBarcode.Text.Trim = "" Then
            MessageBox.Show("ERROR: You must enter a pallet ID.", "CONSUME ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        Return False
    End Function
    Private Function IHasCreateErrors() As Boolean
        If cmbPartRef.Text.Trim = "" Then
            MessageBox.Show("ERROR: You must enter the partRef.", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If
        If txtNoUp.Text.Trim = "" Then
            MessageBox.Show("ERROR: You must enter the number up.", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        If CleanTheText(cmbPartRef.Text) = "" Then
            MessageBox.Show("ERROR: Your partRef does not contain any valid characters([A-Z][0-9]).", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        If Not NumberOrBlank(txtNoUp.Text) Then
            MessageBox.Show("ERROR: Your Number Up contains invalid characters.", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        If Not NumberOrBlank(txtQuantity.Text) Then
            MessageBox.Show("ERROR: Your quantity contains invalid characters.", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        If Not NumberOrBlank(txtStartRecNum.Text) Then
            MessageBox.Show("ERROR: Your start record number contains invalid characters.", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        If Not NumberOrBlank(txtEndRecNum.Text) Then
            MessageBox.Show("ERROR: Your end record number contains invalid characters.", "CREATE ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        End If

        Return False
    End Function


    Private Function CleanTheText(aStr As String) As String
        Dim retStr As String = ""

        For i As Long = 1 To aStr.Length
            Select Case Asc(Mid(aStr, i, 1).ToUpper)
                Case 48 To 57 '0 to 9
                    retStr &= Mid(aStr, i, 1)
                Case 65 To 90 'A to Z
                    retStr &= Mid(aStr, i, 1)
                Case Asc("-")
                    retStr &= "_" 'change - to _
            End Select
        Next

        Return retStr
    End Function

    Private Function NumberOrBlank(aStr As String) As Boolean
        If aStr = "" Then Return True
        If Not IsNumeric(aStr) Then Return False
        If Int(aStr) <> aStr * 1.0 Then Return False
        If aStr < 0 Then Return False

        Return True
    End Function

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim barcodeFontLarge As Font = GetInstance(50, FontStyle.Regular)
        Dim barcodeFontSmall As Font = GetInstance(15, FontStyle.Regular)

        'Draw Header
        e.Graphics.DrawString("Client: " & frmMain.f.lblLargeClient.Text, New Font("Courier", 18), Brushes.Black, 25, 20)

        'Draw Job Name
        e.Graphics.DrawString("Job Name: " & frmMain.f.lblLargeJobName.Text, New Font("Courier", 12), Brushes.Black, 25, 65)

        'Draw Large Barcode
        e.Graphics.DrawString("*" & WIP_NEWPALLET & "*", barcodeFontLarge, Brushes.Black, 20, 100)
        e.Graphics.DrawString("*" & WIP_NEWPALLET & "*", barcodeFontLarge, Brushes.Black, 20, 150)
        e.Graphics.DrawString("*" & WIP_NEWPALLET & "*", barcodeFontLarge, Brushes.Black, 20, 180)
        e.Graphics.DrawString(WIP_NEWPALLET, New Font("Courier", 12), Brushes.Black, 25, 240)

        'Draw Small Barcode
        e.Graphics.DrawString("*" & WIP_NEWPALLET & "*", barcodeFontSmall, Brushes.Black, 25, 300)
        e.Graphics.DrawString("*" & WIP_NEWPALLET & "*", barcodeFontSmall, Brushes.Black, 25, 315)
        e.Graphics.DrawString("*" & WIP_NEWPALLET & "*", barcodeFontSmall, Brushes.Black, 25, 330)
        e.Graphics.DrawString(WIP_NEWPALLET, New Font("Courier", 12), Brushes.Black, 25, 351)

        'Draw Order Number
        e.Graphics.DrawString("Order Number: " & ORDERNO, New Font("Courier", 12), Brushes.Black, 216, 240)
        'Draw Job Number
        e.Graphics.DrawString("Job Number: " & frmMain.f.lblLargeJobNumber.Text & "-" & frmMain.f.lblLargeVersion.Text, New Font("Courier", 12), Brushes.Black, 216, 262)
        'Draw Part Ref
        e.Graphics.DrawString("Part Ref: " & fullPartRef, New Font("Courier", 12), Brushes.Black, 216, 284)
        'Draw Operator
        e.Graphics.DrawString("Operator: " & frmMain.f.lblUser.Text, New Font("Courier", 12), Brushes.Black, 216, 306)
        'Draw Quantity
        e.Graphics.DrawString("Quantity: " & txtQuantity.Text, New Font("Courier", 12), Brushes.Black, 216, 328)
        'Draw Start Record
        e.Graphics.DrawString("Start Record: " & txtStartRecNum.Text, New Font("Courier", 12), Brushes.Black, 379, 328)
        'Draw End Record
        e.Graphics.DrawString("End Record: " & txtEndRecNum.Text, New Font("Courier", 12), Brushes.Black, 379, 350)
        'Draw Seed/Sample/Live
        e.Graphics.DrawString("Type: " & cmbCreateType.Text, New Font("Courier", 12), Brushes.Black, 216, 350)



        ' '''Draw Order Number
        ' ''e.Graphics.DrawString("Order Number: " & ORDERNO, New Font("Courier", 12), Brushes.Black, 216, 240)
        ' '''Draw Job Number
        ' ''e.Graphics.DrawString("Job Number: " & frmMain.f.lblLargeJobNumber.Text & "-" & frmMain.f.lblLargeVersion.Text, New Font("Courier", 12), Brushes.Black, 216, 265)
        ' '''Draw Part Ref
        ' ''e.Graphics.DrawString("Part Ref: " & fullPartRef, New Font("Courier", 12), Brushes.Black, 216, 294)
        ' '''Draw Quantity
        ' ''e.Graphics.DrawString("Quantity: " & txtQuantity.Text, New Font("Courier", 12), Brushes.Black, 216, 322)
        ' '''Draw Start Record
        ' ''e.Graphics.DrawString("Start Record: " & txtStartRecNum.Text, New Font("Courier", 12), Brushes.Black, 379, 322)
        ' '''Draw End Record
        ' ''e.Graphics.DrawString("End Record: " & txtEndRecNum.Text, New Font("Courier", 12), Brushes.Black, 379, 351)
        ' '''Draw Seed/Sample/Live
        ' ''e.Graphics.DrawString("Type: " & cmbCreateType.Text, New Font("Courier", 12), Brushes.Black, 216, 351)

    End Sub

    Private Sub frmWIP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If System.Diagnostics.Debugger.IsAttached Then
            Me.TopMost = False
        End If

        'cmbPrinter.Items.Add(RESOURCEPRINTER)

        For Each str As String In PrinterSettings.InstalledPrinters
            cmbPrinter.Items.Add(str)
        Next

        cmbPrinter.SelectedIndex = 0
        If cmbPrinter.Items.Count > 0 Then
            'Check for default printer
            Dim settings As New PrinterSettings()
            Dim defaultPrinter As String = GetDefaultPrinter().ToUpper

            For i = 0 To cmbPrinter.Items.Count - 1
                If cmbPrinter.Items(i).ToString.ToUpper = defaultPrinter Then
                    cmbPrinter.SelectedIndex = i
                    Exit For
                End If
            Next
        End If



        cmbCreateType.SelectedIndex = 0
        'LoadPartRef()
        LoadPartRef2()

        If ORDERNO = "0" Or ORDERNO = "" Or ORDERNO Is Nothing Then
            MessageBox.Show("ERROR: Unable to find an Order Number for this job." & vbCrLf & "You will be unable to create pallets.")
            cannotCreatePallets = True
        End If

        txtNoUp.Text = WIP_NUp
        cmbPartRef.Text = WIP_PartRef

        TabControl1.SelectedIndex = 1
        TabControl1.SelectedIndex = 0
        TabControl1.SelectedIndex = DefaultNdx
        Select Case DefaultNdx
            Case 0
                txtBarcode.Focus()
            Case 1
                cmbPartRef.Focus()
        End Select
        GeneralBackColor = Me.BackColor
    End Sub

    Public Function GetDefaultPrinter() As String
        Dim settings As PrinterSettings = New PrinterSettings()
        Return settings.PrinterName
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs)

        WIP_NEWPALLET = "WIP32493520"
        fullPartRef = ORDERNO & partRefDividor & (RESOURCENAME & "  ").Substring(0, 2).Trim & partRefDividor & CleanTheText(cmbPartRef.Text) & partRefDividor & cmbCreateType.Text.ToUpper.Substring(0, 4)

        PrintDocument1.PrinterSettings.PrinterName = cmbPrinter.Text
        PrintDocument1.Print()
    End Sub

    'Private Sub LoadPartRef()
    '    WriteApplicationLog("frmWIP LoadPartRef - Entered")
    '    Dim lst As List(Of Json_Structures_Cls.cls_GET_OutputPartRefs) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
    '    Dim iLoop As Integer = 0
    '    Dim strA As String = ""
    '    Dim strResourceID As String = RESOURCEID
    '    cmbPartRef.Items.Clear()

    '    Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
    '    Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
    '    Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
    '    Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

    '    'This Is The Defined Class For settings In Json_Structures_Cls
    '    '  Public Class cls_getCurrentShift
    '    '      Public resourceId As Int32  '120
    '    '      Public start As String      '"2018-04-18T06:00:00"
    '    '      Public End_ As String       '"2018-04-18T14:00:00"
    '    '      Public shiftCode As String  ' "A"
    '    '      Public shiftDate As String  ' "2018-04-18T00:00:00"
    '    'End Class
    '    lst = Json_Structures_Cls.Get_OutputPartRefs(currenttaskID)
    '    If Json_Structures_Cls.bWebResponseSuccess = True Then
    '        For iLoop = 0 To (lst.Count - 1)
    '            cmbPartRef.Items.Add(lst(iLoop).partRef & " " & partRefDividor & " " & lst(iLoop).description)
    '        Next
    '    End If

    '    WriteApplicationLog("frmWIP LoadPartRef - Exited")
    'End Sub

    Private Sub LoadPartRef2()
        WriteApplicationLog("frmWIP LoadPartRef2 - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_GET_JasmineFormsRuns) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID
        cmbPartRef.Items.Clear()

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        '  Public Class cls_getCurrentShift
        '      Public resourceId As Int32  '120
        '      Public start As String      '"2018-04-18T06:00:00"
        '      Public End_ As String       '"2018-04-18T14:00:00"
        '      Public shiftCode As String  ' "A"
        '      Public shiftDate As String  ' "2018-04-18T00:00:00"
        'End Class
        lst = Json_Structures_Cls.GET_JasmineFormsRuns(currenttaskID)
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            For iLoop = 0 To (lst.Count - 1)
                cmbPartRef.Items.Add(lst(iLoop).fileId)
            Next
        End If

        WriteApplicationLog("frmWIP LoadPartRef2 - Exited")
    End Sub

    Private Sub cmbPartRef_DropDown(sender As Object, e As EventArgs) Handles cmbPartRef.DropDown
        Dim g As Graphics = cmbPartRef.CreateGraphics()
        Dim aFont As Font = cmbPartRef.Font
        Dim vertScrollBarWidth As Long = IIf(cmbPartRef.Items.Count > cmbPartRef.MaxDropDownItems, SystemInformation.VerticalScrollBarWidth, 0)

        For Each s As String In cmbPartRef.Items
            Dim newWidth As Integer = g.MeasureString(s, aFont).Width + vertScrollBarWidth
            If (cmbPartRef.DropDownWidth < newWidth) Then
                cmbPartRef.DropDownWidth = newWidth
            End If
        Next
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged

        Select Case TabControl1.SelectedIndex
            Case 0
                txtBarcode.Focus()
            Case 1
                cmbPartRef.Focus()
        End Select
    End Sub

    Private Sub txtBarcode_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBarcode.KeyPress
        lblStatusConsume.Text = "Status: "
        Me.BackColor = GeneralBackColor
        If e.KeyChar = Chr(13) And txtBarcode.Text <> "" Then
            btnGoConsume_Click(New Object, New EventArgs)
        End If
    End Sub

#Region "IMBED FONT"
    'PRIVATE FONT COLLECTION TO HOLD THE DYNAMIC FONT

    Private _pfc As PrivateFontCollection = Nothing

    Public ReadOnly Property GetInstance(ByVal Size As Single, ByVal style As FontStyle) As Font
        Get
            'IF THIS IS THE FIRST TIME GETTING AN INSTANCE
            'LOAD THE FONT FROM RESOURCES
            If _pfc Is Nothing Then LoadFont()

            'RETURN A NEW FONT OBJECT BASED ON THE SIZE AND STYLE PASSED IN
            Return New Font(_pfc.Families(0), Size, style)

        End Get
    End Property

    Private Sub LoadFont()
        Try
            'INIT THE FONT COLLECTION
            _pfc = New PrivateFontCollection

            'LOAD MEMORY POINTER FOR FONT RESOURCE
            Dim fontMemPointer As IntPtr = Marshal.AllocCoTaskMem(My.Resources.barcod39.Length)

            'COPY THE DATA TO THE MEMORY LOCATION
            Marshal.Copy(My.Resources.barcod39, 0, fontMemPointer, My.Resources.barcod39.Length)

            'LOAD THE MEMORY FONT INTO THE PRIVATE FONT COLLECTION
            _pfc.AddMemoryFont(fontMemPointer, My.Resources.barcod39.Length)

            'FREE UNSAFE MEMORY
            Marshal.FreeCoTaskMem(fontMemPointer)
        Catch ex As Exception
            'ERROR LOADING FONT. HANDLE EXCEPTION HERE
        End Try

    End Sub

#End Region

    Private Sub txtCreation_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtQuantity.KeyPress, txtEndRecNum.KeyPress, txtStartRecNum.KeyPress, txtQuantity.KeyPress, cmbPartRef.KeyPress
        lblStatusCreate.Text = "Status: "
        Me.BackColor = GeneralBackColor
    End Sub
End Class