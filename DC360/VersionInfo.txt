﻿*************1.5
Cahnged avgCopySpeed to be Job Average from DCL360_Machine_Counts
Dim avgCopySpeed As Integer = Val(GetSetting("DCL360_Machine_Counts", "Jobs\" & GetSetting("iVu", "Config", "JobNumber") & "\Totals\All_Shifts", "RunAverage"))
.lastGCSpeed = currSpeed

************1.6 V1.6 Release CHG-968: 2/10/2021 
An unhandled error in "tmrRePostFailed" could cause the timer to disable and never try to repost untill the program is restarted.  This caused the repost file to fill with old data.
	tmrRePostFailed_Elapsed
		Added SaveSetting("iVu", "Config", "DC360RepostTimer", tmrRePostFailed.Enabled.ToString)
		Moved Try ...moved to If Not IO.Directory.Exists


