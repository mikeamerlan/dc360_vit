﻿Imports System.ComponentModel
Imports DC360
Public Class frmMain
    Dim VIT_IS_RUNNING As Boolean = False
    Public Shared f As frmMain
#Region "SharedMemory"
    Dim WithEvents clsDC360SM As DC360_SharedMemory_Cls

    '****************************************  NOTE NOTE NOTE  *************************************
    '********************** DO NOT CALL DC360_SharedMemory_Cls CLASS DIRECTLY **********************

    'Add Shared Memory To Watch
    'clsDC360SM.AddSharedMemoryVariable("Memory Name","Default Value If No Memory Exists","[Optional] Maximum Memory Size, Default(255)")

    'See If Memory Thread Is Running
    'clsDC360SM.IsRunning T/F

    'Save A Memory Setting To Shared Memory
    'clsDC360SM.SaveMemorySetting("Memory Name", "New Value", "[Optional] Overwrite If Exists, Default Is TRUE")

    'Start The Server Thread
    ' clsDC360SM.Server_Start(Me) 'leave it: Me

    'Stop The Server Thread
    ' clsDC360SM.Server_Stop()

    'Get/Set Thread Information
    'clsDC360SM.Thread_CurrentLatency
    'clsDC360SM.Thread_CycleCount 
    'clsDC360SM.Thread_MaxLatency
    Private Sub clsDC360SM_ErrorRaised(strErrDescription As String) Handles clsDC360SM.ErrorRaised
        ' MsgBox(strErrDescription)
    End Sub
    Private Sub clsDC360SM_ThreadStarted() Handles clsDC360SM.ThreadStarted        'Event Fires When The Thread Starts
        'ThreadStatus_Lbl.Text = "Thread Status: Started Successfully at: " & Now
        'StartStopSMServer_But.Text = "Stop SM Server"
    End Sub
    Private Sub clsDC360SM_ThreadStopped() Handles clsDC360SM.ThreadStopped 'Event Fires When The Thread Stops
        'ThreadStatus_Lbl.Text = "Thread Status: Stopped Successfully at: " & Now
        'StartStopSMServer_But.Text = "Start SM Server"
    End Sub
    Dim arrTmp() As DC360_SharedMemory_Cls.structSharedMemory

    Private Sub RunSharedMemThread()
        WriteApplicationLog("FORM1 RunSharedMemThread - Entered")
        'Event Fires On Any "watched" Shared Memory Variables Change
        Dim iarrLoop As Integer = 0
        For iarrLoop = 1 To UBound(arrTmp)
            Select Case arrTmp(iarrLoop).strMemoryName
                Case "DC360_JobNumber"
                    'We only write jobNumbers, so do nothing
                Case "DC360_OpCode"
                    If arrTmp(iarrLoop).strMemoryValue <> cmbOpCode.Text Then 'really an OpCodeChange

                        Dim newOpCodeValue As String = arrTmp(iarrLoop).strMemoryValue
                        Dim tryCounter As Long = 0
                        While GetOpCodeNdx(newOpCodeValue) = -1  'Dirty Read
                            newOpCodeValue = arrTmp(iarrLoop).strMemoryValue
                            If newOpCodeValue.ToUpper = "UNKNOWN STOP" Then Exit While
                            If tryCounter > 100 Then 'We have tried 100 times, just set it to Running

                                'See if Running is a possibility
                                For i As Long = 0 To cmbOpCode.Items.Count - 1
                                    If cmbOpCode.Items(i).ToString = "Running" Then
                                        clsDC360SM.SaveMemorySetting("DC360_OpCode", "Running")
                                        Exit Sub
                                    End If
                                Next

                                'If no job was loaded, check for No Work
                                For i As Long = 0 To cmbOpCode.Items.Count - 1
                                    If cmbOpCode.Items(i).ToString = "No Work" Then
                                        clsDC360SM.SaveMemorySetting("DC360_OpCode", "No Work")
                                        Exit Sub
                                    End If
                                Next

                                'If nothing else, set to the first item....

                                clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Items(0).ToString)
                                Exit Sub
                            End If
                            tryCounter += 1
                        End While


                        If UNKNOWNSTOP Then 'We are in an unknown stop and are coming out of it
                            ' MessageBox.Show("Difference in Seconds: " & DateDiff(DateInterval.Second, UnknownStopStart, Now) & vbCrLf & "Minor Stop: " & (MINORSTOPDURATION * 1) & vbCrLf & "Should be minor: " & (DateDiff(DateInterval.Second, UnknownStopStart, Now) < (MINORSTOPDURATION * 1)))
                            If DateDiff(DateInterval.Second, UnknownStopStart, Now) < (MINORSTOPDURATION * 1) Then
                                currentOpCodeNDX = opCodeMinorStopNdx
                            End If
                            UNKNOWNSTOP = False
                            For i As Long = 0 To cmbOpCode.Items.Count - 1
                                If cmbOpCode.Items(i).ToString = newOpCodeValue And cmbOpCode.Items(i).ToString <> cmbOpCode.Text Then
                                    PriorDropDownNDX = cmbOpCode.SelectedIndex : cmbOpCode.SelectedIndex = i
                                    GoTo FOUND_OPCODE1
                                End If
                            Next
                            'Assume we are changing to an OpCode not in the list
                            cmbOpCode.Text = newOpCodeValue
                            PriorDropDownNDX = -99 : cmbOpCode.SelectedIndex = -1 'Setting to -99 forces the opcode to change
FOUND_OPCODE1:
                        ElseIf newOpCodeValue <> cmbOpCode.Text And newOpCodeValue = "Unknown Stop" Then
                            UNKNOWNSTOP = True
                            UnknownStopStart = DateAdd(DateInterval.Second, -60, Now)
                            DROPMINUTE = True
                            cmbOpCode.Text = "Unknown Stop"
                            PriorDropDownNDX = -99 : cmbOpCode.SelectedIndex = -1
                            EVENTSTARTDATE = CreateEventDateTime(UnknownStopStart)
                        Else
                            'Regular OpCode Change
                            For i As Long = 0 To cmbOpCode.Items.Count - 1
                                If cmbOpCode.Items(i).ToString = newOpCodeValue And cmbOpCode.Items(i).ToString <> cmbOpCode.Text Then
                                    PriorDropDownNDX = -99 : cmbOpCode.SelectedIndex = i
                                    GoTo FOUND_OPCODE2
                                End If
                            Next
                            'Assume we are changing to an OpCode not in the list
                            cmbOpCode.Text = newOpCodeValue
                            PriorDropDownNDX = -99 : cmbOpCode.SelectedIndex = -1 'Setting to -99 forces the opcode to change
FOUND_OPCODE2:
                        End If
                        cmbOpCode_SelectedIndexChanged(New Object, New EventArgs)
                    End If

                Case "DC360_OpCodeColor"
                    'cmbOpCode.BackColor = Color.FromName(arrtmp(iarrLoop).strMemoryValue)
                Case "DC360_UserName"
                    lblUser.Text = arrTmp(iarrLoop).strMemoryValue

                Case "DC360_OpCodeList"
                    'We only write op code list, so do nothing
                    'MessageBox.Show("OpCodeList updated to:" & arrtmp(iarrLoop).strMemoryValue)
                    'Dim a As String = arrtmp(iarrLoop).strMemoryValue
                    'Dim b As String = ""
                Case "DC360_JobDialog"
                    If arrTmp(iarrLoop).strMemoryValue <> "1" Then Continue For
                    ChangeJob() 'btnChangeJob_Click(New Object, New EventArgs)
                    clsDC360SM.SaveMemorySetting("DC360_JobDialog", "0")
                    clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                Case "DC360_UserDialog"
                    If arrTmp(iarrLoop).strMemoryValue <> "1" Then Continue For
                    lblUser_Click(New Object, New EventArgs)
                    clsDC360SM.SaveMemorySetting("DC360_UserDialog", "0")
                Case "DC360_GoodToWasteDialog"
                    If arrTmp(iarrLoop).strMemoryValue <> "1" Then Continue For
                    btnGoodToWaste_Click(New Object, New EventArgs)
                    clsDC360SM.SaveMemorySetting("DC360_GoodToWasteDialog", "0")
                Case "DC360_VitOpen"
                    If arrTmp(iarrLoop).strMemoryValue = "1" Then
                        VIT_IS_RUNNING = True
                        btnChangeJob.Enabled = False
                        lblUser.Enabled = False
                        cmbOpCode.Enabled = False
                        btnGoodToWaste.Enabled = False
                        Me.ControlBox = False
                        ChangeView("SMALL")
                    Else
                        VIT_IS_RUNNING = False
                        ChangeView("LARGE")
                        btnChangeJob.Enabled = True
                        lblUser.Enabled = True
                        cmbOpCode.Enabled = True
                        btnGoodToWaste.Enabled = True
                        Me.ControlBox = True
                    End If
                Case "DC360_IsRunning"
                    If arrTmp(iarrLoop).strMemoryValue = "0" Then
                        Me.Close()
                    End If
                Case "DC360_MinorStopDuration"
                    'Dim a As String = arrtmp(iarrLoop).strMemoryValue
                    'Dim b As String = ""
                Case "DC360_WIPCreateDialog"
                    If arrTmp(iarrLoop).strMemoryValue <> "1" Then Continue For
                    Dim aWIP As New frmWIP(0)
                    aWIP.ShowDialog()
                    clsDC360SM.SaveMemorySetting("DC360_WIPCreateDialog", "0")
                Case "DC360_WIPConsumeDialog"
                    If arrTmp(iarrLoop).strMemoryValue <> "1" Then Continue For
                    Dim aWIP As New frmWIP(1)
                    aWIP.ShowDialog()
                    clsDC360SM.SaveMemorySetting("DC360_WIPConsumeDialog", "0")
                Case "DC360_JobNUp"
                    Dim newNoUp As Long = arrTmp(iarrLoop).strMemoryValue
                    If newNoUp <> noUp Then 'Update accordingly
                        cmbNumberUp.Text = newNoUp
                    End If
                Case "DC360_JobConfig"
                    Dim newConfig As String = arrTmp(iarrLoop).strMemoryValue

                    If newConfig <> cmbConfig.Text Then 'really a Config Change
                        For i As Long = 0 To cmbConfig.Items.Count - 1
                            If cmbConfig.Items(i).ToString = newConfig Then
                                PriorDropDownNDX = cmbConfig.SelectedIndex
                                cmbConfig.Text = newConfig
                                cmbSmallConfig.Text = newConfig
                            End If
                        Next
                    End If
                Case "DC360_Assistants"
                    Dim newAssistants As Long = arrTmp(iarrLoop).strMemoryValue
                    If newAssistants <> cmbAssistants.Text Then 'Update accordingly
                        cmbAssistants.Text = newAssistants
                        cmbSmallAssistants.Text = newAssistants
                    End If
            End Select
        Next
        WriteApplicationLog("FORM1 RunSharedMemThread - Exited")
    End Sub
    Private Sub clsDC360SM_SharedMemoryChange(arrSharedMemory() As DC360_SharedMemory_Cls.structSharedMemory) Handles clsDC360SM.SharedMemoryChange
        If Me.InvokeRequired Then
            '***

            arrTmp = arrSharedMemory
            Me.Invoke(New MethodInvoker(AddressOf RunSharedMemThread))
        Else
            'This should 
            WriteApplicationLog("****BAD  MEMORY SHOULD NOT HAVE HIT HERE****")
            Exit Sub
        End If

    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        WriteApplicationLog("FORM1 Form1_FormClosing - Entered")
        clsDC360SM.Server_Stop() 'Stop Server On FormClosing
        WriteApplicationLog("FORM1 Form1_FormClosing - Exited")
        End
    End Sub

    Private Sub LoadMemoryVariables()
        WriteApplicationLog("FORM1 LoadMemoryVariables - Entered")
        clsDC360SM = New DC360_SharedMemory_Cls 'Start A New Class
        clsDC360SM.AddSharedMemoryVariable("DC360_JobNumber", "This Value If No Memory Set") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_OpCode") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_OpCodeColor") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_UserName") 'Set A Few Shared Memory Variables To Watch
        'clsDC360SM.AddSharedMemoryVariable("DC360_OpCodeList") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_OpCodeList",, 10000) 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobDialog") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_UserDialog") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_GoodToWasteDialog") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_VitOpen", "0") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_MinorStopDuration") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_IsRunning") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_MaxSpeed") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_QtyToGo") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_PlannedQty") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_Assistants") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobSpeed") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_WIPCreateDialog") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_WIPConsumeDialog") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobName") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobConfig") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobClient") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobNUp") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_Staff") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_Process") 'Set A Few Shared Memory Variables To Watch
        clsDC360SM.AddSharedMemoryVariable("DC360_JobConfigList") 'Set A Few Shared Memory Variables To Watch

        clsDC360SM.Server_Start(Me) 'Start Thread Server
        WriteApplicationLog("FORM1 LoadMemoryVariables - Exited")
    End Sub

#End Region
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WriteApplicationLog("FORM1 Form1_Load - Entered")
        DoNotWriteApplicationLog = False 'Change this to TRUE to avoid the application log

        If System.Diagnostics.Debugger.IsAttached Then
            Me.TopMost = False
            Button1.Visible = True
            Button2.Visible = True
            Button3.Visible = True
            Button4.Visible = True
            Button5.Visible = True
            Button6.Visible = True
            Button7.Visible = True
            Button8.Visible = True
            Button9.Visible = True
            Button10.Visible = True
            Button11.Visible = True
            Button12.Visible = True
            Button13.Visible = True
            Me.ControlBox = True
        End If

        Me.Text &= " " & Application.ProductVersion
        f = Me


        'set maxNumberUpDefault
        UpdateMaxNumberUp(16)

        GetConfigItems()

        'Validate Connection
        If Not HasValidConnection() Then
            End
        End If
        Me.Text &= " - " & strHost.ToUpper.Replace("HTTP://", "").Replace(".DC360.SG360.COM", "") & " - " & RESOURCENAME & " (" & RESOURCEID & ")"

        grpSmall.Left = 10

        cmbAssistants.SelectedIndex = 0
        cmbConfig.SelectedIndex = 0
        cmbSmallAssistants.SelectedIndex = 0
        cmbSmallConfig.SelectedIndex = 0
        'cmbOpCode.SelectedIndex = 0
        ORGLabelSize = lblSmallClient.Font
        Me.Location = New Point(Screen.GetWorkingArea(Me).Right - Size.Width, (Screen.GetWorkingArea(Me).Bottom - Size.Height) / 2)
        IAMLOADING = False

        'After the form has loaded, load the memory variables.
        LoadMemoryVariables()
        noUpFromJob = 1
        noUp = 1

        Try
            clsDC360SM.SaveMemorySetting("DC360_UserName", "No Operator")
            clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Text)
            Dim changeJobFrm As New frmChangeJob
            lblLargeJobName.Text = changeJobFrm.finalJob.jobName
            lblSmallJobName.Text = changeJobFrm.finalJob.jobName
            lblLargeClient.Text = changeJobFrm.finalJob.company
            lblSmallClient.Text = changeJobFrm.finalJob.company
            lblLargeJobNumber.Text = changeJobFrm.finalJob.jobNumber.Split("-")(0)
            lblSmallJobNumber.Text = changeJobFrm.finalJob.jobNumber.Split("-")(0)
            lblLargeVersion.Text = GetVersionFromSubJob(changeJobFrm.finalJob.jobNumber) 'changeJobFrm.finalJob.jobNumber.Split("-")(1)
            lblSmallVersion.Text = GetVersionFromSubJob(changeJobFrm.finalJob.jobNumber) 'changeJobFrm.finalJob.jobNumber.Split("-")(1)
            SUBJOBNO = "No Selection-No Selection"
            clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
            clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)

            UpdateSettings()
            UpdateOpCodes()
            UpdateConfig()
            LoadCurrentEvents()
            LoadCurrentShift()
            UpdateUserBackColor()
        Catch ex As Exception
            Me.Close()
        End Try

        OKFORPOSTBACK = True
        cmbNumberUp.Text = noUp
        clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
        clsDC360SM.SaveMemorySetting("DC360_IsRunning", "1")
        clsDC360SM.SaveMemorySetting("DC360_PlannedQty", "0")
        clsDC360SM.SaveMemorySetting("DC360_Assistants", cmbAssistants.Text)
        If lblLargeJobNumber.Text = "No Selection" Then
            btnWIP.Visible = False
        Else
            btnWIP.Visible = True
        End If
        WriteApplicationLog("FORM1 Form1_Load - Exited")

    End Sub


    Private Function HasValidConnection() As Boolean
        WriteApplicationLog("FORM1 HasValidConnection - Entered")
        'Check for valid admin code
        Dim lst As List(Of Json_Structures_Cls.cls_admins) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_admins
        '    Public id As Int32          '36
        '    Public userName As String   '"Mark Anderson"
        '    Public pin As Int32         '12345678"
        'End Class

        Dim goodAdmin As Boolean = False
        lst = Json_Structures_Cls.Get_admins()
        If Json_Structures_Cls.bWebResponseSuccess = True Then

            'We can connect with the end points so all is good
            WriteApplicationLog("FORM1 HasValidConnection - PASSED & Exited")
            Return True
        Else
            MessageBox.Show(Me, "ERROR - UNABLE TO CONNECT TO END POINTS.", "DC360 Network Error", vbOKOnly, MessageBoxIcon.Error)
            WriteApplicationLog("FORM1 HasValidConnection - FAILED & Exited")
            Return False
        End If

    End Function


    Private Sub GetConfigItems()
        WriteApplicationLog("FORM1 GetConfigItems - Entered")
        Dim configPath As String = "\\j-drive\j_drive\Steve\templates\DC360 Templates\Config.txt"
        If IO.File.Exists(strDC360RootPath & "\Config.txt") Then
            configPath = strDC360RootPath & "\Config.txt"
        End If
        Dim inTs As New IO.StreamReader(configPath, System.Text.Encoding.Default)
        With inTs
            Do While .Peek <> -1
                Dim line() As String = .ReadLine.Split("~")
                Select Case line(0).ToUpper
                    Case "HOST" : strHost = line(1)
                    Case "RESOURCEID" : RESOURCEID = line(1)
                    Case "RESOURCENAME" : RESOURCENAME = line(1)
                    Case "APPLICATIONLOG" : DoNotWriteApplicationLog = IIf(line(1).ToUpper = "NO", True, False)
                    Case "HIDEDC360" : HideDC360 = IIf(line(1).ToUpper = "YES", True, False)
                    Case "MAX_NUP" : UpdateMaxNumberUp(line(1))

                End Select

            Loop
        End With
        inTs.Close()

        WriteApplicationLog("FORM1 GetConfigItems - Exited")

    End Sub

    Private Sub UpdateMaxNumberUp(maxNoUp As Long)
        Dim priorLoading As Boolean = IAMLOADING
        IAMLOADING = True

        Dim currNumberUp As String = cmbNumberUp.Text
        cmbNumberUp.Items.Clear()
        For i As Long = 1 To maxNoUp
            cmbNumberUp.Items.Add(i)
        Next
        cmbNumberUp.Text = currNumberUp

        IAMLOADING = priorLoading
    End Sub

    Private Sub LoadCurrentEvents()
        WriteApplicationLog("FORM1 LoadCurrentEvents - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_getCurrentEvent) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        ' Public Class cls_getCurrentEvent
        '     Public resourceId As Int32                  '": 120,
        '     Public taskId As Nullable(Of Int32)         '": null,
        '     Public opCodeId As Int32                    '": 144,
        '     Public opUnitId As Int32                    '": 1,
        '     Public opAreaId As Nullable(Of Int32)       '": null,
        '     Public configId As Nullable(Of Int32)       '": null,
        '     Public operatorId As Nullable(Of Int32)     '": null,
        '     Public assistants As Nullable(Of Int32)     '": 0,
        '     Public eventStartDateTime As String         '": "2018-01-12T06:00:00"'
        'End Class

        lst = Json_Structures_Cls.Get_getCurrentEvent(strResourceID)
        If lst Is Nothing Then 'no past events
            CURRENTOPERATORID = 0
            clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
            PushOpCodeList(False)
            LoadCurrentOpCode(144)
            cmbSmallConfig.SelectedIndex = 0
            cmbConfig.SelectedIndex = 0
            currentconfigNDX = cmbConfig.SelectedIndex

            currentnoAssistants = 0
            LoadCurrentAssistant(0)
            EVENTSTARTDATE = Now.ToString("yyyy-MM-ddTHH:mm:ss") '2020-02-20T10:15:18
        ElseIf Json_Structures_Cls.bWebResponseSuccess = True Then
            'strA = "resourceId,taskId,opCodeId,opUnitId,opAreaId,configId,operatorId,assistants,eventStartDateTime" & vbCrLf
            If Not lst(iLoop).operatorId Is Nothing Then
                Try
                    LoadCurrentUser(lst(iLoop).operatorId)
                    CURRENTOPERATORID = lst(iLoop).operatorId
                Catch ex As Exception

                End Try
            End If
            If Not lst(iLoop).taskId Is Nothing Then
                Try
                    LoadCurrentTask(lst(iLoop).taskId)
                    currenttaskID = lst(iLoop).taskId
                    PushOpCodeList(True)
                Catch ex As Exception
                    clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                    PushOpCodeList(False)
                End Try
            Else
                clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                PushOpCodeList(False)
            End If
            LoadCurrentOpCode(lst(iLoop).opCodeId)
            If Not lst(iLoop).configId Is Nothing Then
                Try
                    LoadCurrentConfig(lst(iLoop).configId)
                Catch ex As Exception
                    'If config no longer exists, default to the first entry
                    cmbSmallConfig.SelectedIndex = 0
                    cmbConfig.SelectedIndex = 0
                End Try
            Else
                'If config no longer exists, default to the first entry
                cmbSmallConfig.SelectedIndex = 0
                cmbConfig.SelectedIndex = 0
            End If
            currentconfigNDX = cmbConfig.SelectedIndex

            currentnoAssistants = lst(iLoop).assistants : If Not lst(iLoop).assistants Is Nothing Then LoadCurrentAssistant(lst(iLoop).assistants)
            EVENTSTARTDATE = lst(iLoop).eventStartDateTime

            'For iLoop = 0 To (lst.Count - 1)
            '    strA += lst(iLoop).resourceId & "|"
            '    strA += lst(iLoop).taskId & "|"
            '    strA += lst(iLoop).opCodeId & "|"
            '    strA += lst(iLoop).opUnitId & "|"
            '    strA += lst(iLoop).opAreaId & "|"
            '    strA += lst(iLoop).configId & "|"
            '    strA += lst(iLoop).operatorId & "|"
            '    strA += lst(iLoop).assistants & "|"
            '    strA += lst(iLoop).eventStartDateTime & vbCrLf
            'Next
            'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " getCurrentEvent")
        Else
            ' MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If
        WriteApplicationLog("FORM1 LoadCurrentEvents - Exited")
    End Sub

    Private Sub LoadCurrentUser(operatorID As String)
        WriteApplicationLog("FORM1 LoadCurrentUser - Entered")
        CURRENTOPERATORID = operatorID
        Dim lst As List(Of Json_Structures_Cls.cls_operators) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strOperatorID As String = operatorID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_operators
        '    Public id As String         '"991916"
        '    Public firstName As String  '"Catalina"
        '    Public lastName As String   '"Penaloza"
        'End Class

        lst = Json_Structures_Cls.Get_operators(strOperatorID)
        If Json_Structures_Cls.bWebResponseSuccess = True Then

            Dim tmpEventTime As String = CreateEventDateTime(Now())
            If CURRENTOPERATORID <> "" And CURRENTOPERATORID <> "0" Then DoPostBackTimesheet("Logout", CURRENTOPERATORID, tmpEventTime)

            lblUser.Text = lst(0).lastName & ", " & lst(0).firstName
            CURRENTOPERATORNAME = lblUser.Text
            clsDC360SM.SaveMemorySetting("DC360_UserName", lblUser.Text)

            'strA = "id,firstName,lastName" & vbCrLf
            'For iLoop = 0 To (lst.Count - 1)
            '    strA += lst(iLoop).id & "|"
            '    strA += lst(iLoop).firstName & "|"
            '    strA += lst(iLoop).lastName & vbCrLf
            'Next
            'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " operators")
        Else
            'On initial load, ignore if the prior user no longer exists or was not set.
            If OKFORPOSTBACK Then MsgBox("Error: User Not Found.", MsgBoxStyle.SystemModal, "Web Response Error - Invalid Login")
        End If
        WriteApplicationLog("FORM1 LoadCurrentUser - Exited")
    End Sub
    Private Sub LoadCurrentTask(taskID As String)
        WriteApplicationLog("FORM1 LoadCurrentTask - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_tasks) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strTaskID As String = taskID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_tasks
        '    Public taskID As Int32                          '424421
        '    Public resourceID As Int32                      '1
        '    Public resourceName As String                   '"Press25"
        '    Public startDateTime As String                  '"2017-08-07T07:00:00-06:00"
        '    Public endDateTime As String                    '"2017-08-09T15:00:00-06:00"
        '    Public subJobNo As String                       '"92672-001A"
        '    Public taskName As String                       '"Wal-Mart Mastercard Reissue Dual EMV Benefits Brochure"
        '    Public company As String                        '"string"
        '    Public orderNo As Int32                         '92672
        '    Public plannedCopies As Int32                   '3683000
        '    Public noUp As Int32                            '10
        '    Public makeReadyMins As Int32                   '1200
        '    Public runMins As Int32                         '2160
        '    Public noPasses As Int32                        '1
        '    Public speed As Int32                           '15675
        '    Public netImpressions As Nullable(Of Int32)     '*73000
        '    Public grossImpressions As Nullable(Of Int32)   '*184329
        '    Public wasteImpressions As Nullable(Of Int32)   '*111329
        '    Public netCopies As Nullable(Of Int32)          '*730000
        '    Public grossCopies As Nullable(Of Int32)        '*1113290
        '    Public wasteCopies As Nullable(Of Int32)        '*0
        '    Public qtytoGo As Int32                         '2953000
        '    Public taskStatus As String                     '"LIVE"
        '    Public totalSubJobQty As Int32                  '20000000
        '    Public configId As Int32                        '*23
        'End Class

        lst = Json_Structures_Cls.Get_tasks(strTaskID)
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            ' strA = "taskID,resourceID,resourceName,startDateTime,endDateTime,subJobNo,taskName,company,orderNo,plannedCopies,noUp,makeReadyMins,runMins,speed,netImpressions,grossImpressions,wasteImpressions,netCopies,grossCopies,wasteCopies,qtytoGo,taskStatus,totalSubJobQty,configId" & vbCrLf
            For iLoop = 0 To (lst.Count - 1)
                'strA += lst(iLoop).taskID & "|"
                'strA += lst(iLoop).resourceID & "|"
                'strA += lst(iLoop).resourceName & "|"
                'strA += lst(iLoop).startDateTime & "|"
                'strA += lst(iLoop).endDateTime & "|"
                'strA += lst(iLoop).subJobNo & "|"
                'strA += lst(iLoop).taskName & "|"
                'strA += lst(iLoop).company & "|"
                'strA += lst(iLoop).orderNo & "|"
                'strA += lst(iLoop).plannedCopies & "|"
                'strA += lst(iLoop).noUp & "|"
                'strA += lst(iLoop).makeReadyMins & "|"
                'strA += lst(iLoop).runMins & "|"
                'strA += lst(iLoop).speed & "|"
                'strA += lst(iLoop).netImpressions & "|"
                'strA += lst(iLoop).grossImpressions & "|"
                'strA += lst(iLoop).wasteImpressions & "|"
                'strA += lst(iLoop).netCopies & "|"
                'strA += lst(iLoop).grossCopies & "|"
                'strA += lst(iLoop).wasteCopies & "|"
                'strA += lst(iLoop).qtytoGo & "|"
                'strA += lst(iLoop).taskStatus & "|"
                'strA += lst(iLoop).totalSubJobQty & "|"
                'strA += lst(iLoop).configId & "|"

                lblLargeJobName.Text = lst(iLoop).taskName
                lblSmallJobName.Text = lst(iLoop).taskName
                lblLargeClient.Text = lst(iLoop).company
                lblSmallClient.Text = lst(iLoop).company
                lblLargeJobNumber.Text = lst(iLoop).subJobNo.Split("-")(0)
                lblSmallJobNumber.Text = lst(iLoop).subJobNo.Split("-")(0)
                lblLargeVersion.Text = GetVersionFromSubJob(lst(iLoop).subJobNo) '(lst(iLoop).subJobNo & "-").Split("-")(1)
                lblSmallVersion.Text = GetVersionFromSubJob(lst(iLoop).subJobNo) '(lst(iLoop).subJobNo & "-").Split("-")(1)
                lblCount.Text = lst(iLoop).qtytoGo
                lblFreezeCount.Text = lst(iLoop).qtytoGo
                LastQtyToGo = lblCount.Text
                InitialQtyToGo = lblCount.Text
                'TotalImpressions = lst(iLoop).plannedCopies / noUp 'Change to impressions instead of pieces
                TotalPieces = lst(iLoop).plannedCopies
                'WasteImpressions = lst(iLoop).wasteImpressions
                WastePieces = lst(iLoop).wasteCopies
                currenttaskID = taskID
                currentClient = lblLargeClient.Text
                currentJobName = lblLargeJobName.Text
                SUBJOBNO = lst(iLoop).subJobNo
                ORDERNO = lst(iLoop).orderNo

                noUpFromJob = lst(iLoop).noUp
                Try
                    Dim LocalNumberUp As String = Val(GetSetting("DCL360_Machine_Counts", "Jobs\" & lblLargeJobNumber.Text & "-" & lblLargeVersion.Text, "NumberUp"))
                    If LocalNumberUp <> "0" Then
                        LocalNumberUp = LocalNumberUp.Split("-")(0)
                        noUp = LocalNumberUp
                        cmbNumberUp.Text = LocalNumberUp

                        clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                        If noUp = noUpFromJob Then
                            lblNoUp.BackColor = (New Label).BackColor
                        Else
                            lblNoUp.BackColor = Color.Yellow
                        End If
                    Else
                        noUp = lst(iLoop).noUp
                        cmbNumberUp.Text = noUp
                    End If
                Catch ex As Exception
                    noUp = lst(iLoop).noUp
                    cmbNumberUp.Text = noUp
                End Try


                clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                clsDC360SM.SaveMemorySetting("DC360_PlannedQty", lst(iLoop).plannedCopies)
                clsDC360SM.SaveMemorySetting("DC360_JobSpeed", lst(iLoop).speed)
                clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                clsDC360SM.SaveMemorySetting("DC360_Staff", lst(iLoop).staff)
                clsDC360SM.SaveMemorySetting("DC360_Process", lst(iLoop).process)

            Next
            ' MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " tasks")
        Else
            ' MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
        End If

        WriteApplicationLog("FORM1 LoadCurrentTask - Exited")
    End Sub
    Private Sub LoadCurrentOpCode(opCodeID As String)
        WriteApplicationLog("FORM1 LoadCurrentOpCode - Entered")
        For iLoop As Long = 0 To myOpCodeList.Count - 1
            If (myOpCodeList(iLoop).opCodeID = opCodeID) Then
                For ndx As Long = 0 To cmbOpCode.Items.Count - 1
                    If cmbOpCode.Items(ndx).ToString = myOpCodeList(iLoop).opCodeName Then
                        cmbOpCode.SelectedIndex = ndx
                        If Not IAMLOADING Then clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Text)
                    End If
                Next
                If cmbOpCode.Text <> myOpCodeList(iLoop).opCodeName Then
                    cmbOpCode.Text = myOpCodeList(iLoop).opCodeName
                    If Not IAMLOADING Then clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Text)
                End If
                currentOpCodeNDX = iLoop

                Exit For
            End If
        Next
        WriteApplicationLog("FORM1 LoadCurrentOpCode - Exited")
    End Sub
    Private Sub LoadCurrentConfig(configID As String)
        WriteApplicationLog("FORM1 LoadCurrentConfig - Entered")
        For iLoop As Long = 0 To (myConfigList.Count - 1)
            If (myConfigList(iLoop).ConfigID = configID) Then
                cmbSmallConfig.SelectedIndex = iLoop
                cmbConfig.SelectedIndex = iLoop
                clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)
                Exit Sub
            End If
        Next
        WriteApplicationLog("FORM1 LoadCurrentConfig - Exited")
    End Sub
    Private Sub LoadCurrentAssistant(assistantQty As String)
        WriteApplicationLog("FORM1 LoadCurrentAssistant - Entered")
        cmbAssistants.SelectedIndex = assistantQty
        cmbSmallAssistants.SelectedIndex = assistantQty
        WriteApplicationLog("FORM1 LoadCurrentAssistant - Exited")
    End Sub
    Private Sub ChangeView(myview As String)
        WriteApplicationLog("FORM1 ChangeView - Entered")

        If HideDC360 Then
            Me.Visible = False
            Exit Sub
        End If


        Dim upperRightLocation As Point = New Point(Me.Location.X + Size.Width, Me.Location.Y)
        If myview = "SMALL" Then
            Me.Width = 276
            grpSmall.Visible = True
            grpJobInfo.Visible = False
            grpMetrics.Visible = False
            grpOperations.Visible = False
            btnGoodToWaste.Visible = False
            lblError.Location = New Point(0, 369)
            lblNoUp.Location = New Point(46, 262)
            cmbNumberUp.Location = New Point(178, 259)
            Me.Height = 476
        Else
            Me.Width = 921
            grpSmall.Visible = False
            grpJobInfo.Visible = True
            grpMetrics.Visible = True
            grpOperations.Visible = True
            btnGoodToWaste.Visible = True
            lblError.Location = New Point(572, 268)
            lblNoUp.Location = New Point(676, 196)
            cmbNumberUp.Location = New Point(808, 193)
            Me.Height = 426
        End If
        If IAMLOADING Then
            Me.Location = New Point(Screen.GetWorkingArea(Me).Right - Size.Width, 20)
        Else
            Me.Location = New Point(upperRightLocation.X - Size.Width, upperRightLocation.Y)
        End If

        WriteApplicationLog("FORM1 ChangeView - Exited")
    End Sub
    Private Sub lblUser_Click(sender As Object, e As EventArgs) Handles lblUser.Click
        WriteApplicationLog("FORM1 lblUser_Click - Entered")
        'Show Change User Dialog
        Dim logInFrm As New frmKeyPad("Login", lblUser.Text)
        logInFrm.StartPosition = FormStartPosition.CenterScreen
        Dim bkgrndOp As New BackgroundOpacity
        bkgrndOp.Show()
        logInFrm.ShowDialog()
        bkgrndOp.Dispose()
        Select Case logInFrm.myResult
            Case -1 'LogOut event
                Dim tmpEventTime As String = CreateEventDateTime(Now())

                Dim cntHolder As String = lblCount.Text
                Dim assistHolder As String = cmbAssistants.Text
                Dim configHolder As String = cmbConfig.Text
                Dim configNdxHolder As String = cmbConfig.SelectedIndex
                Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
                DoPostBackCostEvent(tmpEventTime, cntHolder, "21",, opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)

                DoPostBackTimesheet("Logout", CURRENTOPERATORID, tmpEventTime)
                EVENTSTARTDATE = tmpEventTime
                lblUser.Text = "No Operator"
                clsDC360SM.SaveMemorySetting("DC360_UserName", lblUser.Text)
                CURRENTOPERATORID = ""
                CURRENTOPERATORNAME = ""
            Case -2 'Cancel Event
            Case Else 'ID of new user
                Dim lst As List(Of Json_Structures_Cls.cls_operators) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
                Dim iLoop As Integer = 0
                Dim strA As String = ""
                Dim strOperatorID As String = logInFrm.myResult

                Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
                Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
                Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
                Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

                'This Is The Defined Class For settings In Json_Structures_Cls
                'Public Class cls_operators
                '    Public id As String         '"991916"
                '    Public firstName As String  '"Catalina"
                '    Public lastName As String   '"Penaloza"
                'End Class

                lst = Json_Structures_Cls.Get_operators(strOperatorID)
                If Json_Structures_Cls.bWebResponseSuccess = True Then

                    Dim tmpEventTime As String = CreateEventDateTime(Now())
                    Dim cntHolder As String = lblCount.Text
                    Dim assistHolder As String = cmbAssistants.Text
                    Dim configHolder As String = cmbConfig.Text
                    Dim configNdxHolder As String = cmbConfig.SelectedIndex
                    Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
                    DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)

                    EVENTSTARTDATE = tmpEventTime
                    If CURRENTOPERATORID <> "" And CURRENTOPERATORID <> "0" Then DoPostBackTimesheet("Logout", CURRENTOPERATORID, tmpEventTime)
                    DoPostBackTimesheet("Login", strOperatorID, tmpEventTime)
                    CURRENTOPERATORID = strOperatorID

                    lblUser.Text = lst(0).lastName & ", " & lst(0).firstName
                    CURRENTOPERATORNAME = lblUser.Text
                    clsDC360SM.SaveMemorySetting("DC360_UserName", lblUser.Text)

                    'strA = "id,firstName,lastName" & vbCrLf
                    'For iLoop = 0 To (lst.Count - 1)
                    '    strA += lst(iLoop).id & "|"
                    '    strA += lst(iLoop).firstName & "|"
                    '    strA += lst(iLoop).lastName & vbCrLf
                    'Next
                    'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " operators")
                Else
                    MsgBox("Your login was invalid.  Please try again.", MsgBoxStyle.SystemModal, "Invalid Login")
                End If
        End Select
        UpdateUserBackColor()
        WriteApplicationLog("FORM1 lblUser_Click - Exited")
    End Sub
    Private Sub cmbConfig_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbConfig.SelectedIndexChanged, cmbSmallConfig.SelectedIndexChanged
        If IAMLOADING Then Exit Sub
        WriteApplicationLog("FORM1 cmbConfig_SelectedIndexChanged - Entered")
        If sender.SelectedIndex = PriorDropDownNDX Then Exit Sub 'No need to verify if the same index is selected

        'Per Mark Anderson, this no longer needs to be validated by an Administrator.
        Dim tmpEventTime As String = CreateEventDateTime(Now())
        Dim cntHolder As String = lblCount.Text
        Dim assistHolder As String = cmbAssistants.Text
        Dim configHolder As String = sender.Text
        Dim configNdxHolder As String = sender.SelectedIndex
        Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
        DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
        EVENTSTARTDATE = tmpEventTime

        PriorDropDownNDX = sender.SelectedIndex
        cmbSmallConfig.SelectedIndex = sender.selectedindex
        cmbConfig.SelectedIndex = sender.selectedindex
        currentconfigNDX = cmbConfig.SelectedIndex 'Per Mark Anderson, this no longer needs to be validated by an Administrator.
        clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)

        WriteApplicationLog("FORM1 cmbConfig_SelectedIndexChanged - Exited")
    End Sub
    Private Sub UpdateSettings()
        WriteApplicationLog("FORM1 UpdateSettings - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_settings) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        'Public Class cls_settings
        '   Public resourceID As Int32 '120
        '   Public settingID As Int32 '1
        '   Public settingName As String '"Count prompt frequency"
        '   Public settingValue As String '"60"
        'End Class

        lst = Json_Structures_Cls.Get_settings(strResourceID)
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            strA = "resourceID,settingID,settingName,settingValue" & vbCrLf
            For iLoop = 0 To (lst.Count - 1)
                Select Case lst(iLoop).settingID
                    Case "2" 'Max Assistants
                        cmbAssistants.Items.Clear()
                        cmbSmallAssistants.Items.Clear()

                        For i As Long = 0 To lst(iLoop).settingValue
                            cmbSmallAssistants.Items.Add(i)
                            cmbAssistants.Items.Add(i)
                        Next
                        cmbAssistants.SelectedIndex = 0
                    Case "3" 'Max Speed
                        clsDC360SM.SaveMemorySetting("DC360_MaxSpeed", lst(iLoop).settingValue)

                    Case "4" 'Load flag count
                        LOADFLAGQTY = lst(iLoop).settingValue
                    Case "5" 'Printer selection
                        RESOURCEPRINTER = lst(iLoop).settingValue
                    Case "6" 'Task Update Interval
                        TASKUPDATEINTERVAL = lst(iLoop).settingValue
                        tmrUpdateIntervalTimer.Interval = TASKUPDATEINTERVAL * 1000
                        tmrUpdateIntervalTimer.Enabled = True
                    Case "11" 'Minor Stop Interval
                        MINORSTOPDURATION = lst(iLoop).settingValue
                        clsDC360SM.SaveMemorySetting("DC360_MinorStopDuration", MINORSTOPDURATION)
                    Case "12" 'Dashboard Update Interval (secs)
                        ACTUALSUPDATEINTERVAL = lst(iLoop).settingValue
                        tmrUpdateActualsTimer.Interval = ACTUALSUPDATEINTERVAL * 1000
                    Case "13" 'Bartender Commander Path
                        BARTENDERPATH = lst(iLoop).settingValue
                    Case "14" 'Bartender Template Path
                        TEMPLATEPATH = lst(iLoop).settingValue


                End Select

                'strA += lst(iLoop).resourceID & "|"
                'strA += lst(iLoop).settingID & "|"
                'strA += lst(iLoop).settingName & "|"
                'strA += lst(iLoop).settingValue & vbCrLf
            Next
            ' MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " settings")
        Else
            ' MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error - Get Settings")
        End If

        WriteApplicationLog("FORM1 UpdateSettings - Exited")

    End Sub
    Private Sub LoadCurrentShift()
        WriteApplicationLog("FORM1 LoadCurrentShift - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_getCurrentShift) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        'This Is The Defined Class For settings In Json_Structures_Cls
        '  Public Class cls_getCurrentShift
        '      Public resourceId As Int32  '120
        '      Public start As String      '"2018-04-18T06:00:00"
        '      Public End_ As String       '"2018-04-18T14:00:00"
        '      Public shiftCode As String  ' "A"
        '      Public shiftDate As String  ' "2018-04-18T00:00:00"
        'End Class
        lst = Json_Structures_Cls.Get_getCurrentShift(strResourceID)
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            'strA = "resourceId,start,End_,shiftCode,shiftDate" & vbCrLf
            'For iLoop = 0 To (lst.Count - 1)
            'strA += lst(iLoop).resourceId & "|"
            'strA += lst(iLoop).start & "|"
            'strA += lst(iLoop).End_ & "|"
            'strA += lst(iLoop).shiftCode & "|"
            'strA += lst(iLoop).shiftDate & "|" & vbCrLf
            'Next
            'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " getCurrentEvent")
            'Else
            '   MsgBox(Json_Structures_Cls.strWebResponseErrorMessage, vbOKOnly, "Web Response Error")
            Dim nextShift As DateTime = lst(0).End_
            Try
                tmrShiftChangeTimer.Interval = DateDiff(DateInterval.Second, Now(), nextShift) * 1000 'convert to miliseconds
            Catch
                LoadCurrentShift()
            End Try
            tmrShiftChangeTimer.Enabled = True
        Else
            'Assume shifts are at 6AM, 2PM, 10PM
            Dim FirstShiftStart As Date = New DateTime(Now.Year, Now.Month, Now.Day, 6, 0, 0)
            Dim SecondShiftStart As Date = New DateTime(Now.Year, Now.Month, Now.Day, 14, 0, 0)
            Dim ThirdShiftStart As Date = New DateTime(Now.Year, Now.Month, Now.Day, 22, 0, 0)
            Dim NextFirstShiftStart As Date = DateAdd(DateInterval.Day, 1, FirstShiftStart)

            Dim nextShift As Date

            If FirstShiftStart > Now Then 'we have a winner
                nextShift = FirstShiftStart
            ElseIf SecondShiftStart > Now Then
                nextShift = SecondShiftStart
            ElseIf ThirdShiftStart > Now Then
                nextShift = ThirdShiftStart
            ElseIf NextFirstShiftStart > Now Then
                nextShift = NextFirstShiftStart
            End If
            Try
                tmrShiftChangeTimer.Interval = DateDiff(DateInterval.Second, Now(), nextShift) * 1000 'convert to miliseconds
            Catch
                LoadCurrentShift()
            End Try
            tmrShiftChangeTimer.Enabled = True
        End If

        WriteApplicationLog("FORM1 LoadCurrentShift - Exited")
    End Sub
    Private Sub btnMinimize_Click(sender As Object, e As EventArgs) Handles btnMinimize.Click
        WriteApplicationLog("FORM1 btnMinimize_Click - Entered")
        ChangeView("SMALL")
        WriteApplicationLog("FORM1 btnMinimize_Click - Exited")
    End Sub
    Private Sub btnShowDetails_Click(sender As Object, e As EventArgs) Handles btnShowDetails.Click
        WriteApplicationLog("FORM1 btnShowDetails_Click - Entered")
        ChangeView("LARGE")
        WriteApplicationLog("FORM1 btnShowDetails_Click - Exited")
    End Sub
    Private Sub cmbAssistants_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAssistants.SelectedIndexChanged, cmbSmallAssistants.SelectedIndexChanged
        If sender.SelectedIndex = PriorDropDownNDX Then Exit Sub
        If Not OKFORPOSTBACK Then Exit Sub
        WriteApplicationLog("FORM1 cmbAssistants_SelectedIndexChanged - Entered")
        Dim tmpEventTime As String = CreateEventDateTime(Now())

        Dim cntHolder As String = lblCount.Text
        Dim assistHolder As String = sender.Text
        Dim configHolder As String = cmbConfig.Text
        Dim configNdxHolder As String = cmbConfig.SelectedIndex
        Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
        DoPostBackCostEvent(tmpEventTime, cntHolder, "21",, opCodeNdxHolder,, assistHolder, configHolder, configNdxHolder)

        If Not IAMLOADING Then clsDC360SM.SaveMemorySetting("DC360_Assistants", assistHolder)

        ' DoPostBackCostEvent(tmpEventTime, lblCount.Text, "21")
        EVENTSTARTDATE = tmpEventTime
        PriorDropDownNDX = sender.SelectedIndex
        cmbAssistants.SelectedIndex = sender.selectedindex
        cmbSmallAssistants.SelectedIndex = sender.selectedindex
        currentnoAssistants = cmbAssistants.Text
        WriteApplicationLog("FORM1 cmbAssistants_SelectedIndexChanged - Exited")
    End Sub

    Private Sub cmbOpCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbOpCode.SelectedIndexChanged
        If cmbOpCode.SelectedIndex = PriorDropDownNDX Then Exit Sub
        PriorDropDownNDX = cmbOpCode.SelectedIndex
        WriteApplicationLog("FORM1 cmbOpCode_SelectedIndexChanged - Entered")
        If UNKNOWNSTOP Then cmbOpCode.Text = "Unknown Stop"
        If Not IAMLOADING Then clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Text)
        If Not OKFORPOSTBACK Then Exit Sub
        Dim tmpEventTime As String = CreateEventDateTime(Now())
        Dim cntHolder As String = f.lblCount.Text
        Dim assistHolder As String = cmbAssistants.Text
        Dim configHolder As String = cmbConfig.Text
        Dim configNdxHolder As String = cmbConfig.SelectedIndex
        Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
        DoPostBackCostEvent(tmpEventTime, cntHolder, "21",, opCodeNdxHolder,, assistHolder, configHolder, configNdxHolder)
        EVENTSTARTDATE = tmpEventTime
        currentOpCodeNDX = opCodeNdxHolder
        WriteApplicationLog("FORM1 cmbOpCode_SelectedIndexChanged - Exited")
    End Sub

    Private Sub ComboSelection_Click(sender As Object, e As EventArgs) Handles cmbOpCode.Click, cmbAssistants.Click, cmbConfig.Click, cmbSmallAssistants.Click, cmbSmallConfig.Click
        WriteApplicationLog("FORM1 ComboSelection_Click - Entered")
        PriorDropDownNDX = sender.SelectedIndex
        WriteApplicationLog("FORM1 ComboSelection_Click - Exited")
    End Sub

    Private Sub btnChangeJob_Click(sender As Object, e As EventArgs) Handles btnChangeJob.Click
        WriteApplicationLog("FORM1 btnChangeJob_Click - Entered")
        ChangeJob()
        WriteApplicationLog("FORM1 btnChangeJob_Click - Exited")
    End Sub

    Private Sub PushOpCodeList(haveJob As Boolean)
        WriteApplicationLog("FORM1 PushOpCodeList - Entered")
        Dim OpCodeList() As String : ReDim OpCodeList(0)
        cmbOpCode.Items.Clear()
        'OpCodeList(OpCodeList.GetUpperBound(0)) = lst(iLoop).opAreaName & " - " & lst(iLoop).opCodeName & "~" & lst(iLoop).opCodeTypeColorRGB & "~" & lst(iLoop).opCodeTypeName

        For i As Long = 0 To myOpCodeList.Count - 1
            Select Case myOpCodeList(i).opCodeAreaName.ToUpper
                Case "HIDDEN" : Continue For
                Case "IDLE"
                    If haveJob Then Continue For
                    OpCodeList(OpCodeList.GetUpperBound(0)) = myOpCodeList(i).opCodeName & "~" & myOpCodeList(i).opCodeColor & "~" & myOpCodeList(i).opCodeTypeName
                    cmbOpCode.Items.Add(myOpCodeList(i).opCodeName)
                    ReDim Preserve OpCodeList(OpCodeList.GetUpperBound(0) + 1)
                Case Else
                    If Not haveJob Then Continue For
                    OpCodeList(OpCodeList.GetUpperBound(0)) = myOpCodeList(i).opCodeName & "~" & myOpCodeList(i).opCodeColor & "~" & myOpCodeList(i).opCodeTypeName
                    cmbOpCode.Items.Add(myOpCodeList(i).opCodeName)
                    ReDim Preserve OpCodeList(OpCodeList.GetUpperBound(0) + 1)
            End Select
        Next
        ReDim Preserve OpCodeList(OpCodeList.GetUpperBound(0) - 1)
        clsDC360SM.SaveMemorySetting("DC360_OpCodeList", Join(OpCodeList, "|"))
        WriteApplicationLog("FORM1 PushOpCodeList - Exited")
    End Sub

    Private Sub ChangeJob()
        WriteApplicationLog("FORM1 ChangeJob - Entered")
        Dim changeJobFrm As New frmChangeJob()
        changeJobFrm.StartPosition = FormStartPosition.CenterScreen
        Dim bkgrndOp As New BackgroundOpacity
        bkgrndOp.Show()
        changeJobFrm.ShowDialog()
        bkgrndOp.Dispose()

        Dim tmpEventTime As String = CreateEventDateTime(Now())
        Select Case changeJobFrm.myResult
            Case -2 : Exit Sub 'cancel was hit
            Case 0 'job ended
                UNKNOWNSTOP = False
                Dim cntHolder As String = f.lblCount.Text
                Dim assistHolder As String = f.cmbAssistants.Text
                Dim configHolder As String = f.cmbConfig.Text
                Dim configNdxHolder As String = f.cmbConfig.SelectedIndex
                Dim opCodeNdxHolder As String = IIf(f.cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(f.cmbOpCode.Text))
                DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
                GLOBALSTARTENDOVERRIDE = tmpEventTime
                DoPostBackCostEvent(tmpEventTime, cntHolder, "3", OpCodeNotInUseNdx,, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'End Job

                f.lblLargeJobName.Text = "No Selection"
                f.lblSmallJobName.Text = "No Selection"
                f.lblLargeClient.Text = "No Selection"
                f.lblSmallClient.Text = "No Selection"
                f.lblLargeJobNumber.Text = "No Selection"
                f.lblSmallJobNumber.Text = "No Selection"
                f.lblLargeVersion.Text = GetVersionFromSubJob("No Selection-No Selection")
                f.lblSmallVersion.Text = GetVersionFromSubJob("No Selection-No Selection")
                f.lblCount.Text = "0"
                f.lblFreezeCount.Text = "0"
                CurrentQtyToGo = 0
                LastQtyToGo = "0"
                InitialQtyToGo = "0"
                TotalPieces = "0"
                WastePieces = "0"
                currenttaskID = Nothing
                currentClient = "No Selection"
                currentJobName = "No Selection"
                SUBJOBNO = "No Selection-No Selection"
                ORDERNO = ""
                noUpFromJob = "1"
                noUp = "1"
                cmbNumberUp.Text = "1"
                clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobSpeed", "0")
                clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)
                clsDC360SM.SaveMemorySetting("DC360_Staff", "0")
                clsDC360SM.SaveMemorySetting("DC360_Process", "")

                DoPostBackCostEvent(tmpEventTime, "0", "21", OpCodeNotInUseNdx, OpCodeNotInUseNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Clear Job
                currentOpCodeNDX = OpCodeNotInUseNdx
                PushOpCodeList(False)
                clsDC360SM.SaveMemorySetting("DC360_OpCode", myOpCodeList(currentOpCodeNDX).opCodeName)
                EVENTSTARTDATE = tmpEventTime
                PriorDropDownNDX = f.cmbOpCode.SelectedIndex : f.cmbOpCode.SelectedIndex = -1 : f.cmbOpCode.Text = myOpCodeList(currentOpCodeNDX).opCodeName
                PriorDropDownNDX = f.cmbConfig.SelectedIndex : f.cmbConfig.SelectedIndex = 0
                PriorDropDownNDX = f.cmbAssistants.SelectedIndex : f.cmbAssistants.SelectedIndex = 0
                clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)

                WIP_NUp = ""
                WIP_PartRef = ""
            Case -1 'Job Lifted
                UNKNOWNSTOP = False
                Dim cntHolder As String = lblCount.Text
                Dim assistHolder As String = cmbAssistants.Text
                Dim configHolder As String = cmbConfig.Text
                Dim configNdxHolder As String = cmbConfig.SelectedIndex
                Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
                DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
                GLOBALSTARTENDOVERRIDE = tmpEventTime
                DoPostBackCostEvent(tmpEventTime, cntHolder, "2",, opCodeNdxHolder, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Lift Job

                f.lblLargeJobName.Text = "No Selection"
                f.lblSmallJobName.Text = "No Selection"
                f.lblLargeClient.Text = "No Selection"
                f.lblSmallClient.Text = "No Selection"
                f.lblLargeJobNumber.Text = "No Selection"
                f.lblSmallJobNumber.Text = "No Selection"
                f.lblLargeVersion.Text = GetVersionFromSubJob("No Selection-No Selection")
                f.lblSmallVersion.Text = GetVersionFromSubJob("No Selection-No Selection")
                f.lblCount.Text = "0"
                f.lblFreezeCount.Text = "0"
                LastQtyToGo = "0"
                InitialQtyToGo = "0"
                CurrentQtyToGo = 0
                'TotalImpressions = "0"
                TotalPieces = 0
                'WasteImpressions = "0"
                WastePieces = 0
                currenttaskID = Nothing
                currentClient = "No Selection"
                currentJobName = "No Selection"
                SUBJOBNO = "No Selection-No Selection"
                ORDERNO = ""
                noUpFromJob = "1"
                noUp = "1"
                cmbNumberUp.Text = "1"
                clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobSpeed", "0")
                clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)
                clsDC360SM.SaveMemorySetting("DC360_Staff", "0")
                clsDC360SM.SaveMemorySetting("DC360_Process", "")

                DoPostBackCostEvent(tmpEventTime, "0", "21", OpCodeNotInUseNdx, OpCodeNotInUseNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Clear Job
                currentOpCodeNDX = OpCodeNotInUseNdx
                PushOpCodeList(False)
                clsDC360SM.SaveMemorySetting("DC360_OpCode", myOpCodeList(currentOpCodeNDX).opCodeName)
                EVENTSTARTDATE = tmpEventTime
                PriorDropDownNDX = f.cmbOpCode.SelectedIndex : f.cmbOpCode.SelectedIndex = -1 : f.cmbOpCode.Text = myOpCodeList(currentOpCodeNDX).opCodeName
                PriorDropDownNDX = f.cmbConfig.SelectedIndex : f.cmbConfig.SelectedIndex = 0
                PriorDropDownNDX = f.cmbAssistants.SelectedIndex : f.cmbAssistants.SelectedIndex = 0
                clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)

                WIP_NUp = ""
                WIP_PartRef = ""
            Case -3 'Job Started from no job selected
                UNKNOWNSTOP = False
                Dim cntHolder As String = lblCount.Text
                Dim assistHolder As String = cmbAssistants.Text
                Dim configHolder As String = cmbConfig.Text
                Dim configNdxHolder As String = cmbConfig.SelectedIndex
                DoPostBackCostEvent(tmpEventTime, cntHolder, "21", OpCodeNotInUseNdx,, , assistHolder, configHolder, configNdxHolder)
                GLOBALSTARTENDOVERRIDE = tmpEventTime
                noUpFromJob = changeJobFrm.finalJob.noUp
                noUp = changeJobFrm.finalJob.noUp
                cmbNumberUp.Text = noUp
                f.lblLargeJobName.Text = changeJobFrm.finalJob.jobName
                f.lblSmallJobName.Text = changeJobFrm.finalJob.jobName
                f.lblLargeClient.Text = changeJobFrm.finalJob.company
                f.lblSmallClient.Text = changeJobFrm.finalJob.company
                f.lblLargeJobNumber.Text = changeJobFrm.finalJob.jobNumber.Split("-")(0)
                f.lblSmallJobNumber.Text = changeJobFrm.finalJob.jobNumber.Split("-")(0)
                f.lblLargeVersion.Text = GetVersionFromSubJob(changeJobFrm.finalJob.jobNumber) ' (changeJobFrm.finalJob.jobNumber & "-").Split("-")(1)
                f.lblSmallVersion.Text = GetVersionFromSubJob(changeJobFrm.finalJob.jobNumber) ' (changeJobFrm.finalJob.jobNumber & "-").Split("-")(1)
                f.lblCount.Text = changeJobFrm.finalJob.qtytoGo
                f.lblFreezeCount.Text = lblCount.Text
                LastQtyToGo = lblCount.Text
                CurrentQtyToGo = lblCount.Text
                InitialQtyToGo = lblCount.Text
                'TotalImpressions = changeJobFrm.finalJob.plannedImpressions
                TotalPieces = changeJobFrm.finalJob.plannedCopies

                'WasteImpressions = changeJobFrm.finalJob.wasteImpressions
                WastePieces = changeJobFrm.finalJob.wasteCopies
                currenttaskID = changeJobFrm.finalJob.taskID
                currentClient = lblLargeClient.Text
                currentJobName = lblLargeJobName.Text
                SUBJOBNO = changeJobFrm.finalJob.jobNumber
                ORDERNO = changeJobFrm.finalJob.orderNo
                clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                clsDC360SM.SaveMemorySetting("DC360_PlannedQty", TotalPieces)
                clsDC360SM.SaveMemorySetting("DC360_JobSpeed", changeJobFrm.finalJob.jobSpeed)
                clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
                clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)
                clsDC360SM.SaveMemorySetting("DC360_Staff", changeJobFrm.finalJob.staff)
                clsDC360SM.SaveMemorySetting("DC360_Process", changeJobFrm.finalJob.process)

                If changeJobFrm.finalJob.makeReadyMins = 0 Then
                    'Start in Running
                    DoPostBackCostEvent(tmpEventTime, LastQtyToGo, "1", opCodeJobStartRunningNdx, opCodeJobStartRunningNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Start Job
                    currentOpCodeNDX = opCodeJobStartRunningNdx
                Else
                    'Start in Setup
                    DoPostBackCostEvent(tmpEventTime, LastQtyToGo, "1", opCodeJobStartSetupNdx, opCodeJobStartSetupNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Start Job
                    currentOpCodeNDX = opCodeJobStartSetupNdx
                End If
                PushOpCodeList(True)
                clsDC360SM.SaveMemorySetting("DC360_OpCode", myOpCodeList(currentOpCodeNDX).opCodeName)
                EVENTSTARTDATE = tmpEventTime
                PriorDropDownNDX = f.cmbOpCode.SelectedIndex : f.cmbOpCode.SelectedIndex = -1 : f.cmbOpCode.Text = myOpCodeList(currentOpCodeNDX).opCodeName
                PriorDropDownNDX = f.cmbConfig.SelectedIndex : f.cmbConfig.SelectedIndex = GetConfigNdxByID(changeJobFrm.finalJob.configID)
                PriorDropDownNDX = f.cmbAssistants.SelectedIndex : f.cmbAssistants.SelectedIndex = 0
                clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)

                WIP_NUp = ""
                WIP_PartRef = ""
        End Select

        noUpFromJob = changeJobFrm.finalJob.noUp
        noUp = changeJobFrm.finalJob.noUp
        cmbNumberUp.Text = noUp

        Try
            Dim LocalNumberUp As String = Val(GetSetting("DCL360_Machine_Counts", "Jobs\" & lblLargeJobNumber.Text & "-" & lblLargeVersion.Text, "NumberUp"))
            If LocalNumberUp <> "0" Then
                LocalNumberUp = LocalNumberUp.Split("-")(0)
                noUp = LocalNumberUp
                cmbNumberUp.Text = LocalNumberUp

                clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                If noUp = noUpFromJob Then
                    lblNoUp.BackColor = (New Label).BackColor
                Else
                    lblNoUp.BackColor = Color.Yellow
                End If
            End If
        Catch ex As Exception
        End Try

        f.lblLargeJobName.Text = changeJobFrm.finalJob.jobName
        f.lblSmallJobName.Text = changeJobFrm.finalJob.jobName
        f.lblLargeClient.Text = changeJobFrm.finalJob.company
        f.lblSmallClient.Text = changeJobFrm.finalJob.company
        f.lblLargeJobNumber.Text = changeJobFrm.finalJob.jobNumber.Split("-")(0)
        f.lblSmallJobNumber.Text = changeJobFrm.finalJob.jobNumber.Split("-")(0)
        f.lblLargeVersion.Text = GetVersionFromSubJob(changeJobFrm.finalJob.jobNumber) ' (changeJobFrm.finalJob.jobNumber & "-").Split("-")(1)
        f.lblSmallVersion.Text = GetVersionFromSubJob(changeJobFrm.finalJob.jobNumber) ' (changeJobFrm.finalJob.jobNumber & "-").Split("-")(1)
        f.lblCount.Text = changeJobFrm.finalJob.qtytoGo
        f.lblFreezeCount.Text = lblCount.Text
        LastQtyToGo = lblCount.Text
        InitialQtyToGo = lblCount.Text
        CurrentQtyToGo = lblCount.Text
        'TotalImpressions = changeJobFrm.finalJob.plannedImpressions
        TotalPieces = changeJobFrm.finalJob.plannedCopies
        'WasteImpressions = changeJobFrm.finalJob.wasteImpressions
        WastePieces = changeJobFrm.finalJob.wasteCopies
        currenttaskID = changeJobFrm.finalJob.taskID
        currentClient = lblLargeClient.Text
        currentJobName = lblLargeJobName.Text
        SUBJOBNO = changeJobFrm.finalJob.jobNumber
        ORDERNO = changeJobFrm.finalJob.orderNo
        clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
        clsDC360SM.SaveMemorySetting("DC360_PlannedQty", TotalPieces)
        clsDC360SM.SaveMemorySetting("DC360_JobSpeed", changeJobFrm.finalJob.jobSpeed)
        clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
        clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
        clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)
        clsDC360SM.SaveMemorySetting("DC360_Staff", changeJobFrm.finalJob.staff)
        clsDC360SM.SaveMemorySetting("DC360_Process", changeJobFrm.finalJob.process)


        If changeJobFrm.myResult <> -3 And changeJobFrm.finalJob.company <> "No Selection" Then 'If we are starting a real job
            UNKNOWNSTOP = False
            Dim cntHolder As String = lblCount.Text
            Dim assistHolder As String = cmbAssistants.Text
            Dim configHolder As String = cmbConfig.Text
            Dim configNdxHolder As String = cmbConfig.SelectedIndex
            If changeJobFrm.finalJob.makeReadyMins = 0 Then
                'Start in Running
                DoPostBackCostEvent(tmpEventTime, LastQtyToGo, "1", opCodeJobStartRunningNdx, opCodeJobStartRunningNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Start Job
                currentOpCodeNDX = opCodeJobStartRunningNdx
            Else
                'Start in Setup
                DoPostBackCostEvent(tmpEventTime, LastQtyToGo, "1", opCodeJobStartSetupNdx, opCodeJobStartSetupNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Start Job
                currentOpCodeNDX = opCodeJobStartSetupNdx
            End If
            PushOpCodeList(True)
            'MessageBox.Show(myOpCodeList(currentOpCodeNDX).opCodeName)
            clsDC360SM.SaveMemorySetting("DC360_OpCode", myOpCodeList(currentOpCodeNDX).opCodeName)
            PriorDropDownNDX = cmbOpCode.SelectedIndex : cmbOpCode.SelectedIndex = -1 : cmbOpCode.Text = myOpCodeList(currentOpCodeNDX).opCodeName
            PriorDropDownNDX = cmbConfig.SelectedIndex : cmbConfig.SelectedIndex = GetConfigNdxByID(changeJobFrm.finalJob.configID)
            PriorDropDownNDX = cmbAssistants.SelectedIndex : cmbAssistants.SelectedIndex = 0
            clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)
            'MessageBox.Show(cmbOpCode.Text)

            WIP_NUp = ""
            WIP_PartRef = ""
        End If

        GLOBALSTARTENDOVERRIDE = ""
        UpdateUserBackColor()
        Try
            If currenttaskID >= 0 Then
                btnChangeJob.Text = "Change/Stop Job"
            Else
                btnChangeJob.Text = "Change-Stop Job"
            End If
        Catch ex As Exception
            btnChangeJob.Text = "Change/Stop Job"
        End Try


        If lblLargeJobNumber.Text = "No Selection" Then
            btnWIP.Visible = False
        Else
            btnWIP.Visible = True
        End If

        WriteApplicationLog("FORM1 ChangeJob - Exited")
    End Sub

    Private Sub UpdateUserBackColor()
        WriteApplicationLog("FORM1 UpdateUserBackColor - Entered")
        If currenttaskID Is Nothing Then
            lblUser.BackColor = (New Label).BackColor
        Else
            If CURRENTOPERATORID = "" Or CURRENTOPERATORID = "0" Then
                lblUser.BackColor = Color.Red
            Else
                lblUser.BackColor = (New Label).BackColor
            End If
        End If
        WriteApplicationLog("FORM1 UpdateUserBackColor - Exited")
    End Sub

    Private Sub UpdateOpCodes()
        WriteApplicationLog("FORM1 UpdateOpCodes - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_opCodes) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost       'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion 'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        'This Is The Defined Class For opCodes In Json_Structures_Cls
        'Public Class cls_opCodes
        '   Public opCodeID As Integer            '3615
        '   Public opUnitID As Integer            '1675
        '   Public opAreaID As Integer            '1210
        '   Public opCodeTypeColorRGB As String '"RGB(128,255,255)"
        '   Public opCodeName As String         '"No Work"
        '   Public opCodeTypeName As String     '"Idle"
        '   Public opAreaName As String         '"Idle"
        'End Class


        lst = Json_Structures_Cls.Get_opCodes(strResourceID)
        strA = "opAreaID,opAreaName,opCodeID,opCodeName,opCodeTypeColorRGB.opCodeTypeName" & vbCrLf
        ' MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " opCodes")
        ' MessageBox.Show("Start Op Code List Function")

        ReDim myOpCodeList(lst.Count - 1)
        cmbOpCode.Items.Clear()
        Dim OpCodeList() As String : ReDim OpCodeList(0)
        For iLoop = 0 To (lst.Count - 1)

            myOpCodeList(iLoop).opCodeID = lst(iLoop).opCodeID
            myOpCodeList(iLoop).opCodeColor = lst(iLoop).opCodeTypeColorRGB
            myOpCodeList(iLoop).opCodeName = lst(iLoop).opCodeName
            myOpCodeList(iLoop).opCodeAreaID = lst(iLoop).opAreaID
            myOpCodeList(iLoop).opCodeUnitID = lst(iLoop).opUnitID
            myOpCodeList(iLoop).opCodeAreaName = lst(iLoop).opAreaName
            myOpCodeList(iLoop).opCodeTypeName = lst(iLoop).opCodeTypeName

            If lst(iLoop).specialDesc.ToUpper = "REJECT" Then opCodeRejectGoodNdx = iLoop
            If lst(iLoop).specialDesc.ToUpper = "ADD GOOD" Then opCodeAddGoodNdx = iLoop
            If lst(iLoop).specialDesc.ToUpper = "UNKNOWN OPERATION" Then opCodeUnknownStopNdx = iLoop
            If lst(iLoop).specialDesc.ToUpper = "MINOR STOP" Then opCodeMinorStopNdx = iLoop
            If lst(iLoop).specialDesc.ToUpper = "JOB START" Then opCodeJobStartSetupNdx = iLoop
            If lst(iLoop).specialDesc.ToUpper = "RUNNING" Then opCodeJobStartRunningNdx = iLoop
            If lst(iLoop).specialDesc.ToUpper = "NOT IN USE" Then OpCodeNotInUseNdx = iLoop

            If lst(iLoop).opAreaName.ToUpper.StartsWith("HIDDEN") Then Continue For
            cmbOpCode.Items.Add(lst(iLoop).opAreaName & " - " & lst(iLoop).opCodeName)

            OpCodeList(OpCodeList.GetUpperBound(0)) = lst(iLoop).opAreaName & " - " & lst(iLoop).opCodeName & "~" & lst(iLoop).opCodeTypeColorRGB & "~" & lst(iLoop).opCodeTypeName

            ReDim Preserve OpCodeList(OpCodeList.GetUpperBound(0) + 1)

            'strA += lst(iLoop).opAreaID & "|"
            'strA += lst(iLoop).opAreaName & "|"
            'strA += lst(iLoop).opCodeID & "|"
            'strA += lst(iLoop).opCodeName & "|"
            'strA += lst(iLoop).opCodeTypeColorRGB & "|"
            'strA += lst(iLoop).opCodeTypeName & "|"
            'strA += lst(iLoop).opCodeTypeName & vbCrLf
        Next
        ' MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " opCodes")
        'MessageBox.Show("Before Op Code List Save")

        ReDim Preserve OpCodeList(OpCodeList.GetUpperBound(0) - 1)
        ' clsDC360SM.SaveMemorySetting("DC360_OpCodeList", Join(OpCodeList, "|"))
        'MessageBox.Show("After Op Code List Save")
        WriteApplicationLog("FORM1 UpdateOpCodes - Exited")
    End Sub

    Private Sub UpdateConfig()
        WriteApplicationLog("FORM1 UpdateConfig - Entered")
        Dim lst As List(Of Json_Structures_Cls.cls_configs) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strResourceID As String = RESOURCEID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost       'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion 'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'Json_Structures_Cls.strCorrelationID 'This Is Changeable, Default is: 1

        'This Is The Defined Class For configs In Json_Structures_Cls
        'Public Class cls_configs
        '    Public configId As Int32            '14
        '    Public configCode As String         '"2501: Press25 Base"
        '    Public configDescription As String  '"2501 - Press25 - Base"
        'End Class

        lst = Json_Structures_Cls.Get_configs(strResourceID)
        strA = "configCode,configDescription,configId" & vbCrLf

        ReDim myConfigList(lst.Count)
        cmbConfig.Items.Clear()
        cmbSmallConfig.Items.Clear()

        Dim configListToSend() As String
        ReDim configListToSend(lst.Count)

        'Add In Default Config
        myConfigList(0).ConfigID = "0"
        myConfigList(0).ConfigName = "No Addl Services"
        myConfigList(0).ConfigDDNdx = 0

        cmbConfig.Items.Add("No Addl Services")
        cmbSmallConfig.Items.Add("No Addl Services")

        For iLoop = 0 To (lst.Count - 1)
            myConfigList(iLoop + 1).ConfigID = lst(iLoop).configId
            myConfigList(iLoop + 1).ConfigName = lst(iLoop).configDescription
            myConfigList(iLoop + 1).ConfigDDNdx = iLoop + 1

            cmbConfig.Items.Add(lst(iLoop).configDescription)
            cmbSmallConfig.Items.Add(lst(iLoop).configDescription)

            configListToSend(iLoop) = lst(iLoop).configDescription

            'strA += lst(iLoop).configCode & "|"
            'strA += lst(iLoop).configDescription & "|"
            'strA += lst(iLoop).configId & vbCrLf
        Next
        'MsgBox(strA, vbOKOnly, "Returned: " & lst.Count & " configs")
        AdjustWidthComboBox_DropDown(cmbSmallConfig)
        AdjustWidthComboBox_DropDown(cmbConfig)

        clsDC360SM.SaveMemorySetting("DC360_JobConfigList", Join(configListToSend, "|"))

        clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)
        WriteApplicationLog("FORM1 UpdateConfig - Exited")
    End Sub

    Private Sub lblSmall_TextChanged(sender As Object, e As EventArgs) Handles lblSmallJobName.TextChanged, lblSmallClient.TextChanged, lblSmallJobNumber.TextChanged, lblSmallVersion.TextChanged, lblLargeClient.TextChanged, lblLargeJobName.TextChanged, lblLargeJobNumber.TextChanged, lblLargeVersion.TextChanged, lblUser.TextChanged
        WriteApplicationLog("FORM1 lblSmall_TextChanged - Entered")
        CheckFont(sender)
        WriteApplicationLog("FORM1 lblSmall_TextChanged - Exited")
    End Sub

    Private Sub btnGoodToWaste_Click(sender As Object, e As EventArgs) Handles btnGoodToWaste.Click
        WriteApplicationLog("FORM1 btnGoodToWaste_Click - Entered")
        If IAMLOADING Then Exit Sub
        Dim gwFrm As New frmKeyPad("GoodWaste", "Transfer Stock", "Enter Amount Below:")
        gwFrm.StartPosition = FormStartPosition.CenterScreen
        Dim bkgrndOp As New BackgroundOpacity
        bkgrndOp.Show()
        gwFrm.ShowDialog()
        bkgrndOp.Dispose()

        Select Case gwFrm.myResult
            Case -1 'Transfer To Good
                Dim tmpEventTime As String = CreateEventDateTime(Now())
                Dim cntHolder As String = lblCount.Text
                Dim assistHolder As String = cmbAssistants.Text
                Dim configHolder As String = cmbConfig.Text
                Dim configNdxHolder As String = cmbConfig.SelectedIndex
                Dim alteredQty As String = Math.Ceiling(gwFrm.myKeyedEntry * 1.0 / noUp) * noUp 'round up the the nearest impression
                If WastePieces < alteredQty Then
                    MessageBox.Show("ERROR: Your quantity to transfer to good exceeds the current waste amount of " & WastePieces & ".", "ERROR: Quantity invalid", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    DoPostBackCostEvent(tmpEventTime, alteredQty, "26", opCodeAddGoodNdx,, tmpEventTime, assistHolder, configHolder, configNdxHolder)
                End If
            Case -2 'Cancel Event

            Case -3 'Transfer To Waste
                Dim tmpEventTime As String = CreateEventDateTime(Now())
                Dim cntHolder As String = lblCount.Text
                Dim assistHolder As String = cmbAssistants.Text
                Dim configHolder As String = cmbConfig.Text
                Dim configNdxHolder As String = cmbConfig.SelectedIndex
                Dim alteredQty As String = Math.Ceiling(gwFrm.myKeyedEntry * 1.0 / noUp) * noUp 'round up the the nearest impression
                If (TotalPieces - LastQtyToGo) < alteredQty Then
                    MessageBox.Show("ERROR: Your quantity to transfer to waste exceeds the current good amount of " & (TotalPieces - LastQtyToGo) & ".", "ERROR: Quantity invalid", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    DoPostBackCostEvent(tmpEventTime, alteredQty, "27", opCodeRejectGoodNdx,, tmpEventTime, assistHolder, configHolder, configNdxHolder)
                End If


        End Select
        WriteApplicationLog("FORM1 btnGoodToWaste_Click - Exited")
    End Sub

#Region "TIMERS"
    'Private Sub tmrCount_Tick(sender As Object, e As EventArgs) Handles tmrCount.Tick
    '    If IAMLOADING Then Exit Sub
    '    tmrCount.Enabled = False

    '    lblError.Visible = SHOWERROR
    '    If SHOWERROR Then
    '        If lblError.BackColor = Color.Red Then
    '            lblError.BackColor = Color.Yellow
    '        Else
    '            lblError.BackColor = Color.Red
    '        End If
    '    End If

    '    If VIT_IS_RUNNING Then 'VIT is present
    '        lblCount.Text = InitialQtyToGo - GetSetting("iVu", "Config", "GrossCount")
    '    End If

    '    If myOpCodeList(currentOpCodeNDX).opCodeTypeName = "Waste" Or myOpCodeList(currentOpCodeNDX).opCodeTypeName = "Setup" Then 'Don't show the updated quantity
    '        lblFreezeCount.Visible = True
    '        If TotalImpressions = 0 Then
    '            pgrStatus.Value = 0
    '        Else

    '            Dim progValue As Long = Int((1.0 - ((lblFreezeCount.Text * 1.0) / (TotalImpressions * 1.0))) * 100)
    '            Try
    '                pgrStatus.Value = progValue
    '            Catch ex As Exception
    '                If progValue > 100 Then
    '                    pgrStatus.Value = 100
    '                Else
    '                    pgrStatus.Value = 0
    '                End If
    '            End Try
    '        End If
    '        clsDC360SM.SaveMemorySetting("DC360_QtyToGo", lblFreezeCount.Text)
    '        lblComplete.Text = pgrStatus.Value & "% Complete"
    '    Else
    '        lblFreezeCount.Visible = False
    '        If TotalImpressions = 0 Then
    '            pgrStatus.Value = 0
    '        Else

    '            Dim progValue As Long = Int((1.0 - ((lblCount.Text * 1.0) / (TotalImpressions * 1.0))) * 100)
    '            Try
    '                pgrStatus.Value = progValue
    '            Catch ex As Exception
    '                If progValue > 100 Then
    '                    pgrStatus.Value = 100
    '                Else
    '                    pgrStatus.Value = 0
    '                End If
    '            End Try
    '        End If
    '        clsDC360SM.SaveMemorySetting("DC360_QtyToGo", lblCount.Text)
    '        lblComplete.Text = pgrStatus.Value & "% Complete"
    '    End If

    '    lblCount.Visible = Not lblFreezeCount.Visible
    '    tmrCount.Enabled = True
    'End Sub
    'Private Sub tmrUpdateInterval_Tick(sender As Object, e As EventArgs) Handles tmrUpdateInterval.Tick
    '    If currenttaskID Is Nothing Then Exit Sub

    '    tmrUpdateInterval.Enabled = False

    '    Dim lst As List(Of Json_Structures_Cls.cls_tasks) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
    '    Dim iLoop As Integer = 0
    '    Dim strA As String = ""
    '    Dim strTaskID As String = currenttaskID

    '    Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
    '    Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
    '    Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
    '    Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

    '    lst = Json_Structures_Cls.Get_tasks(strTaskID)
    '    If Json_Structures_Cls.bWebResponseSuccess = True Then
    '        If Not lst Is Nothing Then

    '            lblLargeJobName.Text = lst(iLoop).taskName
    '            lblSmallJobName.Text = lst(iLoop).taskName
    '            lblLargeClient.Text = lst(iLoop).company
    '            lblSmallClient.Text = lst(iLoop).company
    '            lblLargeJobNumber.Text = lst(iLoop).subJobNo.Split("-")(0)
    '            lblSmallJobNumber.Text = lst(iLoop).subJobNo.Split("-")(0)
    '            lblLargeVersion.Text = GetVersionFromSubJob(lst(iLoop).subJobNo)
    '            lblSmallVersion.Text = GetVersionFromSubJob(lst(iLoop).subJobNo)
    '            noUp = lst(iLoop).noUp

    '            If TotalImpressions <> lst(iLoop).plannedCopies Or LastQtyToGo <> lst(iLoop).qtytoGo Then
    '                Dim QtyToGoOffset As Long = LastQtyToGo - lst(iLoop).qtytoGo
    '                InitialQtyToGo -= QtyToGoOffset
    '                LastQtyToGo -= QtyToGoOffset
    '                TotalImpressions = lst(iLoop).plannedCopies
    '            End If

    '            lblFreezeCount.Text = LastQtyToGo
    '            WasteImpressions = lst(iLoop).wasteImpressions
    '            SUBJOBNO = lst(iLoop).subJobNo
    '            clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
    '        End If

    '    End If
    '    tmrUpdateInterval.Enabled = True
    'End Sub

    'Private Sub tmrShiftChange_Tick(sender As Object, e As EventArgs) Handles tmrShiftChange.Tick
    '    tmrShiftChange.Enabled = False
    '    Dim tmpEventTime As String = CreateEventDateTime(Now())
    '    Dim cntHolder As String = lblCount.Text
    '    Dim assistHolder As String = cmbAssistants.Text
    '    Dim configHolder As String = cmbConfig.Text
    '    Dim configNdxHolder As String = cmbConfig.SelectedIndex
    '    Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
    '    DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
    '    EVENTSTARTDATE = tmpEventTime
    '    DoPostBackCostEvent(tmpEventTime, cntHolder, "45", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)

    '    Try
    '        Dim tmpLogName As String = Now().ToString("yyyy_MM_dd_hh_mm_ss")

    '        Dim hostFolder As String = ""
    '        If strHost.ToUpper.Contains("DEV") Then hostFolder = "DEV\"
    '        If strHost.ToUpper.Contains("UAT") Then hostFolder = "UAT\"
    '        If strHost.ToUpper.Contains("PROD") Then hostFolder = "PROD\"

    '        If Not IO.Directory.Exists("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME) Then
    '            IO.Directory.CreateDirectory("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME)
    '        End If

    '        IO.File.Move(strDC360RootPath & strDC360LogFileName, strDC360RootPath & strDC360LogFileName & tmpLogName)
    '        IO.File.Copy(strDC360RootPath & strDC360LogFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360LogFileName & tmpLogName)

    '        IO.File.Move(strDC360RootPath & strDC360FaildsPOSTSFileName, strDC360RootPath & strDC360FaildsPOSTSFileName & tmpLogName)
    '        IO.File.Copy(strDC360RootPath & strDC360FaildsPOSTSFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360FaildsPOSTSFileName & tmpLogName)

    '    Catch ex As Exception

    '    End Try

    '    Try
    '        Dim tmpLogName As String = Now().ToString("yyyy_MM_dd_hh_mm_ss")

    '        Dim hostFolder As String = ""
    '        If strHost.ToUpper.Contains("DEV") Then hostFolder = "DEV\"
    '        If strHost.ToUpper.Contains("UAT") Then hostFolder = "UAT\"
    '        If strHost.ToUpper.Contains("PROD") Then hostFolder = "PROD\"

    '        If Not IO.Directory.Exists("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME) Then
    '            IO.Directory.CreateDirectory("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME)
    '        End If

    '        IO.File.Move(strDC360RootPath & strDC360ApplicationLogFileName, strDC360RootPath & strDC360ApplicationLogFileName & tmpLogName)
    '        IO.File.Move(strDC360RootPath & strDC360ApplicationLogFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360ApplicationLogFileName & tmpLogName)

    '    Catch ex As Exception

    '    End Try
    '    LoadCurrentShift()

    'End Sub


#End Region

#Region "SYSTEMS TIMER"

    Private Sub tmrRePostFailed_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles tmrRePostFailed.Elapsed
        SaveSetting("iVu", "Config", "DC360RepostTimer", tmrRePostFailed.Enabled.ToString)
        If IAMLOADING Then Exit Sub
        tmrRePostFailed.Enabled = False

        Dim tmpEventTime As String = CreateEventDateTime(Now())

        If IO.File.Exists(strDC360RootPath & strDC360RePostLogFileName) Then

            Dim tmpLogName As String = Now().ToString("yyyy_MM_dd_hh_mm_ss") & ".txt"
            'Move the file so we are only reading from a copy.  That should reduce errors.
            Try
                IO.File.Move(strDC360RootPath & strDC360RePostLogFileName, strDC360RootPath & strDC360RePostLogFileName & tmpLogName)

            Catch ex As Exception
                'If this didn't work then exit sub and try again later
                'This is probably due to the file being written to.
                tmrRePostFailed.Enabled = True
                Exit Sub
            End Try

            'Read from the moved file in case the application is still appending to the RePost file
            Dim toBeRerun As New List(Of RePost)
            Dim inTs As New IO.StreamReader(strDC360RootPath & strDC360RePostLogFileName & tmpLogName, System.Text.Encoding.Default)
            With inTs
                Do While .Peek <> -1
                    Dim line As String = .ReadLine
                    Dim lineSplit() As String = line.Split(vbTab)
                    Dim aRepost As New RePost
                    '   0              1                 2               3                      4                     5                        6                  7                   8                  9                                    
                    '   Now & vbTab & "POST" & vbTab & "FAIL" & vbTab & "n/a" & "ms" & vbTab & strhttpRoot & vbTab & strhttpPath & vbTab & strCorID & vbTab & strVersion & vbTab & strNotes & vbTab & strJSonData
                    aRepost.wholeLine = line
                    aRepost.strhttpRoot = lineSplit(4)
                    aRepost.strhttpPath = lineSplit(5)
                    aRepost.strCorID = lineSplit(6)
                    aRepost.strVersion = lineSplit(7)
                    aRepost.strNotes = lineSplit(8)
                    aRepost.strJSonData = lineSplit(9)

                    toBeRerun.Add(aRepost)
                Loop
            End With
            inTs.Close()

            Dim hostFolder As String = ""
            If strHost.ToUpper.Contains("DEV") Then hostFolder = "DEV\"
            If strHost.ToUpper.Contains("UAT") Then hostFolder = "UAT\"
            If strHost.ToUpper.Contains("PROD") Then hostFolder = "PROD\"

            Try
                If Not IO.Directory.Exists("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME) Then
                    IO.Directory.CreateDirectory("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME)
                End If

                'Try ...moved to If Not IO.Directory.Exists
                IO.File.Move(strDC360RootPath & strDC360RePostLogFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360RePostLogFileName & tmpLogName)

            Catch ex As Exception
                'If this didn't work, no worries.  Keep going
            End Try


            Dim i As Long = 1 'Ignore header this will be the index for our list
            Try
                'Ignore header
                For i = 1 To toBeRerun.Count - 1
                    RerunPost(toBeRerun(i).strhttpRoot, toBeRerun(i).strhttpPath, toBeRerun(i).strJSonData, toBeRerun(i).strVersion, toBeRerun(i).strCorID)
                Next

            Catch ex As Exception

            Finally
                'write out any remaining items in the toBeRun area
                For j As Long = i To toBeRerun.Count - 1
                    ReWritePost(toBeRerun(j).strJSonData, toBeRerun(j).strhttpRoot, toBeRerun(j).strhttpPath, toBeRerun(j).strVersion, toBeRerun(j).strCorID)
                Next
                tmrRePostFailed.Enabled = True
            End Try
        End If


        tmrRePostFailed.Enabled = True
    End Sub

    Private Sub tmrCountTimer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles tmrCountTimer.Elapsed
        If IAMLOADING Then Exit Sub
        tmrCountTimer.Enabled = False
        Try


            lblError.Visible = SHOWERROR
            If SHOWERROR Then
                If lblError.BackColor = Color.Red Then
                    lblError.BackColor = Color.Yellow
                Else
                    lblError.BackColor = Color.Red
                End If
            End If

            If VIT_IS_RUNNING Then 'VIT is present
                lblCount.Text = InitialQtyToGo - Val(GetSetting("iVu", "Config", "GrossCount"))
            End If

            If myOpCodeList(currentOpCodeNDX).opCodeTypeName = "Waste" Or myOpCodeList(currentOpCodeNDX).opCodeTypeName = "Setup" Then 'Don't show the updated quantity
                lblFreezeCount.Visible = True
                If TotalPieces = 0 Then
                    pgrStatus.Value = 0
                Else

                    Dim progValue As Long = Int((1.0 - ((lblFreezeCount.Text * 1.0) / (TotalPieces * 1.0))) * 100)
                    Try
                        pgrStatus.Value = progValue
                    Catch ex As Exception
                        If progValue > 100 Then
                            pgrStatus.Value = 100
                        Else
                            pgrStatus.Value = 0
                        End If
                    End Try
                End If
                clsDC360SM.SaveMemorySetting("DC360_QtyToGo", lblFreezeCount.Text)
                lblComplete.Text = pgrStatus.Value & "% Complete"
            Else
                lblFreezeCount.Visible = False
                If TotalPieces = 0 Then
                    pgrStatus.Value = 0
                Else

                    Dim progValue As Long = Int((1.0 - ((lblCount.Text * 1.0) / (TotalPieces * 1.0))) * 100)
                    Try
                        pgrStatus.Value = progValue
                    Catch ex As Exception
                        If progValue > 100 Then
                            pgrStatus.Value = 100
                        Else
                            pgrStatus.Value = 0
                        End If
                    End Try
                End If
                clsDC360SM.SaveMemorySetting("DC360_QtyToGo", lblCount.Text)
                lblComplete.Text = pgrStatus.Value & "% Complete"
            End If

            lblCount.Visible = Not lblFreezeCount.Visible
            If lblCount.Visible Then
                CurrentQtyToGo = lblCount.Text
            Else
                CurrentQtyToGo = lblFreezeCount.Text
            End If
        Catch ex As Exception

        Finally
            tmrCountTimer.Enabled = True
        End Try

    End Sub

    Private Sub tmrShiftChangeTimer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles tmrShiftChangeTimer.Elapsed
        WriteApplicationLog("FORM1 tmrShiftChangeTimer_Elapsed - Entered")
        tmrShiftChangeTimer.Enabled = False

        Dim tmpEventTime As String = CreateEventDateTime(Now())
        Dim cntHolder As String = lblCount.Text
        Dim assistHolder As String = cmbAssistants.Text
        Dim configHolder As String = cmbConfig.Text
        Dim configNdxHolder As String = cmbConfig.SelectedIndex
        Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
        DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
        EVENTSTARTDATE = tmpEventTime
        DoPostBackCostEvent(tmpEventTime, cntHolder, "45", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)

        Try
            Dim tmpLogName As String = Now().ToString("yyyy_MM_dd_hh_mm_ss") & ".txt"

            Dim hostFolder As String = ""
            If strHost.ToUpper.Contains("DEV") Then hostFolder = "DEV\"
            If strHost.ToUpper.Contains("UAT") Then hostFolder = "UAT\"
            If strHost.ToUpper.Contains("PROD") Then hostFolder = "PROD\"

            If Not IO.Directory.Exists("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME) Then
                IO.Directory.CreateDirectory("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME)
            End If

            IO.File.Move(strDC360RootPath & strDC360LogFileName, strDC360RootPath & strDC360LogFileName & tmpLogName)
            IO.File.Copy(strDC360RootPath & strDC360LogFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360LogFileName & tmpLogName)

            IO.File.Move(strDC360RootPath & strDC360FaildsPOSTSFileName, strDC360RootPath & strDC360FaildsPOSTSFileName & tmpLogName)
            IO.File.Copy(strDC360RootPath & strDC360FaildsPOSTSFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360FaildsPOSTSFileName & tmpLogName)

        Catch ex As Exception

        End Try

        Try
            Dim tmpLogName As String = Now().ToString("yyyy_MM_dd_hh_mm_ss") & ".txt"

            Dim hostFolder As String = ""
            If strHost.ToUpper.Contains("DEV") Then hostFolder = "DEV\"
            If strHost.ToUpper.Contains("UAT") Then hostFolder = "UAT\"
            If strHost.ToUpper.Contains("PROD") Then hostFolder = "PROD\"

            If Not IO.Directory.Exists("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME) Then
                IO.Directory.CreateDirectory("\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME)
            End If

            IO.File.Move(strDC360RootPath & strDC360ApplicationLogFileName, strDC360RootPath & strDC360ApplicationLogFileName & tmpLogName)
            IO.File.Move(strDC360RootPath & strDC360ApplicationLogFileName & tmpLogName, "\\j-drive\J_Drive\DC360_Log\" & hostFolder & RESOURCENAME & strDC360ApplicationLogFileName & tmpLogName)

        Catch ex As Exception

        End Try
        LoadCurrentShift()
        WriteApplicationLog("FORM1 tmrShiftChangeTimer_Elapsed - Exited")
    End Sub

    Private Sub tmrUpdateIntervalTimer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles tmrUpdateIntervalTimer.Elapsed
        WriteApplicationLog("FORM1 tmrUpdateIntervalTimer_Elapsed - Entered")
        If currenttaskID Is Nothing Then Exit Sub
        If currenttaskID <= 0 Then Exit Sub

        tmrUpdateIntervalTimer.Enabled = False

        Dim lst As List(Of Json_Structures_Cls.cls_tasks) '<- Make Sure You Set The Correct Class, In This Case We Want opCodes
        Dim iLoop As Integer = 0
        Dim strA As String = ""
        Dim strTaskID As String = currenttaskID

        Json_Structures_Cls.strHost = Json_Structures_Cls.strHost                   'This Is Changeable, Default is: "http://sg360-apps-01:8083"
        Json_Structures_Cls.strVersion = Json_Structures_Cls.strVersion             'This Is Changeable, Default is: "v1"
        Json_Structures_Cls.iHttpTimeout = Json_Structures_Cls.iHttpTimeout         'This Is Changeable, Default is: 5000
        Json_Structures_Cls.strCorrelationID = GetCorrelationID() 'This Is Changeable, Default is: 1

        lst = Json_Structures_Cls.Get_tasks(strTaskID)
        If Json_Structures_Cls.bWebResponseSuccess = True Then
            If Not lst Is Nothing Then

                lblLargeJobName.Text = lst(iLoop).taskName
                lblSmallJobName.Text = lst(iLoop).taskName
                lblLargeClient.Text = lst(iLoop).company
                lblSmallClient.Text = lst(iLoop).company
                lblLargeJobNumber.Text = lst(iLoop).subJobNo.Split("-")(0)
                lblSmallJobNumber.Text = lst(iLoop).subJobNo.Split("-")(0)
                lblLargeVersion.Text = GetVersionFromSubJob(lst(iLoop).subJobNo)
                lblSmallVersion.Text = GetVersionFromSubJob(lst(iLoop).subJobNo)

                'Only update the number up if the operator has NOT changed it.
                'The background of the number up label will be yellow if it was changed.
                If lblNoUp.BackColor = Color.Yellow Then
                    'Operator changed so only update noUpFromJob
                    noUpFromJob = lst(iLoop).noUp
                Else
                    'Update as normal
                    noUpFromJob = lst(iLoop).noUp
                    noUp = lst(iLoop).noUp
                    cmbNumberUp.Text = noUp
                End If

                If TotalPieces <> lst(iLoop).plannedCopies Or LastQtyToGo <> lst(iLoop).qtytoGo Then
                    Dim QtyToGoOffset As Long = LastQtyToGo - lst(iLoop).qtytoGo
                    InitialQtyToGo -= QtyToGoOffset
                    LastQtyToGo -= QtyToGoOffset
                    TotalPieces = lst(iLoop).plannedCopies
                End If

                lblFreezeCount.Text = LastQtyToGo
                'WasteImpressions = lst(iLoop).wasteImpressions
                WastePieces = lst(iLoop).wasteCopies
                SUBJOBNO = lst(iLoop).subJobNo
                ORDERNO = lst(iLoop).orderNo
                clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
                clsDC360SM.SaveMemorySetting("DC360_PlannedQty", lst(iLoop).plannedCopies)
                clsDC360SM.SaveMemorySetting("DC360_JobSpeed", lst(iLoop).speed)
                clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
                clsDC360SM.SaveMemorySetting("DC360_Staff", lst(iLoop).staff)
                clsDC360SM.SaveMemorySetting("DC360_Process", lst(iLoop).process)
            End If

        End If
        tmrUpdateIntervalTimer.Enabled = True
        WriteApplicationLog("FORM1 tmrUpdateIntervalTimer_Elapsed - Exited")
    End Sub

    Private Sub tmrUpdateActualsTimer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles tmrUpdateActualsTimer.Elapsed
        WriteApplicationLog("FORM1 tmrUpdateActualsTimer_Elapsed - Entered")
        Try
            tmrUpdateActualsTimer.Enabled = False
            DoPostBackActuals(,, IIf(lblFreezeCount.Visible, lblFreezeCount.Text, lblCount.Text))
        Catch ex As Exception

        Finally
            tmrUpdateActualsTimer.Enabled = True
        End Try

        WriteApplicationLog("FORM1 tmrUpdateActualsTimer_Elapsed - Exited")
    End Sub
#End Region


    Private Sub frmMain_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        WriteApplicationLog("FORM1 frmMain_Closing - Entered")
        Me.Visible = False
        Dim tmpEventTime As String = CreateEventDateTime(Now())
        Dim cntHolder As String = lblCount.Text
        Dim assistHolder As String = cmbAssistants.Text
        Dim configHolder As String = cmbConfig.Text
        Dim configNdxHolder As String = cmbConfig.SelectedIndex
        Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
        DoPostBackCostEvent(tmpEventTime, cntHolder, "21",, opCodeNdxHolder,, assistHolder, configHolder, configNdxHolder)

        Dim HoldInfoOnClose As String = ""
        Try
            HoldInfoOnClose = Val(GetSetting("iVu", "Config", "HoldValuesOnClose"))
        Catch

        End Try

        If HoldInfoOnClose = "1" Then
            SaveSetting("iVu", "Config", "HoldValuesOnClose", "0")
        Else

            'Lift the job
            GLOBALSTARTENDOVERRIDE = tmpEventTime
            DoPostBackCostEvent(tmpEventTime, cntHolder, "2",, opCodeNdxHolder, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Lift Job
            DoPostBackCostEvent(tmpEventTime, cntHolder, "21", OpCodeNotInUseNdx, OpCodeNotInUseNdx, tmpEventTime, assistHolder, configHolder, configNdxHolder) 'Clear Job

            f.lblLargeJobName.Text = "No Selection"
            f.lblSmallJobName.Text = "No Selection"
            f.lblLargeClient.Text = "No Selection"
            f.lblSmallClient.Text = "No Selection"
            f.lblLargeJobNumber.Text = "No Selection"
            f.lblSmallJobNumber.Text = "No Selection"
            f.lblLargeVersion.Text = GetVersionFromSubJob("No Selection-No Selection")
            f.lblSmallVersion.Text = GetVersionFromSubJob("No Selection-No Selection")
            f.lblCount.Text = "0"
            f.lblFreezeCount.Text = "0"
            LastQtyToGo = "0"
            InitialQtyToGo = "0"
            TotalPieces = "0"
            WastePieces = "0"
            currenttaskID = Nothing
            currentClient = "No Selection"
            currentJobName = "No Selection"
            SUBJOBNO = "No Selection-No Selection"
            ORDERNO = ""
            noUpFromJob = "1"
            noUp = "1"
            cmbNumberUp.Text = "1"
            clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
            clsDC360SM.SaveMemorySetting("DC360_JobName", lblSmallJobName.Text)
            clsDC360SM.SaveMemorySetting("DC360_JobClient", lblSmallClient.Text)
            clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
            clsDC360SM.SaveMemorySetting("DC360_Staff", "0")
            clsDC360SM.SaveMemorySetting("DC360_Process", "")

            currentOpCodeNDX = OpCodeNotInUseNdx : opCodeNdxHolder = currentOpCodeNDX
            PushOpCodeList(False)
            clsDC360SM.SaveMemorySetting("DC360_OpCode", myOpCodeList(currentOpCodeNDX).opCodeName)
            EVENTSTARTDATE = tmpEventTime
            PriorDropDownNDX = cmbConfig.SelectedIndex : cmbConfig.SelectedIndex = 0 : configHolder = cmbConfig.Text : configNdxHolder = 0
            PriorDropDownNDX = cmbAssistants.SelectedIndex : cmbAssistants.SelectedIndex = 0 : assistHolder = 0
            clsDC360SM.SaveMemorySetting("DC360_JobNumber", lblLargeJobNumber.Text & "-" & lblLargeVersion.Text)
            clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Text)

            'Log Out the Operator
            If CURRENTOPERATORID <> "0" And CURRENTOPERATORID <> "" Then
                DoPostBackTimesheet("Logout", CURRENTOPERATORID, tmpEventTime)
                EVENTSTARTDATE = tmpEventTime
                lblUser.Text = "No Operator"
                clsDC360SM.SaveMemorySetting("DC360_UserName", lblUser.Text)
                CURRENTOPERATORID = ""
                CURRENTOPERATORNAME = ""
            End If
            DoPostBackCostEvent(tmpEventTime, "0", "21",, opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
        End If
        WriteApplicationLog("FORM1 frmMain_Closing - Exited")

    End Sub

    Private Sub frmMain_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        WriteApplicationLog("FORM1 frmMain_Closed - Entered")
        clsDC360SM.SaveMemorySetting("DC360_IsRunning", "0")
        WriteApplicationLog("FORM1 frmMain_Closed - Exited")
    End Sub

#Region "TESTING"

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        SaveSetting("iVu", "Config", "GrossCount", Val(GetSetting("iVu", "Config", "GrossCount")) + 1000)

        'If lblCount.Text > 0 Then
        '    lblCount.Text = lblCount.Text - 1000
        'End If
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        clsDC360SM.SaveMemorySetting("DC360_JobDialog", "1")
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        clsDC360SM.SaveMemorySetting("DC360_GoodToWasteDialog", "1")
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If cmbOpCode.SelectedIndex = cmbOpCode.Items.Count - 1 Then
            clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Items(0).ToString)
        Else
            clsDC360SM.SaveMemorySetting("DC360_OpCode", cmbOpCode.Items(cmbOpCode.SelectedIndex + 1).ToString)
        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        clsDC360SM.SaveMemorySetting("DC360_UserDialog", "1")
    End Sub
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        clsDC360SM.SaveMemorySetting("DC360_IsRunning", "0")

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        clsDC360SM.SaveMemorySetting("DC360_VitOpen", "1")
        SaveSetting("iVu", "Config", "GrossCount", "0")

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        clsDC360SM.SaveMemorySetting("DC360_OpCode", "Unknown Stop")
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        If cmbConfig.SelectedIndex = cmbConfig.Items.Count - 1 Then
            clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Items(0).ToString)
        Else
            clsDC360SM.SaveMemorySetting("DC360_JobConfig", cmbConfig.Items(cmbConfig.SelectedIndex + 1).ToString)
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        EMERGENCYJOB = True
    End Sub
    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        clsDC360SM.SaveMemorySetting("DC360_OpCode", "Running")
    End Sub
    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles btnWIP.Click
        Dim aWIP As New frmWIP()
        aWIP.ShowDialog()

    End Sub
    Private Sub Button12_Click_1(sender As Object, e As EventArgs) Handles Button12.Click
        If cmbNumberUp.SelectedIndex = cmbNumberUp.Items.Count - 1 Then
            clsDC360SM.SaveMemorySetting("DC360_JobNUp", cmbNumberUp.Items(0).ToString)
        Else
            clsDC360SM.SaveMemorySetting("DC360_JobNUp", cmbNumberUp.Items(cmbNumberUp.SelectedIndex + 1).ToString)
        End If
    End Sub
    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        clsDC360SM.SaveMemorySetting("DC360_OpCode", "Unkning")
    End Sub
#End Region


    Private Sub cmbOpCode_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cmbOpCode.KeyPress
        e.Handled = True 'Effectively cancels the keystroke
    End Sub

    Private Sub cmbNumberUp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbNumberUp.SelectedIndexChanged
        If IAMLOADING Then Exit Sub
        If noUp = cmbNumberUp.Text Then
            clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp) 'Ensure the SharedMemory variable is correct
            Exit Sub 'no change actually happened.
        End If

        Dim adminCode As String = GetAdminLogin("change the number up")

        If adminCode = "" Then 'Cancel the NoUp Change
            cmbNumberUp.Text = noUp
            clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
            Exit Sub
        End If

        'With the multiple windows all requesting top most the message box below
        'can sometimes get lost (visibly).  This causes some individuals to believe
        'the application is frozen and must be closed.
        'To address this, we are commenting out the confirmation.
        'To get here, you need to change the number up and enter an admin password.  That should be enough of a confirmation.
        'If MessageBox.Show("Are you sure you'd like to change the number up change from " & noUp & " to " & cmbNumberUp.Text & "?", "Confirm Number Up Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.No Then
        '    cmbNumberUp.Text = noUp
        '    clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
        '    Exit Sub
        'End If

        'Post Counts using prior number up 
        Dim tmpEventTime As String = CreateEventDateTime(Now())
        Dim cntHolder As String = lblCount.Text
        Dim assistHolder As String = cmbAssistants.Text
        Dim configHolder As String = cmbConfig.Text
        Dim configNdxHolder As String = cmbConfig.SelectedIndex
        Dim opCodeNdxHolder As String = IIf(cmbOpCode.Text = "", currentOpCodeNDX, GetOpCodeNdx(cmbOpCode.Text))
        DoPostBackCostEvent(tmpEventTime, cntHolder, "21", , opCodeNdxHolder, , assistHolder, configHolder, configNdxHolder)
        EVENTSTARTDATE = tmpEventTime

        'Then update the number up
        noUp = cmbNumberUp.Text
        clsDC360SM.SaveMemorySetting("DC360_JobNUp", noUp)
        If noUp = noUpFromJob Then
            lblNoUp.BackColor = (New Label).BackColor
        Else
            lblNoUp.BackColor = Color.Yellow
        End If

        SaveSetting("DCL360_Machine_Counts", "Jobs\" & lblLargeJobNumber.Text & "-" & lblLargeVersion.Text, "NumberUp", noUp & "-" & adminCode)


    End Sub

End Class
