﻿Imports System.IO
Imports System.IO.MemoryMappedFiles
Imports System.Threading
Public Class DC360_SharedMemory_Cls
    Private _sttSMServer As New System.Threading.Thread(AddressOf _Thread)
    Private _bRequestCloseThread As Boolean = False
    Private _bThreadRunning As Boolean = False
    Private _Latency As Long = 0
    Private _MaxLatency As Long = 0
    Private _ThreadCycles As Long = 0
    Public Structure structSharedMemory
        Public strMemoryName As String
        Public strMemoryValue As String
        Public strMemoryDefaultValue As String
        Public strPreviousValue As String
        Public iMaxTextLength As String
        Public mmf As MemoryMappedFile
    End Structure
    Private _arrSharedMemory(0) As structSharedMemory
    Public Event ThreadStarted()
    Public Event ThreadStopped()
    Public Event ErrorRaised(ByVal strErrDescription As String)
    Public Event SharedMemoryChange(arrSharedMemory() As structSharedMemory)
    Public Function SaveMemorySetting(ByVal strKeyName As String, ByVal strSetting As String, Optional bOverWriteExistingMemory As Boolean = True) As Boolean
        Dim iarrLoop As Integer = 0
        Dim bResult As Boolean = False
        Dim strX As String = ""
        For iarrLoop = 1 To UBound(_arrSharedMemory)
            If _arrSharedMemory(iarrLoop).strMemoryName = strKeyName Then
                bResult = _MakeMemoryMappedFile(iarrLoop, strKeyName, strSetting, True)
                Exit For
            End If
        Next
        Return bResult
    End Function
    Public Function GetMemorySetting(ByVal strKeyName As String, ByVal strDefault As String, Optional ByVal lMaxLenght As Long = 255) As String
        Dim strSetting As String = ""
        Dim iPos As Integer = 0
        strSetting = _ReadMemoryMappedFile(strKeyName, strDefault, lMaxLenght)
        iPos = InStr(strSetting, Chr(0))
        If iPos > 0 Then
            strSetting = Left(strSetting, iPos - 1)
        Else End If
        GetMemorySetting = strSetting
    End Function
    Private Function _ReadMemoryMappedFile(ByVal strMemoryName As String, ByVal strDefault As String, Optional ByVal lMaxLenght As Long = 255) As String
        _ReadMemoryMappedFile = Nothing
        Try
            Using file = MemoryMappedFile.OpenExisting(strMemoryName)
                Using reader = file.CreateViewAccessor(0, lMaxLenght)
                    Dim bytes = New Byte(lMaxLenght - 1) {}
                    reader.ReadArray(Of Byte)(0, bytes, 0, bytes.Length)
                    Dim sValue As String = System.Text.ASCIIEncoding.ASCII.GetString(bytes)
                    _ReadMemoryMappedFile = sValue
                    If sValue = "" Then _ReadMemoryMappedFile = strDefault
                End Using
            End Using
        Catch noFile As FileNotFoundException
            _ReadMemoryMappedFile = strDefault
        Catch Ex As Exception
        End Try
    End Function
    Private Function _MakeMemoryMappedFile(arrID As Integer, Optional strMemoryName As String = "", Optional strMemoryValue As String = "", Optional bOverWrite As Boolean = False) As Boolean
        Dim strVal As String = ""
        Dim strX As String = "{|NO_MEM|}"
        Try
            If strMemoryName <> "" Then
                strVal = strMemoryValue
            Else
                strVal = _arrSharedMemory(arrID).strMemoryValue
            End If
            If bOverWrite = False Then
                strX = GetMemorySetting(_arrSharedMemory(arrID).strMemoryName, "{|NO_MEM|}", _arrSharedMemory(arrID).iMaxTextLength)
            End If
            If strX = "{|NO_MEM|}" Then
                Dim iCamLoop As Integer = 0
                Dim array() As Byte = System.Text.Encoding.ASCII.GetBytes(strVal)
                If array.Length > _arrSharedMemory(arrID).iMaxTextLength Then _arrSharedMemory(arrID).iMaxTextLength = array.Length
                Dim bytes = New Byte(_arrSharedMemory(arrID).iMaxTextLength - 1) {}

                For i As Integer = 0 To array.Length - 1
                    bytes(i) = array(i)
                Next
                _arrSharedMemory(arrID).mmf = MemoryMappedFile.CreateOrOpen(_arrSharedMemory(arrID).strMemoryName, _arrSharedMemory(arrID).iMaxTextLength)
                Using writer = _arrSharedMemory(arrID).mmf.CreateViewAccessor(0, bytes.Length)
                    writer.WriteArray(Of Byte)(0, bytes, 0, bytes.Length)
                End Using
            Else End If
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function AddSharedMemoryVariable(strMemoryName As String, Optional strDefaultMemoryValue As String = "", Optional iMaxTextLength As Integer = 255) As Boolean
        Dim iarrLoop As Integer = 0
        Dim bResult As Boolean = False
        Try
            For iarrLoop = 1 To UBound(_arrSharedMemory)
                If _arrSharedMemory(iarrLoop).strMemoryName = strMemoryName Then 'Already Exists
                    Return False
                End If
            Next
            ReDim Preserve _arrSharedMemory(UBound(_arrSharedMemory) + 1)
            _arrSharedMemory(UBound(_arrSharedMemory)).strMemoryName = strMemoryName
            _arrSharedMemory(UBound(_arrSharedMemory)).strPreviousValue = vbNullChar ' strDefaultMemoryValue
            _arrSharedMemory(UBound(_arrSharedMemory)).strMemoryValue = "" 'strDefaultMemoryValue
            _arrSharedMemory(UBound(_arrSharedMemory)).strMemoryDefaultValue = strDefaultMemoryValue
            _arrSharedMemory(UBound(_arrSharedMemory)).iMaxTextLength = iMaxTextLength
            bResult = _MakeMemoryMappedFile(UBound(_arrSharedMemory), strMemoryName, strDefaultMemoryValue)
            Return bResult
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Property Thread_CycleCount() As String
        Get
            Return _ThreadCycles
        End Get
        Private Set(ByVal value As String)
            _ThreadCycles = value
        End Set
    End Property
    Public Property Thread_MaxLatency() As String
        Get
            Return _MaxLatency
        End Get
        Private Set(ByVal value As String)
            _MaxLatency = value
        End Set
    End Property
    Public Property Thread_CurrentLatency() As String
        Get
            Return _Latency
        End Get
        Private Set(ByVal value As String)
            _Latency = value
        End Set
    End Property
    Public ReadOnly Property IsRunning() As String
        Get
            Return _bThreadRunning
        End Get
    End Property
    Public Sub Server_Start(frmParent)
        Try
            Server_Stop()
            frmParent.CheckForIllegalCrossThreadCalls = False '<- Only Needed While In Debug Mode
            _bRequestCloseThread = False
            _sttSMServer = New System.Threading.Thread(AddressOf _Thread) 'Create A New Thread Object
            _sttSMServer.IsBackground = False

            _sttSMServer.Priority = Threading.ThreadPriority.Lowest
            _sttSMServer.SetApartmentState(Threading.ApartmentState.STA) 'Create Single Threaded Apartment Thread
            _sttSMServer.Start()
        Catch ex As Exception
            RaiseEvent ErrorRaised(ex.Message)
        End Try
    End Sub
    Private Sub _Thread()
        Dim _strNewThreadTime As String = ""
        Dim iarrLoop As Integer = 0
        Dim strMemValue As String = ""
        Dim arrNewValues(0) As structSharedMemory
        Dim swtchLatency As New Stopwatch
        _ThreadCycles = 0
        _MaxLatency = 0
        _Latency = 0
        _bRequestCloseThread = False
        _bThreadRunning = True
        RaiseEvent ThreadStarted()
        Do Until _bRequestCloseThread = True
            swtchLatency.Reset()
            ReDim arrNewValues(0)
            Try
                For iarrLoop = 1 To UBound(_arrSharedMemory)
                    If _bRequestCloseThread = True Then Exit Do
                    strMemValue = GetMemorySetting(_arrSharedMemory(iarrLoop).strMemoryName, _arrSharedMemory(iarrLoop).strMemoryDefaultValue, _arrSharedMemory(iarrLoop).iMaxTextLength)
                    If strMemValue <> _arrSharedMemory(iarrLoop).strMemoryValue Then
                        ReDim Preserve arrNewValues(UBound(arrNewValues) + 1)
                        arrNewValues(UBound(arrNewValues)).strPreviousValue = _arrSharedMemory(iarrLoop).strMemoryValue
                        arrNewValues(UBound(arrNewValues)).strMemoryName = _arrSharedMemory(iarrLoop).strMemoryName
                        arrNewValues(UBound(arrNewValues)).strMemoryValue = strMemValue
                        arrNewValues(UBound(arrNewValues)).strMemoryDefaultValue = _arrSharedMemory(iarrLoop).strMemoryDefaultValue
                        _arrSharedMemory(iarrLoop).strPreviousValue = _arrSharedMemory(iarrLoop).strMemoryValue
                        _arrSharedMemory(iarrLoop).strMemoryValue = strMemValue
                    End If
                Next
                If UBound(arrNewValues) > 0 Then
                    RaiseEvent SharedMemoryChange(arrNewValues)
                End If
            Catch ex As Exception
                If _bRequestCloseThread = False Then
                    RaiseEvent ErrorRaised(ex.Message)
                End If
            End Try
            _Latency = swtchLatency.ElapsedMilliseconds
            If _Latency > _MaxLatency Then
                _MaxLatency = swtchLatency.ElapsedMilliseconds
            End If
            _ThreadCycles += 1
        Loop
        _bThreadRunning = False
    End Sub
    Public Sub Server_Stop()
        If _sttSMServer.ThreadState = Threading.ThreadState.Running Then
            _bRequestCloseThread = True
            _sttSMServer.Abort()
            _bThreadRunning = False
            If _sttSMServer.ThreadState = Threading.ThreadState.Running Then
                RaiseEvent ErrorRaised("Error Closing Thread")
            End If
            RaiseEvent ThreadStopped()
        End If
    End Sub
End Class
