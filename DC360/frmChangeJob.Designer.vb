﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmChangeJob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.btnLift = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.lblJobNumber = New System.Windows.Forms.Label()
        Me.pnlJobGrid = New System.Windows.Forms.Panel()
        Me.chkShowCompleted = New System.Windows.Forms.CheckBox()
        Me.cmbResource = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnStartJob = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VScrollBar1 = New System.Windows.Forms.VScrollBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtJobSearch = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnStop
        '
        Me.btnStop.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStop.Location = New System.Drawing.Point(435, 335)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(137, 65)
        Me.btnStop.TabIndex = 4
        Me.btnStop.Text = "End"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'btnLift
        '
        Me.btnLift.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLift.Location = New System.Drawing.Point(578, 335)
        Me.btnLift.Name = "btnLift"
        Me.btnLift.Size = New System.Drawing.Size(137, 65)
        Me.btnLift.TabIndex = 5
        Me.btnLift.Text = "Pause"
        Me.btnLift.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(721, 335)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(137, 65)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(453, 72)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(53, 25)
        Me.lblTitle.TabIndex = 42
        Me.lblTitle.Text = "Title"
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(194, 72)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(103, 25)
        Me.lblCompany.TabIndex = 41
        Me.lblCompany.Text = "Company"
        '
        'lblJobNumber
        '
        Me.lblJobNumber.AutoSize = True
        Me.lblJobNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobNumber.Location = New System.Drawing.Point(12, 72)
        Me.lblJobNumber.Name = "lblJobNumber"
        Me.lblJobNumber.Size = New System.Drawing.Size(128, 25)
        Me.lblJobNumber.TabIndex = 40
        Me.lblJobNumber.Text = "Job Number"
        '
        'pnlJobGrid
        '
        Me.pnlJobGrid.AutoScroll = True
        Me.pnlJobGrid.Location = New System.Drawing.Point(12, 100)
        Me.pnlJobGrid.Name = "pnlJobGrid"
        Me.pnlJobGrid.Size = New System.Drawing.Size(846, 229)
        Me.pnlJobGrid.TabIndex = 43
        '
        'chkShowCompleted
        '
        Me.chkShowCompleted.AutoSize = True
        Me.chkShowCompleted.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowCompleted.Location = New System.Drawing.Point(12, 335)
        Me.chkShowCompleted.Name = "chkShowCompleted"
        Me.chkShowCompleted.Size = New System.Drawing.Size(193, 29)
        Me.chkShowCompleted.TabIndex = 44
        Me.chkShowCompleted.Text = "Show Completed"
        Me.chkShowCompleted.UseVisualStyleBackColor = True
        '
        'cmbResource
        '
        Me.cmbResource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbResource.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbResource.FormattingEnabled = True
        Me.cmbResource.Location = New System.Drawing.Point(123, 364)
        Me.cmbResource.Name = "cmbResource"
        Me.cmbResource.Size = New System.Drawing.Size(306, 33)
        Me.cmbResource.Sorted = True
        Me.cmbResource.TabIndex = 45
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 367)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 25)
        Me.Label3.TabIndex = 46
        Me.Label3.Text = "Resource:"
        '
        'btnStartJob
        '
        Me.btnStartJob.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStartJob.Location = New System.Drawing.Point(506, 335)
        Me.btnStartJob.Name = "btnStartJob"
        Me.btnStartJob.Size = New System.Drawing.Size(137, 65)
        Me.btnStartJob.TabIndex = 47
        Me.btnStartJob.Text = "Start Job"
        Me.btnStartJob.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 25)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Current Job:"
        '
        'VScrollBar1
        '
        Me.VScrollBar1.Location = New System.Drawing.Point(812, 101)
        Me.VScrollBar1.Name = "VScrollBar1"
        Me.VScrollBar1.Size = New System.Drawing.Size(48, 228)
        Me.VScrollBar1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(461, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 25)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "Job Search:"
        '
        'txtJobSearch
        '
        Me.txtJobSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobSearch.Location = New System.Drawing.Point(594, 6)
        Me.txtJobSearch.Name = "txtJobSearch"
        Me.txtJobSearch.Size = New System.Drawing.Size(266, 31)
        Me.txtJobSearch.TabIndex = 50
        '
        'frmChangeJob
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 405)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtJobSearch)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.VScrollBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnStartJob)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmbResource)
        Me.Controls.Add(Me.chkShowCompleted)
        Me.Controls.Add(Me.pnlJobGrid)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.lblJobNumber)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnLift)
        Me.Controls.Add(Me.btnStop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmChangeJob"
        Me.Text = "Select New Job"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnStop As Button
    Friend WithEvents btnLift As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents lblTitle As Label
    Friend WithEvents lblCompany As Label
    Friend WithEvents lblJobNumber As Label
    Friend WithEvents pnlJobGrid As Panel
    Friend WithEvents chkShowCompleted As CheckBox
    Friend WithEvents cmbResource As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents btnStartJob As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents VScrollBar1 As VScrollBar
    Friend WithEvents Label2 As Label
    Friend WithEvents txtJobSearch As TextBox
End Class
