﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmergJob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtJobNumber = New System.Windows.Forms.TextBox()
        Me.btnCreateJob = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.txtJobName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtClientName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtQtyToGo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNumberUp = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(52, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Job Number:"
        '
        'txtJobNumber
        '
        Me.txtJobNumber.Location = New System.Drawing.Point(125, 12)
        Me.txtJobNumber.Name = "txtJobNumber"
        Me.txtJobNumber.Size = New System.Drawing.Size(100, 20)
        Me.txtJobNumber.TabIndex = 0
        '
        'btnCreateJob
        '
        Me.btnCreateJob.Location = New System.Drawing.Point(326, 114)
        Me.btnCreateJob.Name = "btnCreateJob"
        Me.btnCreateJob.Size = New System.Drawing.Size(70, 23)
        Me.btnCreateJob.TabIndex = 5
        Me.btnCreateJob.Text = "Create Job"
        Me.btnCreateJob.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(231, 114)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(53, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.TabStop = False
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'txtJobName
        '
        Me.txtJobName.Location = New System.Drawing.Point(125, 38)
        Me.txtJobName.Name = "txtJobName"
        Me.txtJobName.Size = New System.Drawing.Size(271, 20)
        Me.txtJobName.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(61, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Job Name:"
        '
        'txtClientName
        '
        Me.txtClientName.Location = New System.Drawing.Point(125, 64)
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Size = New System.Drawing.Size(271, 20)
        Me.txtClientName.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(65, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Company:"
        '
        'txtQtyToGo
        '
        Me.txtQtyToGo.Location = New System.Drawing.Point(125, 90)
        Me.txtQtyToGo.Name = "txtQtyToGo"
        Me.txtQtyToGo.Size = New System.Drawing.Size(100, 20)
        Me.txtQtyToGo.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(35, 93)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Planned Copies:"
        '
        'txtNumberUp
        '
        Me.txtNumberUp.Location = New System.Drawing.Point(125, 116)
        Me.txtNumberUp.Name = "txtNumberUp"
        Me.txtNumberUp.Size = New System.Drawing.Size(100, 20)
        Me.txtNumberUp.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(55, 119)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Number Up:"
        '
        'frmEmergJob
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(408, 148)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtNumberUp)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtQtyToGo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtClientName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtJobName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnCreateJob)
        Me.Controls.Add(Me.txtJobNumber)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmEmergJob"
        Me.Text = "Create Emergency Job"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtJobNumber As TextBox
    Friend WithEvents btnCreateJob As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents txtJobName As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtClientName As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtQtyToGo As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtNumberUp As TextBox
    Friend WithEvents Label5 As Label
End Class
