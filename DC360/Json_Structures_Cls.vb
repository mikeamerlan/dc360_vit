﻿Imports System.Web.Script.Serialization
Module Json_Structures_Cls
    Public strHost As String = "http://dev.dc360.sg360.com"
    Public strVersion As String = "v1"
    Public strCorrelationID As String = "1"
    Public iHttpTimeout As Integer = 30000
    Public strWebResponseErrorMessage As String = ""
    Public bWebResponseSuccess As Boolean = False
    Public strLastGetorPostRawJSonData As String = ""
    Public strDC360RootPath As String = "C:\_Vit\_DC360"
    Public strDC360DirectoryCreationErrorMessage As String = ""
    Public strDC360LogFileName As String = "\DC360TransactionLog.tab"
    Public strDC360FaildsPOSTSFileName As String = "\DC360FailedPOSTS.tab"
    Public strDC360ApplicationLogFileName As String = "\DC360ApplicationLog.tab"
    Public strDC360RePostLogFileName As String = "\DC360RePostLog.tab"
    Public strLogFileHeader As String = "DATE_TIME" & vbTab & "CALL_TYPE" & vbTab & "SUCCESS_FAIL" & vbTab & "LATENCY" & vbTab & "HTTP_ROOT" & vbTab & "HTTP_PATH" & vbTab & "CORE_ID" & vbTab & "VERSION" & vbTab & "RESPONSE_MESSAGE" & vbTab & "JSON_DATA"
    Public strHeaderWidths As String = "126" & vbTab & "76" & vbTab & "96" & vbTab & "65" & vbTab & "161" & vbTab & "267" & vbTab & "61" & vbTab & "63" & vbTab & "336" & vbTab & "AUTO"

    Public USENEWPATH As Boolean = True 'http://uat.dc360.sg360.com/DC360/DCUsers/admins/ vs http://uat.dc360.sg360.com/DCUsers/v3/operators/123/

    '*START *** /resources ****************************************************************************************************************************
    Public Class Inheritable_Resources 'cls_similarResources AND cls_machineConfig
        Public resourceID As Int32
        Public resourceName As String
        Public locID As Nullable(Of Int32)
        Public locName As String
        Public deptCode As String
        Public deptName As String
        Public deptType As Int32
        Public facilityID As Int32
        Public facilityName As String
        Public pcFriendlyName As String
        Public pcPrimaryMacAddress As String
        Public pcSecondaryMacAddress As String
    End Class
    '   *START ************************************ configs, configs, configs, configs, configs, configs, configs, ************************************
    Public Class cls_configs
        Public configId As Int32            '14
        Public configCode As String         '"2501: Press25 Base"
        Public configDescription As String  '"2501 - Press25 - Base"
    End Class
    Public Function Get_configs(resourceId As String) As List(Of cls_configs)
        Dim strResult As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCResources/" & strV & "resources/" & resourceId & "/configs/"        'http://sg360-apps-01:8083/DCResources/v1/resources/120/configs/

        If USENEWPATH Then
            strPath = "/DC360/DCResources/resources/" & resourceId & "/configs/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_configs) = js.Deserialize(strResult, GetType(List(Of cls_configs)))
        Get_configs = lstResult
    End Function
    '   *END *************************************** configs, configs, configs, configs, configs, configs, configs, ************************************
    '   *START ************************************* opCodes, opCodes, opCodes, opCodes, opCodes, opCodes, opCodes,  ************************************
    Public Class cls_opCodes
        Public opCodeID As Int32              '3615
        Public opUnitID As Int32              '1675
        Public opAreaID As Int32              '1210
        Public opCodeTypeColorRGB As String     '"RGB(128,255,255)"
        Public opCodeName As String             '"No Work"
        Public opCodeTypeName As String         '"Idle"
        Public opAreaName As String             '"Idle"
        Public specialCode As Int32            'Added 4/23/2018 for /v2 release
        Public specialDesc As String            'Added 4/23/2018 for /v2 release
    End Class
    Public Function Get_opCodes(resourceId As String) As List(Of cls_opCodes)
        Dim strResult As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCResources/" & strV & "resources/" & resourceId & "/opCodes/"        'http://sg360-apps-01:8083/DCResources/v1/resources/120/opCodes/

        If USENEWPATH Then
            strPath = "/DC360/DCResources/resources/" & resourceId & "/opCodes/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_opCodes) = js.Deserialize(strResult, GetType(List(Of cls_opCodes)))
        Get_opCodes = lstResult
    End Function
    '   *END ************************************ opCodes, opCodes, opCodes, opCodes, opCodes, opCodes, opCodes,  ************************************
    '   *START ********************************** settings, settings, settings, settings, settings, settings, ************************************
    Public Class cls_settings
        Public resourceID As Int32 '120
        Public settingID As Int32 '1
        Public settingName As String '"Count prompt frequency"
        Public settingValue As String '"60"
    End Class
    Public Function Get_settings(resourceId As String) As List(Of cls_settings)
        Dim strResult As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCResources/" & strV & "resources/" & resourceId & "/settings/"       'http://sg360-apps-01:8083/DCResources/v1/resources/120/settings/

        If USENEWPATH Then
            strPath = "/DC360/DCResources/resources/" & resourceId & "/settings/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_settings) = js.Deserialize(strResult, GetType(List(Of cls_settings)))
        Get_settings = lstResult
    End Function
    '   *END ************************************ settings, settings, settings, settings, settings, settings, ************************************
    '   *START ********************************** similarResources, similarResources, similarResources, ************************************
    Public Class cls_similarResources
        Inherits Inheritable_Resources
    End Class
    Public Function Get_similarResources(resourceId As String) As List(Of cls_similarResources)
        Dim strResult As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCResources/" & strV & "resources/" & resourceId & "/similarResources/"        'http://sg360-apps-01:8083/DCResources/v1/resources/120/similarResources/

        If USENEWPATH Then
            strPath = "/DC360/DCResources/resources/" & resourceId & "/similarResources/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_similarResources) = js.Deserialize(strResult, GetType(List(Of cls_similarResources)))
        Get_similarResources = lstResult
    End Function
    '   *END ************************************ similarResources, similarResources, similarResources, ************************************
    '*END *** /resources ****************************************************************************************************************************

    '*START *** /machineConfig ****************************************************************************************************************************
    Public Class cls_machineConfig
        Inherits Inheritable_Resources
    End Class
    Public Function Get_machineConfig(ipAddress As String) As List(Of cls_machineConfig)
        Dim strResult As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCResources/" & strV & "machineConfig/" & ipAddress & "/"  'http://sg360-apps-01:8083//DCResources/v1/machineConfig/10.60.3.187

        If USENEWPATH Then
            strPath = "/DC360/DCResources/machineConfig/" & ipAddress & "/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_machineConfig) = js.Deserialize(strResult, GetType(List(Of cls_machineConfig)))
        Get_machineConfig = lstResult
    End Function
    '*END ***** /machineConfig ****************************************************************************************************************************

    '*START *** /DCUsers ****************************************************************************************************************************
    Public Class cls_operators
        Public id As String         '"991916"
        Public firstName As String  '"Catalina"
        Public lastName As String   '"Penaloza"
    End Class
    Public Function Get_operators(OperatorID As String) As List(Of cls_operators)
        Dim strResult As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCUsers/" & strV & "operators/" & OperatorID & "/"        'http://sg360-apps-01:8083/DCUsers/v1/operators/991916/

        If USENEWPATH Then
            strPath = "/DC360/DCUsers/operators/" & OperatorID & "/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_operators) = js.Deserialize(strResult, GetType(List(Of cls_operators)))
        Get_operators = lstResult
    End Function
    Public Class cls_admins
        Public id As Int32          '36
        Public userName As String   '"Mark Anderson"
        Public pin As Int32         '12345678"
    End Class
    Public Function Get_admins() As List(Of cls_admins)
        Dim strResult As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCUsers/" & strV & "admins/"      'http://sg360-apps-01:8083/DCUsers/v1/admins/

        If USENEWPATH Then
            strPath = "/DC360/DCUsers/admins/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_admins) = js.Deserialize(strResult, GetType(List(Of cls_admins)))
        Get_admins = lstResult
    End Function
    '*END *** /DCUsers ****************************************************************************************************************************

    '*START *** /DCTasks ****************************************************************************************************************************
    Public Class Inheritable_task_resource
        Public taskID As Int32                          '424421
        Public resourceID As Int32                      '1
        Public resourceName As String                   '"Press25"
        Public startDateTime As String                  '"2017-08-07T07:00:00-06:00"
        Public endDateTime As String                    '"2017-08-09T15:00:00-06:00"
        Public subJobNo As String                       '"92672-001A"
        Public taskName As String                       '"Wal-Mart Mastercard Reissue Dual EMV Benefits Brochure"
        Public company As String                        '"string"
        Public orderNo As Int32                         '92672
        Public plannedCopies As Int32                   '3683000
        Public noUp As Int32                            '10
        Public makeReadyMins As Int32                   '1200
        Public runMins As Int32                         '2160
        Public noPasses As Int32                        '1
        Public speed As Int32                           '15675
        Public netImpressions As Nullable(Of Int32)     '*73000
        Public grossImpressions As Nullable(Of Int32)   '*184329
        Public wasteImpressions As Nullable(Of Int32)   '*111329
        Public netCopies As Nullable(Of Int32)          '*730000
        Public grossCopies As Nullable(Of Int32)        '*1113290
        Public wasteCopies As Nullable(Of Int32)        '*0
        Public qtytoGo As Int32                         '2953000
        Public taskStatus As String                     '"LIVE"
        Public totalSubJobQty As Int32                  '20000000
        Public configId As Int32                        '*23
        Public lastUpdate As String                    '"2017-08-09T15:00:00"
        Public staff As Int32
        Public process As String
    End Class
    Public Class cls_task_resource
        Inherits Inheritable_task_resource
    End Class
    Public Function Get_task_resource(resourceId As String, Optional includeCompleted As Nullable(Of Boolean) = Nothing, Optional searchString As String = "") As List(Of cls_task_resource)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCTasks/" & strV & "tasks/resource/" & resourceId & "/"
        'http://sg360-apps-01:8083/DCTasks/v1/tasks/resource/120/?includeCompleted=true&searchString=Discover

        If USENEWPATH Then
            strPath = "/DC360/DCTasks/tasks/resource/" & resourceId & "/"
        End If

        If includeCompleted = True Then
            strQry += "includeCompleted=true"
        ElseIf includeCompleted = False Then
            strQry += "includeCompleted=false"
        Else
            'No includeCompleted Filter
        End If
        If searchString <> "" Then
            If strQry <> "" Then strQry += "&"
            strQry += "searchString=" & FormatSearchString(searchString)
        End If
        If strQry <> "" Then strPath += "?" & strQry
        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_task_resource) = js.Deserialize(strResult, GetType(List(Of cls_task_resource)))
        Get_task_resource = lstResult
    End Function
    Public Class cls_tasks
        Inherits Inheritable_task_resource
    End Class
    Public Function Get_tasks(taskid As String) As List(Of cls_tasks)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCTasks/" & strV & "tasks/" & taskid & "/"
        'http://sg360-apps-01:8083/DCTasks/v1/tasks/288995

        If USENEWPATH Then
            strPath = "/DC360/DCTasks/tasks/" & taskid & "/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_tasks) = js.Deserialize(strResult, GetType(List(Of cls_tasks)))
        Get_tasks = lstResult
    End Function
    '*END *** /DCTasks ****************************************************************************************************************************

    '*START *** /DCSfdc ****************************************************************************************************************************
    Public Class cls_getCurrentEvent
        Public resourceId As Int32                  '": 120,
        Public taskId As Nullable(Of Int32)         '": null,
        Public opCodeId As Int32                    '": 144,
        Public opUnitId As Int32                    '": 1,
        Public opAreaId As Nullable(Of Int32)       '": null,
        Public configId As Nullable(Of Int32)       '": null,
        Public operatorId As Nullable(Of Int32)     '": null,
        Public assistants As Nullable(Of Int32)     '": 0,
        Public eventStartDateTime As String         '": "2018-01-12T06:00:00"
    End Class
    Public Function Get_getCurrentEvent(resourceId As String) As List(Of cls_getCurrentEvent)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSfdc/" & strV & "getCurrentEvent/" & resourceId & "/"
        'http://sg360-apps-01:8083/DCSfdc/v1/getCurrentEvent/120/

        If USENEWPATH Then
            strPath = "/DC360/DCSfdc/getCurrentEvent/" & resourceId & "/"
        End If


        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getCurrentEvent) = js.Deserialize(strResult, GetType(List(Of cls_getCurrentEvent)))
        Get_getCurrentEvent = lstResult
    End Function
    Public Class cls_getCurrentShift
        Public resourceId As Int32  '120
        Public start As String      '"2018-04-18T06:00:00"
        Public End_ As String       '"2018-04-18T14:00:00"
        Public shiftCode As String  ' "A"
        Public shiftDate As String  ' "2018-04-18T00:00:00"
    End Class
    Public Function Get_getCurrentShift(resourceId As String) As List(Of cls_getCurrentShift)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSfdc/" & strV & "getCurrentShift/" & resourceId & "/"


        If USENEWPATH Then
            strPath = "/DC360/DCSfdc/getCurrentShift/" & resourceId & "/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))

        strResult = Replace(strResult, Chr(34) & "end" & Chr(34), Chr(34) & "end_" & Chr(34))


        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Try
            Dim lstResult As List(Of cls_getCurrentShift) = js.Deserialize(strResult, GetType(List(Of cls_getCurrentShift)))
            Get_getCurrentShift = lstResult
        Catch ex As Exception
            bWebResponseSuccess = False
            Get_getCurrentShift = New List(Of Json_Structures_Cls.cls_getCurrentShift) 'Return an empty list
        End Try

    End Function
    Public Structure struct_POST_sfdcEvents
        Public resourceID As Int32                  '* 120
        Public noAssistants As Int32                '* 1
        Public grossImpressions As Int32            '* 20
        Public netImpressions As Int32              '* 10
        Public grossCopies As Int32                 '* 10
        Public netCopies As Int32                   '* 5
        Public eventType As Int32                   '* 0
        Public eventStartDateTime As String         '* "2017-06-22T09:14:58"
        Public eventEndDateTime As String           '* "2017-06-22T11:25:38"
        Public operatorId As String                 '* "1590"
        Public operatorName As String               '* "OSTROWSKI, VINCENT R"
        Public noStreams As Int32                   '* 1
        Public timeSheetNo As Nullable(Of Int32)    '  394971
        Public taskID As Int32                      '* 0
        Public subJobNo As String                   '* "string"
        Public OpCodeID As Int32                    '*11
        Public emergencyJob As Boolean              '*
        Public OpUnitID As Int32                    '*
        Public OpAreaID As Int32                    '* 449
        Public configId As Int32                    '* 0
        Public configName As String                 '* "string"
        Public runNo As Int32                       '* 1
        Public shiftNote As String                  '  "string"
        'New to V2
        Public noAssistantsNew As Int32
        Public configIdNew As Int32
        Public configNameNew As String
        Public OpCodeIDNew As Int32
        Public OpUnitIDNew As Int32
        Public OpAreaIDNew As Int32

    End Structure
    Public Function post_sfdcEvents(sfdcEvent As struct_POST_sfdcEvents) As Boolean 'Success = True
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSfdc/" & strV & "sfdcEvents/" 'http://sg360-apps-01:8083/DCSfdc/v1/sfdcEvents/       

        If USENEWPATH Then
            strPath = "/DC360/DCSfdc/sfdcEvents/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(sfdcEvent)
        post_sfdcEvents = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID,,, True)
    End Function
    Public Structure struct_POST_timesheetEvents
        Public id As Int32                 '"0"
        Public resourceId As Int32          '120                        'Swagger Defines as string
        Public eventType As String          '"Login"                    Login or Logout
        Public operatorId As Int32          '991916                     'Swagger Defines as string
        Public eventDateTime As String      '"2018-04-18T16:00:00"
    End Structure
    Public Function post_timesheetEvents(sfdcEvent As struct_POST_timesheetEvents) As Boolean 'Success = True
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCTimesheets/" & strV & "timesheetEvents/" 'http://sg360-apps-01:8083/DCTimesheets/v1/timesheetEvents/

        If USENEWPATH Then
            strPath = "/DC360/DCTimesheets/timesheetEvents/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(sfdcEvent)
        post_timesheetEvents = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID,,, True)
    End Function
    Public Structure struct_POST_updateActuals
        Public id As Int32
        Public taskId As Int32
        Public avgCopySpeed As Int32
        Public lastGCSpeed As Int32
        Public copiesPerCycle As Int32
        Public inProgress As Boolean
        Public taskComplete As Boolean
        Public grossNoUp As Int32
        Public grossCollect As Int32
        Public netNoUp As Int32
        Public netCollect As Int32
        Public configName As String
        Public quantityToGo As Int32
        Public OpCodeID As Int32
        Public MRWaste As Int32
        Public RunWaste As Int32

    End Structure
    Public Function post_updateActuals(sfdcEvent As struct_POST_updateActuals) As Boolean 'Success = True
        Dim strV As String = "v2"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSfdc/" & strV & "updateActuals/" 'http://sg360-apps-01:8083/DCTimesheets/v1/timesheetEvents/

        If USENEWPATH Then
            strPath = "/DC360/DCSfdc/updateActuals/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(sfdcEvent)
        post_updateActuals = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID)
    End Function


    Public Structure struct_POST_WIPConsume
        Public system As String
        Public barcode As String
        Public dateTime As String
        Public operatorId As Int32
        Public jobNo As String
        Public pieces As Int32
        Public noUp As Int32
        Public resourceId As Int32
        Public method As String
    End Structure
    Public Function post_WIPConsume(sfdcEvent As struct_POST_WIPConsume) As Boolean 'Success = True
        Dim strV As String = "v1"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCWip/" & strV & "wip/consumePallet/" 'http://sg360-apps-01:8083/DCTimesheets/v1/timesheetEvents/

        If USENEWPATH Then
            strPath = "/DC360/DCWip/wip/consumePallet/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(sfdcEvent)
        post_WIPConsume = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID)
    End Function


    Public Structure struct_POST_WIPCreate
        Public system As String
        Public partRef As String
        Public dateTime As String
        Public operatorId As Int32
        Public pieces As Int32
        Public noUp As Int32
        Public startRecordNo As Int32
        Public endRecordNo As Int32
        Public docsPerRecord As Int32
        Public resourceId As Int32
        Public loadFlagTemplate As String
        Public printerUNC As String
        Public loadFlagQtyToPrint As Int32
        Public bartenderCommanderPath As String

    End Structure
    Public Function post_WIPCreate(sfdcEvent As struct_POST_WIPCreate) As Boolean 'Success = True
        Dim strV As String = "v1"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCWip/" & strV & "wip/createPallet/" 'http://sg360-apps-01:8083/DCTimesheets/v1/timesheetEvents/

        If USENEWPATH Then
            strPath = "/DC360/DCWip/wip/createPallet/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(sfdcEvent)
        post_WIPCreate = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID,, "1")
    End Function


    Public Structure struct_POST_WIPAdjust
        Public system As Int32
        Public partRef As Int32
        Public dateTime As Int32
        Public operatorId As Int32
        Public pieces As Int32
        Public noUp As Boolean
        Public startRecordNo As Boolean
        Public endRecordNo As Int32
        Public docsPerRecord As Int32
        Public resourceId As Int32
        Public loadFlagTemplate As Int32
        Public printerUNC As String
        Public loadFlagQtyToPrint As Int32
        Public bartenderCommanderPath As Int32

    End Structure
    Public Function post_WIPAdjust(sfdcEvent As struct_POST_WIPAdjust) As Boolean 'Success = True
        Dim strV As String = "v1"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCWip/" & strV & "wip/adjustPallet/" 'http://sg360-apps-01:8083/DCTimesheets/v1/timesheetEvents/

        If USENEWPATH Then
            strPath = "/DC360/DCWip/wip/adjustPallet/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(sfdcEvent)
        post_WIPAdjust = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID)
    End Function

    Public Class cls_GET_OutputPartRefs
        Public partRef As String
        Public description As String
    End Class
    Public Function Get_OutputPartRefs(taskID As String) As List(Of cls_GET_OutputPartRefs)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = "v1"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCWip/" & strV & "wip/getOutputPartRefs/" & taskID & "/"

        If USENEWPATH Then
            strPath = "/DC360/DCWip/wip/getOutputPartRefs/" & taskID & "/"
        End If


        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_GET_OutputPartRefs) = js.Deserialize(strResult, GetType(List(Of cls_GET_OutputPartRefs)))
        Get_OutputPartRefs = lstResult
    End Function
    Public Class cls_GET_JasmineFormsRuns
        Public fileId As String
        Public formNumber As String
        Public runNumber As String
        Public segmentId As String
        Public productionType As String
    End Class
    Public Function GET_JasmineFormsRuns(taskID As String) As List(Of cls_GET_JasmineFormsRuns)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = "v1"
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCWip/" & strV & "wip/getJasmineFormsRuns//" & taskID & "/"

        If USENEWPATH Then
            strPath = "/DC360/DCWip/wip/getJasmineFormsRuns/" & taskID & "/"
        End If


        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_GET_JasmineFormsRuns) = js.Deserialize(strResult, GetType(List(Of cls_GET_JasmineFormsRuns)))
        GET_JasmineFormsRuns = lstResult
    End Function


    Public Class cls_getJobNotes
        Public noteId As Int32      'Internal ID. Required for Post and Patch. Example: 1.0 (SHOWS STRING IN EXAMPLE)
        Public categoryId As String     '* Note Category
        Public resourceId As Int32  '* (SHOWS STRING IN EXAMPLE)
        Public jobNo As String  '
        Public taskId As String  '* Set to zero if categoryId = Shift
        Public createdDateTime As String       '*
        Public operatorId As String  '* Set to zero if no operatorID
        Public operatorFirstName As String  '
        Public operatorLastName As String       '
        Public note As String  '* Text note
    End Class
    Public Structure struct_POST_JobNotes
        Public noteId As Int32      'Internal ID. Required for Post and Patch. Example: 1.0 (SHOWS STRING IN EXAMPLE)
        Public categoryId As String     '* Note Category
        Public resourceId As Int32  '* (SHOWS STRING IN EXAMPLE)
        Public jobNo As String  '
        Public taskId As String  '* Set to zero if categoryId = Shift
        Public createdDateTime As String       '*
        Public operatorId As String  '* Set to zero if no operatorID
        Public operatorFirstName As String  '
        Public operatorLastName As String       '
        Public note As String  '* Text note
    End Structure
    Public Function Get_getJobNotes(jobNo As String) As List(Of cls_getJobNotes)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCNotes/" & strV & "getJobNotes/" & jobNo & "/"

        If USENEWPATH Then
            strPath = "/DC360/DCNotes/getJobNotes/" & jobNo & "/"
        End If


        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getJobNotes) = js.Deserialize(strResult, GetType(List(Of cls_getJobNotes)))
        Get_getJobNotes = lstResult

    End Function
    Public Function Get_getShiftNotes(resourceId As String) As List(Of cls_getJobNotes)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCNotes/" & strV & "getShiftNotes/" & resourceId & "/"

        If USENEWPATH Then
            strPath = "/DC360/DCNotes/getShiftNotes/" & resourceId & "/"
        End If


        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getJobNotes) = js.Deserialize(strResult, GetType(List(Of cls_getJobNotes)))
        Get_getShiftNotes = lstResult

    End Function
    Public Function Get_getNotes(noteId As String) As List(Of cls_getJobNotes)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCNotes/" & strV & "notes/" & noteId & "/"

        If USENEWPATH Then
            strPath = "/DC360/DCNotes/notes/" & noteId & "/"
        End If


        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getJobNotes) = js.Deserialize(strResult, GetType(List(Of cls_getJobNotes)))
        Get_getNotes = lstResult


    End Function
    Public Function post_Notes(jobNote As struct_POST_JobNotes) As Boolean
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        'Dim strPath As String = "/DCNotes/" & strV & "notes/" & jobNote.noteId & "/" '
        Dim strPath As String = "/DCNotes/" & strV & "notes/"  '

        If USENEWPATH Then
            strPath = "/DC360/DCNotes/notes/"
        End If



        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(jobNote)
        post_Notes = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID)

    End Function
    Public Function put_Note(jobNote As struct_POST_JobNotes) As Boolean
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCNotes/" & strV & "notes/" & jobNote.noteId & "/" '

        If USENEWPATH Then
            strPath = "/DC360/DCNotes/notes/" & jobNote.noteId & "/"
        End If



        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(jobNote)
        put_Note = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID, "PUT")

    End Function
    Public Structure struct_POST_LogEntry
        Public localDateTime As String      '"2018-04-18T18:41:01"
        Public applicationName As String    'name of the application generating the log (e.g. DC360)
        Public functionName As String       'Function that the user was carrying out (typically dialog.object, e.g. MainScreen.ChangeOpCode)
        Public userID As String             'Users ID if known
        Public description As String        'Other useful information (e.g. count, job no, etc.)
    End Structure
    Public Function post_LogEntry(jobNote As struct_POST_LogEntry) As Boolean
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCLogging/" & strV & "logEntry/"  '

        If USENEWPATH Then
            strPath = "/DC360/DCLogging/logEntry/"
        End If

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim strJson As String = js.Serialize(jobNote)
        post_LogEntry = webRequest_POST(strHost, strPath, strJson, strV, strCorrelationID)
    End Function
    Public Class cls_getLanguages
        Public languageId As Int32 '*       Example: 1
        Public locale As String  '* Example: en-US
        Public description As String  '* Example: English, Ingles      
    End Class
    Public Function Get_getLanguages() As List(Of cls_getLanguages)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSettings/" & strV & "languages/"       '"http://sg360-apps-01.ad.sg360.com/DCSettings/languages"

        If USENEWPATH Then
            strPath = "/DC360/DCSettings/languages/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getLanguages) = js.Deserialize(strResult, GetType(List(Of cls_getLanguages)))
        Get_getLanguages = lstResult
    End Function
    Public Class cls_getEnvironments
        Public Id As Int32 '*       Environment ID, Example: 1
        Public name As String  '* Environment Name, 
        Public hostName As String  '* Example: prod.dc360.sg360.com
        Public rgbColor As String  '* Comma separated list of rgb values, Example: 0, 255,0
    End Class
    Public Function Get_getEnvironments() As List(Of cls_getEnvironments)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSettings/" & strV & "environments/"       'curl -X GET "http://sg360-apps-01.ad.sg360.com/DCSettings/v1/environments"

        If USENEWPATH Then
            strPath = "/DC360/DCSettings/environments/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getEnvironments) = js.Deserialize(strResult, GetType(List(Of cls_getEnvironments)))
        Get_getEnvironments = lstResult
    End Function
    Public Class cls_getSettings
        Public settingId As Int32 '*       Example: 1
        Public settingName As String  '* Admin Mode Timeout Secs
        Public settingValue As String  '* Example: 120  
    End Class
    Public Function Get_getSettings() As List(Of cls_getSettings)
        Dim strResult As String = ""
        Dim strQry As String = ""
        Dim strV As String = strVersion
        If strV <> "" Then strV += "/"
        Dim strPath As String = "/DCSettings/" & strV & "settings/"       'http://sg360-apps-01.ad.sg360.com/DCSettings/v1/settings

        If USENEWPATH Then
            strPath = "/DC360/DCSettings/settings/"
        End If

        strResult = FixRawJson(WebRequest_GET(strHost, strPath, strV, strCorrelationID))
        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer
        Dim lstResult As List(Of cls_getSettings) = js.Deserialize(strResult, GetType(List(Of cls_getSettings)))
        Get_getSettings = lstResult
    End Function
    Private Function webRequest_POST(ByVal strhttpRoot As String, ByVal strhttpPath As String, ByVal strJson As String, strVersion As String, strCorID As String, Optional strPOSTorPUT As String = "POST", Optional ManualResponse As String = "", Optional RepostRequest As Boolean = False) As Boolean 'Success = True
        Dim wrRequest As System.Net.WebRequest
        Dim ws As System.Net.WebResponse
        Dim strErrors As String = ""
        Dim strSuccessFail As String = ""
        Dim swLatency As New Stopwatch
        swLatency.Start()
        strLastGetorPostRawJSonData = strPOSTorPUT & ": " & strJson
        Dim xx As Long = 0
        Try
            Dim postData() As Byte = System.Text.Encoding.ASCII.GetBytes(strJson) : xx += 1
            wrRequest = System.Net.WebRequest.Create(strhttpRoot & strhttpPath) : xx += 1
            wrRequest.Method = strPOSTorPUT : xx += 1
            wrRequest.ContentType = "application/json" : xx += 1
            wrRequest.Headers.Add("correlationID", strCorID) : xx += 1
            wrRequest.Timeout = iHttpTimeout : xx += 1
            wrRequest.ContentLength = postData.Length : xx += 1
            wrRequest.GetRequestStream().Write(postData, 0, postData.Length) : xx += 1 'Write the body
            ws = wrRequest.GetResponse() : xx += 1 'Send request, get response
            Dim retStatCode As Long = CType(ws, System.Net.HttpWebResponse).StatusCode
            Using reader As New System.IO.StreamReader(ws.GetResponseStream()) : xx += 1
                While Not reader.EndOfStream
                    strErrors += reader.ReadLine()
                End While
            End Using : xx += 1
            Select Case ManualResponse
                Case "1"
                    WIP_NEWPALLET = ""
                    If strErrors.StartsWith("{""barcode"":null,""system"":null,""pieces"":null,""startRecordNo"":null,""endRecordNo"":null,""palletNumber"":") Then
                        WIP_NEWPALLET = strErrors.Split(":")(strErrors.Split(":").GetUpperBound(0)).Replace("}", "")
                        strWebResponseErrorMessage = ""
                        strErrors = ""
                        bWebResponseSuccess = True
                    Else
                        Err.Raise(99, strErrors)
                    End If
                Case ""
                    If strErrors = "" Then 'success
                        strWebResponseErrorMessage = ""
                        bWebResponseSuccess = True
                    Else
                        'MsgBox(strErrors)
                        Err.Raise(99, strErrors)
                    End If

            End Select

            strSuccessFail = "SUCCESS"
        Catch ex As System.Net.WebException ' Exception
            Dim retStatCode As Long
            If ex.Response Is Nothing Then 'time out

            Else
                retStatCode = CType(ex.Response, System.Net.HttpWebResponse).StatusCode
            End If

            strWebResponseErrorMessage = ex.Message
            bWebResponseSuccess = False
            strSuccessFail = "FAIL"
            GoLogFailedPOST(strJson, strhttpRoot, strhttpPath, strPOSTorPUT, strSuccessFail, strVersion, strCorID, swLatency.ElapsedMilliseconds, strWebResponseErrorMessage)
            If RepostRequest Then 'Repost EVENT
                GoLogRePOST(strJson, strhttpRoot, strhttpPath, strPOSTorPUT, strSuccessFail, strVersion, strCorID, swLatency.ElapsedMilliseconds, strWebResponseErrorMessage)
            End If
        End Try
        webRequest_POST = bWebResponseSuccess
        GoLog(strJson, strhttpRoot, strhttpPath, strPOSTorPUT, strSuccessFail, strVersion, strCorID, swLatency.ElapsedMilliseconds, strWebResponseErrorMessage)

    End Function
    Private Function WebRequest_GET(ByVal strhttpRoot As String, ByVal strhttpPath As String, strVersion As String, strCorID As String) As String
        Dim wrRequest As System.Net.WebRequest
        Dim ws As System.Net.WebResponse
        Dim swLatency As New Stopwatch
        Dim strRaw As String = ""
        Dim strSuccessFail As String = ""
        swLatency.Start()
        strLastGetorPostRawJSonData = "GET: "
        Try
            wrRequest = System.Net.WebRequest.Create(strhttpRoot & strhttpPath)
            wrRequest.Method = "GET"
            wrRequest.ContentType = "text/json"
            wrRequest.Headers.Add("correlationID", strCorID)
            wrRequest.Timeout = iHttpTimeout
            ws = wrRequest.GetResponse()
            Dim retStatCode As Long = CType(ws, System.Net.HttpWebResponse).StatusCode

            Dim ReceiveStream As System.IO.Stream = ws.GetResponseStream()
            Dim encode As System.Text.Encoding = System.Text.Encoding.GetEncoding("utf-8")
            Dim readStream As New System.IO.StreamReader(ReceiveStream, encode)
            strRaw = readStream.ReadToEnd()
            strLastGetorPostRawJSonData += strRaw
            strWebResponseErrorMessage = ""
            bWebResponseSuccess = True
            WebRequest_GET = strRaw
            'MsgBox(strRaw)
            strSuccessFail = "SUCCESS"
        Catch ex As System.Net.WebException '  Exception
            Dim retStatCode As Long
            If ex.Response Is Nothing Then 'time out

            Else
                retStatCode = CType(ex.Response, System.Net.HttpWebResponse).StatusCode
            End If
            strWebResponseErrorMessage = ex.Message
            bWebResponseSuccess = False
            WebRequest_GET = ""
            strSuccessFail = "FAIL"
        End Try
        GoLog(strRaw, strhttpRoot, strhttpPath, "GET", strSuccessFail, strVersion, strCorID, swLatency.ElapsedMilliseconds, strWebResponseErrorMessage)

    End Function
    Private Function FixRawJson(strJsonRaw As String) As String
        Dim strReturn As String = strJsonRaw
        If strJsonRaw <> "" Then
            If Strings.Left(strJsonRaw, 1) <> Chr(91) And Strings.Right(strJsonRaw, 1) <> Chr(93) Then
                strReturn = Chr(91) & strReturn & Chr(93)
            Else : End If
        Else : End If
        FixRawJson = strReturn
    End Function
    Private Function FormatSearchString(strSearchString As String) As String
        Dim strReturn As String = strSearchString
        'Fix Search Search Result Errors Here, Future
        FormatSearchString = strReturn
    End Function
    Public Sub FailedPOSTCount(ByRef iTotalRecords As Integer, ByRef iFailCount As Integer, ByRef iFixedCount As Integer, ByRef iSuccessCount As Integer)
        Dim swFile As System.IO.StreamReader
        Dim strLine As String = ""
        Dim iTotalCount As Integer = 0
        Dim arrLine() As String
        Dim strField As String

        If System.IO.File.Exists(strDC360RootPath & strDC360FaildsPOSTSFileName) Then
            swFile = My.Computer.FileSystem.OpenTextFileReader(strDC360RootPath & strDC360FaildsPOSTSFileName, System.Text.Encoding.ASCII)
            While Not swFile.EndOfStream
                strLine = swFile.ReadLine()
                If iTotalRecords > 0 Then
                    arrLine = Split(strLine, vbTab)
                    strField = arrLine(2)
                    Select Case strField
                        Case "FAIL"
                            iFailCount += 1
                        Case "SUCCESS"
                            iSuccessCount += 1
                        Case "FIXED"
                            iFixedCount += 1
                    End Select
                End If
                iTotalRecords += 1
            End While
        End If


    End Sub
    Private Function WriteFile(strPathFileName As String, strData As String)
        Dim bExists As Boolean = CreateDC360Directory()
        Dim strBuffer As String = ""
        Dim swFile As System.IO.StreamWriter
        If bExists Then
            bExists = System.IO.File.Exists(strPathFileName)
            swFile = My.Computer.FileSystem.OpenTextFileWriter(strPathFileName, True)
            If bExists = False Then
                strBuffer = strLogFileHeader
                swFile.WriteLine(strBuffer)
            Else End If
            strBuffer = strData
            swFile.WriteLine(strBuffer)
            swFile.Close()
            Return True
        Else
            Return False
        End If

    End Function

    Public Function RerunPost(ByVal strhttpRoot As String, ByVal strhttpPath As String, ByVal strJson As String, strVersion As String, strCorID As String) As Boolean
        RerunPost = webRequest_POST(strhttpRoot, strhttpPath, strJson, strVersion, strCorID, , , True)
    End Function
    Public Function ReWritePost(strJSonData As String, strhttpRoot As String, strhttpPath As String, strVersion As String, strCorID As String) As Boolean
        ReWritePost = GoLogRePOST(strJSonData, strhttpRoot, strhttpPath, "POST", "FAIL", strVersion, strCorID)
    End Function
    Private Function GoLogFailedPOST(strJSonData As String, strhttpRoot As String, strhttpPath As String, strGETorPOST As String, strSUCCESSorFAIL As String, strVersion As String, strCorID As String, Optional strLatency As String = "n/a", Optional strNotes As String = "")
        Dim strBuffer As String = ""
        strBuffer = Now & vbTab & "POST" & vbTab & "FAIL" & vbTab & "n/a" & "ms" & vbTab & strhttpRoot & vbTab & strhttpPath & vbTab & strCorID & vbTab & strVersion & vbTab & strNotes & vbTab & strJSonData
        WriteFile(strDC360RootPath & strDC360FaildsPOSTSFileName, strBuffer)
        Return True
    End Function
    Private Function GoLogRePOST(strJSonData As String, strhttpRoot As String, strhttpPath As String, strGETorPOST As String, strSUCCESSorFAIL As String, strVersion As String, strCorID As String, Optional strLatency As String = "n/a", Optional strNotes As String = "")
        Dim strBuffer As String = ""
        strBuffer = Now & vbTab & "POST" & vbTab & "FAIL" & vbTab & "n/a" & "ms" & vbTab & strhttpRoot & vbTab & strhttpPath & vbTab & strCorID & vbTab & strVersion & vbTab & strNotes & vbTab & strJSonData
        WriteFile(strDC360RootPath & strDC360RePostLogFileName, strBuffer)
        Return True
    End Function
    Private Function GoLog(strJSonData As String, strhttpRoot As String, strhttpPath As String, strGETorPOST As String, strSUCCESSorFAIL As String, strVersion As String, strCorID As String, Optional strLatency As String = "n/a", Optional strNotes As String = "") As Boolean
        Dim strBuffer As String = ""

        strBuffer = Application.ProductVersion & vbTab & Now & vbTab & strGETorPOST & vbTab & strSUCCESSorFAIL & vbTab & strLatency & "ms" & vbTab & strhttpRoot & vbTab & strhttpPath & vbTab & strCorID & vbTab & strVersion & vbTab & strNotes & vbTab & strJSonData
        WriteFile(strDC360RootPath & strDC360LogFileName, strBuffer)
        Return True
    End Function
    Public Function CreateDC360Directory() As Boolean 'Returns True If Exists Or Successfully Created
        Dim bExists As Boolean = System.IO.Directory.Exists(strDC360RootPath)
        strDC360DirectoryCreationErrorMessage = ""
        If Not bExists Then
            Try
                System.IO.Directory.CreateDirectory(strDC360RootPath)
                bExists = True
            Catch ex As Exception
                strDC360DirectoryCreationErrorMessage = ex.Message
            End Try
        Else End If
        Return bExists
    End Function
End Module
